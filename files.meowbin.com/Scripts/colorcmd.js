function verifyHex(hex) {
    if (/(^#[0-9A-F]{3}$)/i.test(hex)) hex = "#" + hex[1] + hex[1] + hex[2] + hex[2] + hex[3] + hex[3];
    if (/^#[0-9A-F]{6}$/i.test(hex)) {
        return true;
    } else {
        return false;
    }
}
function formatHex(hex) {
    if (verifyHex(hex)) {
        if (/(^#[0-9A-F]{3}$)/i.test(hex)) hex = "#" + hex[1] + hex[1] + hex[2] + hex[2] + hex[3] + hex[3];
        return hex;
    } else {
        return false;
    }
}
newCmd("color", function(msg) {
    let color = Color.map[msg.text[0] + msg.text.substr(1)] || Color.map[msg.text.split(" ").reduce((a,b)=>a+=(a==""?"":" ")+b[0].toUpperCase()+b.substr(1),"")] || false;
    if (msg.isBlank) {
        sendChat("Your color looks like " + new Color(msg.p.color).getName() + " | " + msg.p.color);
    } else if (msg.part) {
        sendChat("Friend " + msg.part.name + "'s color looks like " + new Color(msg.part.color).getName() + " | " + msg.part.color);
    } else if (verifyHex(msg.text)) {
        msg.text = formatHex(msg.text);
        sendChat("That looks like " + new Color(msg.text).getName() + " | " + msg.text);
    } else if (color) {
        sendChat(color.getName() + "'s hex value is " + color.toHexa());
    } else {
        sendChat("Not found or not valid (" + msg.text + ")");
    }
});