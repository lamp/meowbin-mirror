function checkUrl(msg) {
    msg = msg.split(" ").reduce(function(a,b,c) {
        if (/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(b)) a = b;
        return a;
    }, "");
    if (msg == "") return false;
    return msg.match(/[a-zA-Z0-9\-\_]{11}/)[0] || false;
}
MPP.client.on("a", function(msg) {
    var url = checkUrl(msg.a);
    if (url) {
        $.getJSON("http://10.0.0.8:8180/api/https://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=" + url + "&format=json", function(resp) {
            sendChat("Video title: " + resp.res.title + ", Video author: " + resp.res.author_name + " //todo: better API for more detailed info");
        });
    }
});