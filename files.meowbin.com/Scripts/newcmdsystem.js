String.prototype.temp = function(obj) {
    return this.replace(/\[.*?\]/g, function(x) {
        return eval(x.split(".").reduce(function(a,b) { a+="['"+b.replace(/\[|\]/g, "")+"']"; return a; }, "obj"));
    });
};
Client.prototype.getParticipantByName = function(name) {
    for (var i in this.ppl) {
        var part = this.ppl[i];
        if (part.name.toLowerCase().indexOf(name.toLowerCase()) !== -1 || part._id == name || part.id == name) {
            return part;
            break;
        }
    }
    return false;
};
var ops = [MPP.client.user._id];
var commands = {};
function newCmd(cmd, resp, args) {
    commands[cmd] = {resp: resp, args: args};
};
MPP.client.on("a", function(msg) {
    msg.args = msg.a.split(' ');
    msg.cmd = msg.args[0].toLowerCase();
    msg.text = msg.args.splice(1).join(' ');
    msg.isOp = ops.indexOf(msg.p._id) !== -1;
    msg.part = MPP.client.search(msg.text) || "￼".repeat(50);
    //if (msg.part.name == msg.p.name) msg.part = "self".repeat(30);
    msg.isBlank = msg.text.trim() == '';
	msg.me = MPP.client.getOwnParticipant();
    msg.get = function(x) { return window[x]; };
    msg.invis = "￼";
    for (let cmd in commands) {
        if (msg.cmd == operator+cmd) {
            let resp = eval('`'+commands[cmd].resp+'`');
            MPP.chat.send(msg.part == "￼".repeat(50) && commands[cmd].args ? "Participant not found" : msg.isBlank && commands[cmd].args ? "Please enter a name" : resp);
        }
    }
});
newCmd("test", "MEOW! [msg.p.name]", false);
newCmd("poke", "￼[msg.p.name] poked [msg.part.name]", true);