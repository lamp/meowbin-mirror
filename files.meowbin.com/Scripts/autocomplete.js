document.getElementsByClassName("translate")[0].onkeydown = function(evt) {
    if (evt.code == "Tab") {
        let args = evt.target.value.split(" ");
        args[args.length - 1] = MPP.client.search(args[args.length - 1]).name || args[args.length - 1];
        evt.target.value = args.join(" ");
    }
}
Client.prototype.search = function (query) {
	if (query.trim() == "") return false;
	for (var i in this.ppl) {
		var part = this.ppl[i];
		var name = part.name.toLowerCase();
		if (name.indexOf(query.toLowerCase()) !== -1) {
			return part;
			break;
		}
	}
	return false;
};