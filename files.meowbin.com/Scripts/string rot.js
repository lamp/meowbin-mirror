String.prototype.rot = function(d = 13) {
    let abc = "abcdefghijklmnopqrstuvwxyz";
    return this.split("").reduce(function(a,b) {
        let c = b.toLowerCase();
        let char = (abc.indexOf(c) == -1 ? b : abc[(abc.indexOf(c) + d) % abc.length]);
        a += b == b.toUpperCase() ? char.toUpperCase() : char;
        return a;
 }, "");
};
var rot = 0;
MPP.chat.send = function(msg) {
    MPP.client.sendArray([{m: "a", message: msg.rot(rot)}]);
};