String.prototype.scramble = function() {
    var arr = this.split("")
    for (var i = arr.length - 1; i > 0; i--) {
        var b = Math.floor(Math.random() * (i + 1));
        var tmp = arr[i];
        arr[i] = arr[b];
        arr[b] = tmp;
    }
    return arr.join("");
};
var inGame = false;
var words = ["writer","heal","spiders","volatile","invention","creature","stiff","hill","stupid","frame","bang","new","type","delicious","flimsy","closed","home","imminent","kindhearted","shirt","beg","knife","sticky","ocean","wood","divergent","grubby","grab","print","talk","hop","wink","abiding","quickest","please","gaudy","tough","invincible","jobless","zipper","ultra","tight","shape","arch","zoom","push","van","giddy","invite","blue","ethereal","unruly","language","abnormal","tasty","flaky","colossal","paste","pest","beds","aromatic","apathetic","current","army","miscreant","brave","female","wholesale","steam","fax","knowing","mundane","act","support","disagreeable","parcel","mountain","grandfather","yam","homeless","train","mice","trains","exciting","arrest","eggnog","peck","inexpensive","race","dusty","youthful","better","canvas","clear","selection","offbeat","ask","flowers","decorous","infamous","scold","carriage","button","economic","imperfect","rabbits","spiritual","porter","upbeat","condemned","alive","mailbox","wise","idea","skinny","strip","whine","waste","flow","thin","learn","squeamish","disgusted","acid","lackadaisical","excite","hurt","annoyed","hulking","lavish","channel","humdrum","reaction","join","left","sisters","receive","juicy","ignorant","suit","achiever","like","hesitant","claim","pump","girls","ducks","elderly","afternoon","meal","wash","hunt","curly","joyous","wish","bare","telephone","hospitable","draconian","anger","replace","hapless","night","smell","level","stare","coat","knot","can","deadpan","acoustic","upset","vulgar","wreck","star","offend","cheat","sleet","engine","pause","hilarious","cobweb","flower","bumpy","brainy","bird","dream","obese","suck","tick","abortive","sad","price","handsomely","pumped","melodic","ajar","stocking","thing","rabbit","river","wicked","tin","sordid","young","cheap","squirrel","enter","shop","hurried","poised","basketball","trashy","stain","ripe","drunk","tearful","historical","fresh","utter","milk","lie","obscene","design","spicy","trick","riddle","visitor","cooperative","turn","sticks","naive","prickly","conscious","domineering","limping","gleaming","useful","glue","report","staking","fear","horse","guide","care","wrong","whip","bulb","roll","repulsive","axiomatic","overwrought","confuse","rainy","box","request","late","time","lighten","credit","governor","answer","crook","office","finicky","pin","grey","jumbled","horses","lace","yummy","fearful","leg","rest","raise","step","stone","arrange","ambitious","paint","momentous","shake","island","interest","spell","match","reflective","muddle","nut","boil","cook","endurable","wealthy","year","silk","unadvised","satisfying","encouraging","land","brown","farm","shaggy","trucks","interesting","rule","vengeful","meeting","zonked","seat","ill","cover","cowardly","breezy","imaginary","vest","include","fragile","strong","hypnotic","stream","thumb","third","advice","entertain","ancient","literate","racial","society","trouble","trip","tremble","event","wobble","point","jam","comfortable","permit","flavor","plain","sweltering","wooden","square","mate","chunky","gun","multiply","tempt","girl","skillful","book","flagrant","cabbage","agreement","lucky","disgusting","toothpaste","want","crown","consider","sweater","near","fence","quiet","story","fair","water","tooth","thinkable","instinctive","interfere","miss","fire","troubled","sharp","debt","violet","good","yoke","plant","discover","appliance","page","tow","sin","things","silky","treatment","quartz","hour","head","hall","instrument","trust","reminiscent","zoo","rainstorm","division","object","flesh","sassy","flat","efficacious","warn","pollution","zealous","bathe","hammer","hateful","nerve","cold","regret","film","angle","mysterious","north","noiseless","hope","mourn","disapprove","plants","horn","evasive","rot","absurd","famous","mind","accidental","arrive","touch","unknown","stick","impress","eatable","man","fry","cherry","unfasten","dad","paddle","silver","transport","acoustics","heat","tank","resonant","subtract","boot","screw","far-flung","sail","walk","oven","furry","stitch","successful","wriggle","truck","numberless","table","fold","freezing","drain","erratic","likeable","innate","clammy","harmonious","educated","advertisement","nest","plug","measure","tree","opposite","rice","cloth","onerous","income","loutish","imagine","easy","cart","double","floor","tasteful","boiling","calculate","gold","flap","air","employ","turkey","drop","zany","program","juggle","grumpy","bait","distribution","dust","explode","tax","violent","dynamic","name","meek","vessel","decision","donkey","bore","tray","remarkable","spoon","rescue","argue","angry","guiltless","move","mom","general","gaping","tip","wiry","abundant","salty","precious","dark","suppose","rings","full","terrific","authority","delicate","attend","intend","lumpy","faint","doll","guarantee","cumbersome","cent","wide","burn","ship","veil","scrape","drum","worthless","statuesque","wheel","courageous","coil","extra-large","enchanted","use","clean","deserve","nosy"];
var word;
var guesses;
var scrambled;
MPP.client.on("a", function(msg) {
	var args = msg.a.split(" ");
    var cmd = args[0].toLowerCase();
    var message = msg.a.substring(cmd.length + 1);
	if (cmd == "-ws" || cmd == "-wordscramble") {
		if (word) console.log("Word: " + word, "Scrambled: " + scrambled, "Current Guesses: " + guesses.join(", "), "Game started: " + inGame);
		if (!inGame) {
			word = words[Math.floor(Math.random() * words.length)];
			guesses = [];
			scrambled = word.scramble();
			inGame = true;
			MPP.chat.send("New word scramble game started, try and guess what word " + scrambled + " makes.  (" + (10 - guesses.length) + " guesses left)");
	    } else {
			if (message == word) {
				inGame = false;
				MPP.chat.send("Congratulations " + msg.p.name + " :3 You guessed the word correctly (" + word + ")");
			} else if (guesses.length == 10) {
				inGame = false;
				MPP.chat.send("You're out of guesses :c The word was: " + word);
			} else {
				if (message.trim() == "") {
					MPP.chat.send("Game already started, use -ws guess_here or -wordscramble guess_here (Scrambled word: " + scrambled + ")");
					return;
				}
				if (guesses.indexOf(message) !== -1) {
					MPP.chat.send("You already guessed " + message + "!");
					return;
				}
				guesses.push(message);
				MPP.chat.send("The word is not " + message + " :c (Guesses left: " + ((10 - guesses.length) == 0 ? "0, last chance!" : (10 - guesses.length)) + ", Current Guesses: " + guesses.join(", ") + ", Scrambled word: " + scrambled + ")");
			}
	    }
	}
});