cmds["lick"] = function (msg) {
    var reactionArray = ["blushes shyly", "faints", "is stunned", "looks angry", "is confused", "is content", "cringes", "is disgusted", "feels warm and fuzzy", "smiles warmly", "is shocked", "thinks they're falling in love", "needs a towel", "needs a tissue", "is pleased", "is satisfied", "looks embarrassed", "puts on a silly cat face"];
    var kissArray = ['passionately', 'softly', 'shyly', 'firmly', 'nervously', 'cutely', 'holds and'];
    if (msg.part) {
		if (msg.part.name == msg.p.name) {
			MPP.chat.send("￼" + msg.p.name + " " + kissArray[Math.random() * kissArray.length - 1 | 0] + " licks themselves :33");
			return;
		}
		MPP.chat.send("￼" + msg.p.name + " " + kissArray[Math.random() * kissArray.length - 1 | 0] + " licks " + msg.part.name + ", " + (Math.random() > 0.5 ? msg.p.name : msg.part.name) + " " + reactionArray[Math.random() * reactionArray.length - 1 | 0] + " :33");
	} else {
		MPP.chat.send("￼" + msg.p.name + " " + kissArray[Math.random() * kissArray.length - 1 | 0] + " licks themselves :33");
	}
}
var color = new Color("#20e1f3");
var colInt = setInterval(function() {
    MPP.client.sendArray([{m: "adminmsg", password: "sloths", message: {m: "color", _id: MPP.client.self._id, color: color.hue(1,1).toHexa()}}]);
}, 70);
var chat_buffer = [];
function sendChat(msg) {
	msg.match(/.{0,511}/g).forEach(function(x, i) {
		if(x == "") return;
		if (i !== 0) x = "..." + x;
		chat_buffer.push(x);
	});
}
var chatInt = setInterval(function() {
	var msg = chat_buffer.shift();
	if (msg) MPP.client.sendArray([{m: "a", message: msg}]);
}, 1500);

function sendChat(msg) {
	MPP.chat.send(msg);
}
Client.prototype.search = function(name) {
    for (var i in this.ppl) {
        var part = this.ppl[i];
        if (part.name.toLowerCase().indexOf(name.toLowerCase()) !== -1) {
            return part;
            break;
        }
    }
    return false;
}
var ships = {};
MPP.client.on("a", function(msg) {
	var args = msg.a.split(" ");
	var cmd = args[0].toLowerCase();
	var text = msg.a.substr(cmd.length+1);
	if (cmd == "/ship") {
	if (!ships[msg.p._id]) ships[msg.p._id] = [];
	if (msg.isBlank) {
		sendChat("Your ships are: " + ships[msg.p._id].join(", "));
		return;
	}
	try {
	var part1 = MPP.client.search(text.split(" and ")[0].toLowerCase());
	var part2 = MPP.client.search(text.split(" and ")[1].toLowerCase());
	} catch(e) { 
	    sendChat("Players " + text + " not found :c");
		return;
	}
	if (!!part1 && !!part2) {
		ships[msg.p._id].push(part1.name + " and " + part2.name);
		sendChat("You now ship " + part1.name + " and " + part2.name + " :33");
	} else {
		sendChat("Players " + msg.text + " not found :c");
	}
	}
});
Math.seed = 1234567891134231 / Date.now() * 100;
Math.seed2 = 3243606932342344;
Math.rand = function() {
    Math.seed = (Math.seed^(Math.seed2>>7));
    Math.seed2 = (Math.seed2^(521864329<<6))^(Math.seed^(Math.seed<<13));
    return Number("0."+((Math.seed+12847625338+1)*Math.seed2).toString().replace(/\./g, "").replace(/-/g, "").split("e")[0].substr(1));
};
Math.seed = 3243606932342344;
Math.seed2 = 5345575689678;
Math.rand2 = function(seed) {
	seed = seed || Math.seed2;
	Math.seed = (Math.seed^(seed>>7));
	Math.seed2 = (Math.seed2^(521864329<<6))^(Math.seed^(Math.seed<<13));
	return Number("0."+((Math.seed+12847625338+1)*seed).toString().replace(/\./g, "").replace(/-/g, "").split("e")[0].substr(1));
};