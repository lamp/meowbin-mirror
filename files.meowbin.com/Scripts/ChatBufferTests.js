﻿var chat_buffer = [];
function sendChat(msg) {
	msg.match(/.{0,511}/g).forEach(function(x, i) {
		if(x == "") return;
		if (i !== 0) x = "..." + x;
		chat_buffer.push(x);
	});
};
var chatInt = setInterval(function() {
	var msg = chat_buffer.shift();
	if (msg) MPP.client.sendArray([{m: "a", message: msg}]);
}, 1900);

var chat_buffer = [];
function sendChat(msg, time) {
	var ms = Date.now() + (time || 1500) * (chat_buffer.length == 0 ? 1 : chat_buffer.length);
	msg.match(/.{0,511}/g).forEach(function(a,b,c) {
		if(a == "") return;
		if (b !== 0) a = "..." + a;
		if (typeof time !== "number" && chat_buffer.length == 0) ms = Date.now();
		chat_buffer.push({t: ms, m: a});
	});
};
var chat_int = setInterval(function() {
	chat_buffer = chat_buffer.reduce(function(a,b,c) {
		if (Date.now() >= b.t) MPP.client.sendArray([{m: "a", message: b.m}]);
		else 
			a.push(b);
		return a;
	}, []);
}, 100);