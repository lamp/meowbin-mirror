function scale(notes, time, transpose, noDown) {
    let pKeys = ["a", "as", "b", "c", "cs", "d", "ds", "e", "f", "fs", "g", "gs"];
    let args = notes.toLowerCase().replace(/#/g, "s").split(" ");
    let code = "";
    let d = 0;
    if (!time) time = 30;
    if (!transpose) transpose = 0;
    for (var i = 0; args.length > i; i++) {
        var note = args[i];
        if (pKeys.indexOf(note) == -1) continue;
        code += (code == "" ? "" : " || ") + (note[1] !== undefined ? "note.startsWith(\"" + note + "\")" : "note.startsWith(\"" + note + "\") && note.indexOf(\"s\") == -1");
    }
    var keys = Object.keys(MPP.piano.keys);
    for (var id = 0; keys.length * 2 > id; id++) {
        var note = keys[id];
        if (id > 88) note = keys[88 - (id - 88)];
        if (noDown && id >= 88) break;
        if (!note) continue;
        if (eval(code)) {
            d++;
            (function(n, i) {
                setTimeout(function() {
                    var note = MPP.piano.keys[keys[keys.indexOf(n) + transpose]];
                    if (note) MPP.press(note.note);
                }, time * d);
            }(note, id));
        }
    }
}

function scale(notes = "", time = 60, trans = 0, down = false) {
    let d = 0;
    let keys = Object.keys(MPP.piano.keys);
    for (let i = 176; i--;) {
        let note = keys[(176 - i) - 1] || keys[i];
        if (!down && i <= 88) break;
        if (eval(notes.toLowerCase().replace(/#/g, "s").split(" ").reduce((a,b)=>{
                let n = "note.startsWith(\"" + b + "\")";
                a += keys.join(",").replace(/[0-9]/g,'').split(",").includes(b) ? (a == "" ? "" : " || ") + (b.endsWith("s") ? n : n + " && !note.includes(\"s\")") : "";
                return a;
            }, ""))) {
            d++;
            ((n)=>{
                let key = keys[keys.indexOf(n) + trans];
                if (key) setTimeout(()=>MPP.press((MPP.piano.keys[key]).note), time * d);
            })(note);
        }
    }
}