function translate(text, lang, cb) {
    $.getJSON("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20151222T235754Z.6a5ec8746a5bf1be.cd13262ce958f929d6cdf59b1c2e68d447fdb18f&text=" + text + "&lang=" + lang).done(cb);
}
var chat_messages = [];
function sendChat(msg) {
    var arr = [];
    while (msg.length > 511) {
        arr.push(msg.substr(0, 511));
        msg = "..." + msg.substr(511);
    }
    arr.push(msg);
    arr.forEach(function(x) {
        chat_messages.push([{m: "a", message: x}]);
    });
}
if (chat_interval) clearInterval(chat_interval);
var chat_interval = setInterval(function() {
    var msg = chat_messages.shift();
    if (msg) {
        MPP.client.sendArray(msg);
    }
}, 1600);
var langs = {"Albanian": "sq", "English": "en", "Arabic": "ar", "Armenian": "hy", "Azerbaijan": "az", "Afrikaans": "af", "Basque": "eu", "Belarusian": "be", "Bulgarian": "bg", "Bosnian": "bs", "Welsh": "cy", "Vietnamese": "vi", "Hungarian": "hu", "Haitian": "ht", "Galician": "gl", "Dutch": "nl", "Greek": "el", "Georgian": "ka", "Danish": "da", "Yiddish": "he", "Indonesian": "id", "Irish": "ga", "Italian": "it", "Icelandic": "is", "Spanish": "es", "Kazakh": "kk", "Catalan": "ca", "Kyrgyz": "ky", "Chinese": "zh", "Korean": "ko", "Latin": "la", "Latvian": "lv", "Lithuanian": "lt", "Malagasy": "mg", "Malay": "ms", "Maltese": "mt", "Macedonian": "mk", "Mongolian": "mn", "German": "de", "Norwegian": "no", "Persian": "fa", "Polish": "pl", "Portuguese": "pt", "Romanian": "ro", "Russian": "ru", "Serbian": "sr", "Slovakian": "sk", "Slovenian": "sl", "Swahili": "sw", "Tajik": "tg", "Thai": "th", "Tagalog": "tl", "Tatar": "tt", "Turkish": "tr", "Uzbek": "uz", "Ukrainian": "uk", "Finish": "fi", "French": "fr", "Croatian": "hr", "Czech": "cs", "Swedish": "sv", "Estonian": "et", "Japanese": "ja"};
var langs2 = {};
for (var i in langs) {
	if (!langs.hasOwnProperty(i)) continue;
	langs2[langs[i]] = i;
}
function toIn(x) {
	return langs[x];
}
MPP.client.on("a", function(msg) {
    var args = msg.a.split(" ");
    var cmd = args[0].toLowerCase();
	args.shift();
    var message = args.join(" ");
    var query = message.split("-");
	if (cmd == "-translate") {
		var text = query[0];
		var lang = message.split("-L")[1].substring(1);
		var lin = toIn(lang);
		if (!text) {
			MPP.chat.send("You must enter some text to translate. Usage: -translate test text -l lang (use -langs for a list of languages)");
			return;
		}
		if (!lang) {
			MPP.chat.send("You must enter the language you wish to translate the text to. Usage: -translate test text -l lang (use -langs for a list of languages)");
			return;
		}
		if (!lin) {
			MPP.chat.send("Language not supported :c");
		} else {
			translate(text, lin, function(x) { sendChat("Translation (Probable Language: " + langs2[x.lang.split("-")[0]] + "): " + x.text[0]) });
		}
	}
	if (cmd == "-langs") {
		var str = "";
		for (var i in langs) {
			if (!langs.hasOwnProperty(i)) continue;
			str += str == "" ? i : ", " + i;
		}
		sendChat("Current supported langauges: " + str);
	}
    if (cmd == "-ud") {
		var term = query[0];
		var page = message.split("-p")[1];
		if (!term) {
			MPP.chat.send("You must enter a term!");
			return;
		}
        $.ajax({
            url: 'https://mashape-community-urban-dictionary.p.mashape.com/define?term=' + term,
            dataType: "json",
            headers: {
                "X-Mashape-Key": "it5X0f16eImshiux7zwwY8DDOqxQp1JQ51vjsn7vlSIeDn72nT"
            }
        }).done(function(x) {
			var list = x.list;
			if (!page) {
				MPP.chat.send("There are " + list.length + " descriptions for this term, enter a page with -ud term_here -p page_number");
				return;
			}
			try {
                sendChat("Definition: " + x.list[~~page - 1].definition.replace(/\n|\r\n|\r/g, " "));
			} catch(e) {
				sendChat("Definition of the term " + term + " not found :c");
			}
        });
    }
});