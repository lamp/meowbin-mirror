var transLvl = 0;
var octave = 0;
var msgs = 0;
var keys = [];
function msgBox(about, info, duration, target) {
    MPP.client.emit("notification", {
        title: about,
        html: info,
        target: target,
        duration: duration
    });
}
keyDown = function(evt) {
    var code = parseInt(evt.keyCode);
    var code = parseInt(evt.keyCode);
    if (code == 33) {
        transLvl++;
        msgBox('Transposing', 'Transpose level: ' + transLvl, 1500, '#midi-btn');
    }
    if (code == 34) {
        transLvl--;
        msgBox('Transposing', 'Transpose level: ' + transLvl, 1500, '#midi-btn');
    }
    if (!keys[code]) {
        octave = 0;
        keys[code] = true;
        if (keys[32]) --octave == --octave + -octave;
        else if (keys[18]) ++octave == ++octave + +octave;
        else if (keys[192]) ++octave == ++octave + ++octave;
    }
};
keyUp = function(evt) {
    var code = parseInt(evt.keyCode);
    if (keys[code]) {
        octave = 0;
        keys[code] = false;
        if (keys[32]) --octave == --octave + -octave;
        else if (keys[18]) ++octave == ++octave + +octave;
        else if (keys[192]) ++octave == ++octave + ++octave;
    }
}
$(document).on("keydown", keyDown);
$(document).on("keyup", keyUp);
MPP.client.startNote = function(note, vel) {
    note = MPP.piano.keys[Object.keys(MPP.piano.keys)[Object.keys(MPP.piano.keys).indexOf(note) + transLvl]].note;
    var octaves = parseInt(note.replace(/[^\d.]/, '').replace('s', ''));
    var note = note.replace(/[0-9]/g, '').replace("-", "");
    note = note + (octaves + octave);
    if (this.isConnected()) {
        var vel = typeof vel === "undefined" ? undefined : +vel.toFixed(3);
        if (!this.noteBufferTime) {
            this.noteBufferTime = Date.now();
            this.noteBuffer.push({
                n: note,
                v: vel
            });
        } else {
            this.noteBuffer.push({
                d: Date.now() - this.noteBufferTime,
                n: note,
                v: vel
            });
        }
    }
}
MPP.piano.play = function(note, vol, participant, delay_ms) {
    if (participant._id == MPP.client.getOwnParticipant()._id) {
        note = MPP.piano.keys[Object.keys(MPP.piano.keys)[Object.keys(MPP.piano.keys).indexOf(note) + transLvl]].note;
        var octaves = parseInt(note.replace(/[^\d.]/, '').replace('s', ''));
        var note = note.replace(/[0-9]/g, '').replace("-", "");
        note = note + (octaves + octave);
    }
    if (!this.keys.hasOwnProperty(note)) return;
    var key = this.keys[note];
    if (key.loaded) this.audio.play(key.note, vol, delay_ms, participant.id);
    var self = this;
    var jq_namediv = $(typeof participant == "undefined" ? null : participant.nameDiv);
    if (jq_namediv) {
        setTimeout(function() {
            self.renderer.visualize(key, typeof participant == "undefined" ? "yellow" : (participant.color || "#777"));
            jq_namediv.addClass("play");
            setTimeout(function() {
                jq_namediv.removeClass("play");
            }, 30);
        }, delay_ms);
    }
};
$("#volume-label").text("Volume: " + Math.floor(MPP.piano.audio.volume * 100) + "%");
$(".volume-slider").on("input", function(evt) {
    var v = $(".volume-slider")[0].value;
    MPP.piano.audio.setVolume(v);
    if (window.localStorage) localStorage.volume = v;
    $("#volume-label").text("Volume: " + Math.floor(v * 100) + "%");
});