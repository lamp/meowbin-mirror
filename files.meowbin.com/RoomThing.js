var banned = [];
var whitelist = []; //people that will never be banned
var admins = []; //people that can temp-ban
var masters = []; //people that can perma-ban
//TODO: Make sure everyone is on the whitelist
//Add localStorage to keep lists
//Switch to Node.js
function ban(_id, mins) {
    if (_id !== MPP.client.getOwnParticipant()._id && !whitelist.includes(_id) && !admins.includes(_id)) banBuff.push({m: "kickban", _id: _id, ms: mins*60*1000});
}
function findPeep(text) { //too lazy to prototype it
    var part = false;
    for (var id in MPP.client.ppl) { var p = MPP.client.ppl[id]; if (p.name == text || p._id == text) { part = p; break; } }
        return part;
}

MPP.client.on("a", function(msg) {
    //if (/(.{5,12})\1{9}/.test(msg.a)) ban(msg.p._id); //bans on chat spam, sometimes detects non-spam so disabled
    var args = msg.a.split(" ");
    var cmd = args[0].toLowerCase();
    var text = msg.a.substring(cmd.length).trim();
    if (cmd == "-ban" && admins.includes(msg.p._id)) {
        if (text == "") return;
        var part = findPeep(text);
        if (part) ban(part._id, 7);
    }
    if (cmd == "-longban" && masters.includes(msg.p._id)) {
        if (text == "") return;
        var part = findPeep(text);
        if (part) ban(part._id, 60);
    }
    if (cmd == "-permban" && masters.includes(msg.p._id)) {
        if (text == "") return;
        var part = findPeep(text);
        if (part) { ban(part._id, 60); banned.push(part._id); }
    }
    if (cmd == "-unban" && masters.includes(msg.p._id)) { //only removes from permaban, cont disable kickban times
        if (text == "") return;
        if (banned.includes(text)) delete banned[text];
    }
});
var banBuff = [];
var banInt = setInterval(function() {
    var msg = banBuff.shift();
    if (msg) {
		MPP.client.sendArray([msg]);
		bClient.sendArray([{m: "a", message: "Banned due to "}]); //TODO: add reasons to bans
	}
}, 2100);
var bClient = new Client("ws://www.multiplayerpiano.com:443");
bClient.setChannel("test/awkward");
bClient.start();

MPP.client.on("p", function(msg) {
    var part = MPP.client.ppl[msg.id] || false;
    if (!part) return;
    if (banned.includes(part._id)) ban(part._id, 5);
});