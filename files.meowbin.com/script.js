// ==UserScript==
// @name Custom Script
// @namespace  http://www.multiplayerpiano.com/
// @version    0.0.3
// @description  Custom script for multiplayerpiano.com
// @include http://www.multiplayerpiano.com/*
// @match http://www.multiplayerpiano.com/script.js
// @updateURL http://files.meowbin.com/script.js
// @grant none
// @copyright electrashave 2017+
// ==/UserScript==
var currentFps = 0;
var alwaysRender = true;
var images = {
    black: new Image(),
    white: new Image()
};

images.black.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAEZCAYAAAD/k4dfAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGpVJREFUeNqUXU2O5EZzDSaTZHVPt2Ygyb6CVjqAz+ClL2Stvo23XvgOXnshwIC9MyAY0gVsQIAWlqVptUZTVV0ki/SiO1IvH1+wxw00eqZ+k5nx++JFsPnuu+/Wb775xn744Qfrus6WZbGu6+zjx4/W972ZmV2vVzMza5rG/GdZlvL/dV3LLz7vP+u62jzPZmaWUrJ5ni3nbNfr1aZpMjOzy+Vin3/++ZRyzta2raWULKVUPrRt2/KFKSVrmsZSSuXxtm2r59Z1taZpykL88/x9OWdLKZULa5qmXCj+JPxwfoEvyD/Ur9YXkFKyZVnKY7yDfnG4m/55uJvVgvzF+IV4Fbhr/hr/N752XVdblqU8js/jruPicJH+usQ7gtuMC8Pd8aPxo/aF+OO+OHwP7hTuMi8s5Zzt5ubGxnG0YRiKYPsZ+5ehnPnV+heyvLHgL8ti0zQVwfeL4h0qMuRyxIKodgyv0hfD2uVH5gvtus5yztXnoELgT/bFoMyg0KJg4g6gQKPq4xf4LrIJ4Nfh8aV1Xa3rukqocatZ26o30xX68znnjXq7Bvsp+GMs+AmPqUh6Slvpf3kNL4oFE5Uh+ovfx3/LgpQwRurJx8ryxhfjIoGaeL1eq++sdsgFDs8ehRJdA6o27h5+uLJfLDf++XjB67r+qWV4JPxifyw6DhR0JbjzPFdy5Aud53kjnwl9mG8v/l9Z8sgl+Bf7FyzLUhSj67rNBaDJ8M9N7CpQRVEr+KzxKPBoUI7YHeH71E/TNM9HxtvvQudf5hYbdwTtEMoUOmlWDtbg6/W6VRgUVBZgPiLeKfRRHAfhYlE+/UL9cT6dxKGGOhqWBxWiuNDia3wX0aHiwtXnFaHGK/PVo8fGBXLYgVqFtgZfg2GJ70zOeStfuM0YNuBjHIaoAAu3380G7grLGto5tP4ZNYPVWYUI7HRR09iTuynAwMwXxkdWPk+poa8ctxQXwFrltosX7kfv2sThrxvfjdorf+SLQSvMQX/kWHnB7GJUMFcW6rvh29e2reWcK7VlW8LyxZaaLTtrnnLoVTyEW64sNcuR2hF8LWofx0pRtlHkUAX3vCi2ISgL+FxkkTnecpvFR2ZmltB8R8aKv4RDEP83yhQedRQncUQqtUxdOafD+GUYFeAF+SLRVUSeQGqZchHqTepLOTDjo/VTUNktX3TCWEUlbiqU9b8YUXIqhS4JnafLzjAMNo7j1uJzAM8ZaqQtLG/+GW3bFtPBaRWLBRpGfz5jzo1v5vAhAgo4fuZYil0ShsYukyivaVmWDVSCRlLJUKQtylbx+1A5NlY6pWdLjRiRCrZeCz+jWIrDYozfVV5f1J6FGj04x0HoGNm+sF3h93O04Blu9dw8zxu/opCtSND5Qnjn2KyguVCJaOK8iqE9xhdRUPmD8bg4kcR83x/vum67Qw6XoFZ0XVcE+vb2dnNkuAjcTb5qlQigv1SylNgY8k5wNoLmQFlaBUC4VlXZBUGC/to8TVOI3+BiODlE+8MBvdoVhGUw5t5omUu7G0eGdyNniLulnDEH/pyW4/9xpxJ7eD5b5d84xt4LL1R8hZFAiFMr58rAlMrVXZbUDqK87MVamyNjIxfZGHTEUTircGz0k4zOyjRoD4tmQ8fgJ4ez8qoBwMDFovZtYGHMJNEYcrahAAZ+LQdrqG3oZJUIJPTqKE8seHtgt4qRfUc4C2YAYnP0HGZ4kugv8BIVxzoK1FJW2o+HsaDI46dxHGX6w3LEDhW/HEsQaFB50W7vbm5uSrmBY/CkoBdVOFFIBgu1slfKPOxFE8kDbbzSSGgVnhiFtlGa40fOGcpGqDnWYW1SQToH+VEKjheM8bZMz71Sg05TbTtWHlnWPAxh06BCWgY7N7Lmb3D4hYFslq2oiqOw7Ag98f97gFYZRkxF2INzNKlipqgqzTupnLCUIZQZ5WSjcgJ79CgdR5ukyu2bI/O8jCuLatcUbq2wI8YtMb93WW3b1o7HY5XhlHgIjZaKj7jQG/mwyIZFSsIpfMGHPCNA+YkqjJ+iRYxuICzDQOlmQYhAoEOMPHiUVShrHgl0FNQ1TfOsZWhfIrPOtodhO5QPljEVM83zLLWzBPlKLXGBqCWOYihCitpJlbmqALCUyTlr5Z1ho4fhbPQTFVxULXaTSvsuPT09WUrJpmkyz9cYPFeGz+Wubduq5I6v8SNi8H1TT0N4FhkvSqUZdWWckJWCS6e+4zc3NzZNU7F7uHNZaRPXutRfVWhB7UFDGvlHGTFO01SFsFxwYw/vV6tSGBWiKDB9L/zIHL+wdcVMViWD7BoUsK6wxihtSsonoXpz0sfUC/WF6LfwF7FMdkXFdvmb0anuEVGiyJCNHBObOCpARgWeTFK4IAo140MqeMdFqGiSHW5UPSpHhgVZpz8wlIKugcko+BqG+yICQgQ7p6enJ8s5W9/3dr1eC5zHxBOMl7C0oFJnFY54HuaBvhtiTEqXZXk+sii34poGB1PKnnCY4nLT932JuViDcfeyHxEKmPJbCvlQ/onlw3dbHZlUe5d+LupHbsS/yAMuFXMzDOyV6XmepfZWVt/pgvgBjE4oLghfJTtOXixWpRkL37gOZlOx1qgUiAlQ6t+4WAw1XBk8AqgW5IYx51xJPFpXVavgFJvJBIyoqMxYhbEJj4CBJeZvYFQZ0b5Y5f1xZM8wqaAK0M7nc9kZXByTaxmiU7ERfjHHQ1jC8AqUihASl7SjsEEJoRLuvaiBUyYvi21C2Ag1xR2KULEIrolKo4zqb8AGBJA+hUzAYa0Cs9g5o9XG13pOWCGzEawbVQOVkPvjrsbov7ACHUUFVRqEUv8aCM7PfUoB2AN/Lk1hPITymfcIBKh1KpTA57AEzsLOvi9i4piZZc44FQNmD1xgvpCi5zjk417BwQdMp0uQ//j4WBI8zJWQRup9GHjFWKdQKD7H5B5V+Ps928G8cEP54uhP0bsw2mOaWFTA46qQv34Yhi0FHqvGHNDvUQXZ/vCxc4KIqIp/H8N+L2tJRSA5+ccsgTXRn2Oas4oiGRVRGFKFwnKwzu03UYE3gorVvzksYRC9OHU3UhEorpifCCUrUhMnjOwn/X1sSAvoiVfpks9EJETrGSlTjjOikkXgRuU6FO2UOc+4cx4bMz1M7ajik+wVAtPDw0MFCXM9XRV/UXUxW1H8fFy0K9A0TdZ1nb17966qmZV6GacuUYFFAZiKFRG1hH1KVJFc4hkBUVwPVS2KkgCVobB7YZivBGjX63Vjg1TxRdH9lKYp3FopgtwhTE0wzWWIbq+Ks0cm2GOMKs5I5vI47ghzg9htYKi7RyTgSDJqqKtCWCZcK0Sfax3/H2Yxy5TylyW3V7xEbhxgdY9khll+EaUnUoR8uVzser3a4XAI23Fub29LP6rHNbwoj5mxlBUh/E7EY9CiMM4Zs+EMFn1XVJLC41R1DATV3dIrODkhRMLpy17hVwX8kYYxdSfifVR2CFOaKPhSBG7WFoVp484ho5iBDDP788g4E4iKcRHCGgVleyGt5K0hEZfBgahWugcRq4iBCzguyFhxquwQd8Yx0MApc9S/qAgDis/Pxb9qh7iayD6GQYHXcjWliUjm5QZfGQ95lIjxDQtihHowABWRDTA/83hIoh/IpMJMglU1QuU53uEogaMBxaCQkJ5rmyITYBireEMcLyseP+4SVoVkVRqzA8QBlRpjQ9we6qHQWs9Y2FRU6Acm/WjeFYipSLd7nXlK4FVAR1Y9VXRiZOIpi8tVn6hlXckZdoMqblEVMTpOrTqgkLfBtVc1zUJluPiDGrahyyPbCTVMkXNV6eFToEC8EO+SUAWYdV0tHY/HsgNv3ryx8/lsXdfZOI7FtHOBV9VB/DO89oZf6uMR8GIcc0JsYV3XZzhGdfRy168KHZRrUFEmmhNcPDeKF9AzSm95vgdeoZoRghEg/qAYYIlBhSE5qsdHiZ8ipERBGr+H6WSKjJCxSIKh7B4Cpnj5KjdTTUt4ERhllOe4MSlC6VVyiDmWavmKNBDlh2PxzNS/qLyE9dM9MIGBeEyfonZ4zNUSZxioGWzYIoATd5jLDDxegQWaHXJGzNgtqGM2/px/kNe4EKP2Kx/HseoPYaiwaZpCFnfbxuNbqvIUGzxFpkQB9oQR+w5VYoB2iYFORV1NCNtF9BpekH8oZp0cI6togYctKbuXVT6OTDymVbDzRZyb8zlutcDIgrkmRY65zorChsFYpQkU6irSpLL+eOFs88o6ODSNwG5GwVR/Pddo8TnXLnfYkQlInOoyhBddIROWIv6jyu9cbsO2Um6CVYNq0Gly//On9EHzEXtctKFEY1/ZMAzFNngc0zSNjeNYbIjbKC5ZcryN2PeyLHa5XMrCnEektDop6J+5YkyQRK3gHE5RwRTQELZRsJapVJm7WdgNqCIgC/MeXLMRaj4GxnwiVidbZG4FU918qlG8WpBDbJHlVOkN10JUOsMkbqwo7XYtOADAtoPlwDUNOT8YjkzTtGnKVgRM1aG+YXoqYIlnOTApDtMZFUcrg4lgZziYjeMRtC84stCLtlw9igIuDNwQoOJQdlOVHobBzueztW1rwzBY27bF/piZjeNY4h3fFT86lwdMc7jzwf/vOVjf9xuqRqVQyvQrgVZtOorCpWhhUY+9bF5h4eMwNUJZo/FjjLBxgsljfiSPUXVtRsmjiq/5N/rhMmnA2Ephcz8KOI+D4iQvYn9GDpdHtRR3wiqObkGN+1FpkJIv7kvjiT3RAMqKnPtaT7OqAu3NAWUIBz/fjbGsdexlqIqGs1ege62iyOzkDYJ2OBxsHMcKpej7vozTxBGYmHaz4XutgIeZivcjcWPUuq5/di0o/qKa7amYECzwUS9ZyU5zrkiWlbdnYIDdAFtSRi1Q6OUXkHPdA81LVZrPlD8YiQZ45rxzXGpATgC3ZkQ2q1rQHhe/4s0TxVBVp5m+wSZBdbAXS40vYAaCQjqifkVFU+bwVi1ElslVKTvqPYzGaUTUC85kmXu9W+tQ9Qy2T8qu4PtUQyS35zDkV2To/v7eTqeTzfNs9/f39scff5SYx/mMqnzudS9H5nnaSTTvAS078q5L3R/xHa4aR8U4rrtzXZ/ZVJ44Mu9DjoxSzUgqDGE2C87bm6apSosjph7Lne8wPp8VJq3mxWC2wRiPJPuL8ZiMuCGkV9kh7itUwsrPefWIDV9ExeAfNRO2FPAiCh8Hbuh/9gZvsfVnQMw1kbV7E1MjJ5aPkiEUR1HHcawoY6ocyvZKAe0bsIE7XXjEk++G75IvxntiUX3VoAD0YViAYYVJb9++rYxc3/c2jmPBpD1P67rO5nkuz93e3lrf99a2rd3e3lauJMIPvUbm3zMMQ5Xbrev6XJ7yYSSqcqjmMHBCGM0sYm37lJE/OZrWxQ3WiGXz9itqBffDqkmnSoYyWk01jE+FD+yflK9TFFTu6mPDWOyQCzG27OEvssMrq/pih3Cu+d4wSHc7Cs/ecD9U32DEIFauJvJ97vd4xG806SmzcLIAq3mdqsCH2881OK5E70HJWY2qw2hRtSmrwAyrQ2p0EGtcNJgtDcNgp9Opams4nU5VTqWMnUr8uq6r5uMhboTO2RfQ9/0m40kqoPdt5dLka2PIVFcng6TKelf5295wLCy6KUOHJQlmLihPHnn+6lgx1U0pFQ4IZ6aKp8EMGaWNHPpGjPOqXobEyteqy6rbHHeKtYuLf3uUjeLt/QFv3MZ8fY+TGFKQIf/iLFZBfpKTz7dU4Qm5EXIWxd1Rn1k0wzoc3M/2R+XtEaE7oltE5Dkl6Onu7s4ul0vBbJ6enkpkeDgc7HQ6yVEZTJn3L0aA3e2Q494ea/nFeT4X4kM8o1o1jzB3AzOHKNNQobCKMEpvUETziziu+JeTPTkfj+bnRXNlS17GRo8FUc1kULhkBHRh0dcdMVcqN1kHbilXd9QIXjV+JepmiEhSGElsJqPwwhy2jUBxLt4xlYeNq6pOKgQ3c7sWF/kVVULRUnF6Dtft1fyqKDnNDPerAm10lEw84ISBrfTePYaKeLx9+9ZOp1PBp90o+lV4lIc7wHceaNu2ZLAh2U30sCn0P0dqyjQv5iX6jnEvB8N3EY1598g4jY56OFSn1dPTk7wHlWpE2euC2NRcER7ZG1eo+sNe83EoWxFaskFhMRDjTMNlCsvlHNSpmEnd3onTc96xrLoTVO+ryw2SwRXIqXI5BRtGiH6KBlqzsLkAY+wU1d/3hplEc2E3O8RMF6aAoR2J7gUUVbg5F0MN3aAln332mX38+LGkP9hzcblcClbtsuJQHArqOI7VICXMfLFUej6fZc0EfWXe1M1FvKu8skqf8Iv2GH97I8uyGkqE3XSYKqt8nOuvaiqzqs1GEUFitm40AEkVVKKZi8rQvjZPrcgZR3TqDiSq0qiyjmg+sbLUu+OgI9roa3UzDuxURwuHuzwUN7TU/CGuHe63UFsYjFAlLC7gcLEOizEbX/ba/RCZ28rGkwMu5USje+WJoQHJuNnEeT4+C4TvHBFRR7HRCZ2s6liIyhGZJ3ltEC0xiiUqqav2UX8dThFTDriSIZYj9OR7nQqMlqhYHNVdFew23p4RtKjuFd0RQI3d4C5zRVaILHXiyJBhFDVbmOvzPBWDF4gEcCz8ytF1qjbGrcoq0mMLrO7z4p+JY2BUvFTBN3t3UoqmEET3SkT3wSPLojFl0lLzOUcFEl5g1AigRthxC3RYDbq/vy+W2E27x0U49hu1TgktA/Bsn/z5y+Vifd/bNE12Op22eRn2WiiLrQrBXIVklBZjIuTHqlkxMrdXo8d4ISr2VqNWI8YMj/JQN+UrQT4bMGy3YgeK2LWC+qJ7uPLA0oj5l7lJkgeIqpqXUnmF/TDrSuHfG9SW8ycsh6uZVHsdwBybc1UpSqWr8IebGJnxxD2GGCtFAwEVyYkXgopQTV+OhsuqnArzc2VEo6G2bHx3wYZhGMzM7OnpqeRdjiUj7V2hJEzUZi3CHer7vmDeGDEymz1xBhkNFo3wHJXaRM0B3GrhBcNKhjkLiO7qpnj3yhHzLCNs4YpmgeDfzOevNEG1mvIHqXorLgYpPdHYu2IYcbRP5DR5rLNipatyFfo4Ru85Ra/U3neIb3MQxUeR+1AlJ3VHOB6VUAyjuh2TGqr+2giX6NZiezxImf1GgZki+XP7VtTyx+A5K4y3YKikIN3e3hZs5/7+vuqbR3uBMuBICC5IDTJGmzMMQylnIVcbjWwlQ6oJYC+h26PcqLQJG1N44PGme4r5PXxLVKYMRj1oe4vHqQQKNt5w8lV84zunRtNFsbainmIEoOZgbxJF3EZE6aO7mDKLQXVEcRnBPxt7IHd3SI115kZcxqejDEWN2mA7h20UIU6twAYVpka2hLMS5qopwosscSJxf2+QqALM1e0JVFaK3gBpPBuwwfMvzLu8Rob99ntj6DlgV8iY493TNNnt7W3V9yrbKNR9NPdsD9MpWJj55licSEbuo7qhGoeqaspOtEBVI0HUA+0bmhOJD+0hp9GuqVvLcV7HVUasmUQ9aSkiPqpb0KksIxpYspeB8FC3DfU0unEnE07YOqsWDJUsRjMY1difzFe1d7/EaEpTVB1kB8qomSxPRdlFZHt46/caSvDX28HmeS7qr/o6khuoaZrscDgUwRuGwdZ1tePxuIF00driVB1uv/GFYFfDMAw2z3M1D7sytHv394kYVuoeipxBcJWJnbKC9coAAOZF8yDaaHqAMnwK0VdWPbpnR2IOLJNKIkwwKncrhkPEygvvzuVyE5H6FVtPzcPnOJuzWHURnxQPvUaVUDV+bpRjjVXjYBQvKUVwmyplKqGNOu7Q/iBHiXP+sIAX3fxKLSzy7Cr+3usQljg1EwKwgohn7v3y/gVd1234+G74sGcIAzHs4+f5stX8odfux6pqqaoSFE3x3hsWuLm5zZ6/isIQTJ04oYyGaPPrFGxcaZkSyujOAOoicHFs0bloE2FMFfqhykuqtU8dHYcXTCtUt2CNxgVtkHxuCOBZjSrgivhmCrPmKpH0ZSp0UJUdJZDRTY4iUgIuRjYJvHbTBi6gRPZIlR940Xu9+dXoOjT5p9PJDodD4VX7FfmsNMSX/Rix6wX5+MgvMrNSJ+MbGmGGk1148bZhPJifu5w4qOLIj0tVqm7rhnXTzuUv9O4XTHXZ9yihVZTSqBWeJ6HIQSROCfUdwjf6Y2jyec4MZyDRyDH+XEbsNvMY/ddvEOqLdGxQDVfbu60Kv9bNiYsExulV+6m3h16vV7tcLnY+n63ve7u7uysf4HOHnRGq3IJirLNrUfep3qi9f8nxeLTHx0f7/fffLedsd3d31Y2oVZzEt3CKhj4quI9ZDlV/2bqu9vHjR/vw4UOhELoW4IfzmWPQhXKobh6KPUf+f1WNzG5b3r9/b99++629e/fOfvvtN1vX1Q6Hgx2Pxyq0VSiIX13f91Uy6IvFOYzojPFmtGVBfhzff/+9nU4ne3h4KMNqc852c3Njl8slDB24qZbrGZiFsEPFhrcNO+b9+/f2448/2vl8Lh26PjDE0S4OZ5mOiq0SKgJQN7TZVIOaprm0bWuHw8HO53NJd5Fcwq2gCjxg4iVXery84C3JCGQBMP+/uW3b/zSzvzGz5tdffy3qejweS8/h/f195aE5j4/umcgEcfQAfkFffPGF8/k/DMPwzyml9JeU0n87Gfd8PtuHDx+K3PhNhZHmh/cd878uc0xdZX4I2iBPJsZxPOac/6Xv+39MKaV/NbN/GobhvWuJe2f3/oW58sKcYaop38eTx9EzBOxD3F7+jvM8/3vbtv9wc3PzX826rvb1119/eTwe//Lzzz//3d3dXWtmTdd1zeFwsMfHR7u7u1vmefaBpM3L1V5TSu3zdzybiGmalmmaGjNrcHba+rzCxt3F6XRalmVp+r5fpmn6j6enp7//8ssvv/vpp5+ujV/NV1999Ve//PLL33Zd99cvQtoPw9Bcr9fruq7zNE325s2bLufcPD+0jl3XNU3TdNfrNV2v16Vt2ynnvJpZ/7Lw67quS9M0yziO/csFzOM4TsMw2DAM/3O5XP7t4eHhp8vlspqZ/d8AShVXgZs11I4AAAAASUVORK5CYII=";
images.white.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAAHyCAQAAADSTlFKAAAACXBIWXMAAAsTAAALEwEAmpwYAAADGGlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjaY2BgnuDo4uTKJMDAUFBUUuQe5BgZERmlwH6egY2BmYGBgYGBITG5uMAxIMCHgYGBIS8/L5UBFTAyMHy7xsDIwMDAcFnX0cXJlYE0wJpcUFTCwMBwgIGBwSgltTiZgYHhCwMDQ3p5SUEJAwNjDAMDg0hSdkEJAwNjAQMDg0h2SJAzAwNjCwMDE09JakUJAwMDg3N+QWVRZnpGiYKhpaWlgmNKflKqQnBlcUlqbrGCZ15yflFBflFiSWoKAwMD1A4GBgYGXpf8EgX3xMw8BSMDVQYqg4jIKAUICxE+CDEESC4tKoMHJQODAIMCgwGDA0MAQyJDPcMChqMMbxjFGV0YSxlXMN5jEmMKYprAdIFZmDmSeSHzGxZLlg6WW6x6rK2s99gs2aaxfWMPZ9/NocTRxfGFM5HzApcj1xZuTe4FPFI8U3mFeCfxCfNN45fhXyygI7BD0FXwilCq0A/hXhEVkb2i4aJfxCaJG4lfkaiQlJM8JpUvLS19QqZMVl32llyfvIv8H4WtioVKekpvldeqFKiaqP5UO6jepRGqqaT5QeuA9iSdVF0rPUG9V/pHDBYY1hrFGNuayJsym740u2C+02KJ5QSrOutcmzjbQDtXe2sHY0cdJzVnJRcFV3k3BXdlD3VPXS8Tbxsfd99gvwT//ID6wIlBS4N3hVwMfRnOFCEXaRUVEV0RMzN2T9yDBLZE3aSw5IaUNak30zkyLDIzs+ZmX8xlz7PPryjYVPiuWLskq3RV2ZsK/cqSql01jLVedVPrHzbqNdU0n22VaytsP9op3VXUfbpXta+x/+5Em0mzJ/+dGj/t8AyNmf2zvs9JmHt6vvmCpYtEFrcu+bYsc/m9lSGrTq9xWbtvveWGbZtMNm/ZarJt+w6rnft3u+45uy9s/4ODOYd+Hmk/Jn58xUnrU+fOJJ/9dX7SRe1LR68kXv13fc5Nm1t379TfU75/4mHeY7En+59lvhB5efB1/lv5dxc+NH0y/fzq64Lv4T8Ffp360/rP8f9/AA0ADzT6lvFdAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAACFOSURBVHjazF27kiRZUj3uEdUzs8vDDDNEFBCR2f0JxBX4AT4AiR9AR1wVZQUMBUPiGzDjA5BQwLAFzFjb7dme7q68jpDxuO5+PDKrKrIiaqx7uruyMm/ch/vx48f9yt/aE77BEwDBAIVAMUCmPwkEIL90+ZNg/mrLv6zfub7Outdd3xXTd7V7v+unDVAYBgx4xkd8xBcIxuu35sFdf2yEQKc/y/IB/TD7QV6/a93gDLr8hKafxfQqXX7eMCyfNEDQMGKAYMCICwyjYsCAAQpggE5/Hqe30eVDxM2dpKFeh6kwN5B+kP3v2q2CLv8mAIZp0IrLNBrDeJ29EYDiaRqgTs+xzqR/O3G/+7/Dzfs6YKS5nJfZv5NC0bpxDMB1uYdpzgY8LTOpyxJo+Fjp3hjhu3EQ6460sG2se+h+mMM01AFtGtHzdbl1GvPT9LsuC88OD59LtqjiFh9uG9iyFdS9QgHYtM3mXTpej8cw/TdiXAaq02yyYzKfRVs+Yn37dXdiGqgCaOEBLGwQ6Q4fMK/vgAbDKNBliAPG5ZdOi74OTd1CymJQLCyadd/r/+Zn1OiOnTeCH/603LIs8DAN8amzm/PTWTdQpR+EsOvW7xr8F/vZefeuKwUM+IDxOk9jd3jGaXfqcniUnlBxe07D/rPOasqy3Ov78N0KslqGUZZRq5vNfpASFrRf2N6T9LPV71ULSwoyx+tsIpmzsfctsylfz7tORgF0g4tbWj+b/RCuW8PCpgDZKPNAbfkMhV5nUqZB5bmUyW7Vi+F3oZLlRDE4/yDRRK2f2a4eZ/Wb2rl5mf6mZIHmXzr9nx0Yc3MTB2ppV8fX2rSPDSOWBdUOm2BxSUoWUpZ/sWnOLGyI3pibezQQIz8/0rrH22L8DMBoy0xiQSH97HncY+H08mMg3dCt+3dzhyNaUOsGux4fAaAC/x+6DRvR4ToAI+Z5XSpzQ6qsonT717pXr0s94BkfoNB8HMSZnbjDzA0o7jdJIAPpwdbdaAESWzfn/WPq6lEYVPAzGOFWb5qxzKHd9WC2DHTdItezbGhuraaD4420uCebj44t89Fve3NHxtwhiO4wHp1+C/TvI2kDjEig1dv91Zta96HWHRK/aEbNCbqjxDZRnHc/SPX42YdS3FHFQ5DPMsIh8DMVZ03c969/bt1k4OpxjMxm9DL+ORF+z8OSBWJksyVTbGnEISIYeAOgQxda9ksMsnjiHqefZdB5tO5BzD2elQ7TulWxabi6xnNKHZefRQmzGVG2pI+34O2zCbeAJ2f0OmDAt7jMKHY9HhrsZbR5lobhDXw895I2iYRHjsdnjn6kewCVZRa1GyLIfhMCGyI+lG5wkoCFBfRoYULiv12tpvQmKBtqLKEVSDAgxBaCWsctNMQOoGdNDKop8pXwwjhzQo9Kfy4tQJBsKazDOBXKXK3kdHA07A2hPmMdWP9KSwBYyE9EFG8BVzJz1gd90wu1A22GrS9LphrJ1MTh58czZy1qXDAdHE07ZTWorftgC+7KknHqD4glt1jHNnxHduhhDR96TpL7F5S+JhsmJMPdP+YVMbbiAebX6RztS0E5Gdlh1Sk1Cszqh7K0/BHPhpmUxXxqcnVClgIJjvT8IvP+Wp5jSc4zssETwJBA4DGjgeQboudgUJc/KIq9GLHD+p6KAHeRcIhQkinGKBUIRoC3t62GdDM5URc97+hdGhzAjU7PljMshGvLQa7dwVxIMewRNJgH4WcssRgeq0sR0/D9WPFt1vm1xecYIUwi4hN6To06TXYwZGO5jfzdG76eqE18TA7V4Qw7El70Ib2UZnxrkaU734J2ZXq9l8g/wMk6j4BiWMu4IKOWlm8FW3ig67hGWXhuC35U3H6LHlru8Eb94+idXBAI+6HRB4hzTKB8d56BaF8l8D+cC2JRVX94rEdBkULqUUxvHzkXlIHXW7igPuq29XRbZ3QtnEpLsbIVyGVfLgiR2pYux8pQ+C0uSB7ABTWHNifQ29KW79ktcUsui+WTwCVmlC2FrZCEyY3a6evUjTmAMhcN3+aCkPbt27igHlpcM2K6MtOVR7jFBdnOXJCHbTrbSSNnFC/ggmxXLihbYa3oNluOwS0uSOhgX88FRcQgUCN2KgaiW1yQDzP24IKi45wGuVIa/puS0mpGhiGlx3o9F+QdhV6RBjq7xYLXLS7IHsIFaRfSjP2iGnV021yQ0DP9Vi5oXgtdlxtkSwuNrTMXZA/jgq6Skc9oV9/dpsVtCRsbAWKcQt2bCwr5D29o2VCsYCuEQv/7uCAQ+xzhWkdYrUfHCPgFJUkqovolXNB2/N3RfvOe5LSJEawdHSE7KsyK5se0DcLK43JdX9IC4IzcgwS/axTbZGDH/Hpt6nrl1bInjcZtL2EwhMyNle92H4OxzqL4gyPdOcfdDAaoWkiCQ7yfwRBHfk0MxvVjGyFIGSUgBIRhVwajdQyfTUoYXXF53Cd2F4MhOzMYWAJgx2DM89HIk95mMPAQBsP7rHEdoga6WBKuez8Gw5v1cV75aBUl6Qbem8GwBWRovwsbyTXwuZTgGvdmMDxpps0tcbRztxkM253BsE6FsYQP5r7BCI/3ZTDy8dQ6iOJg7NEMxrztWhdPapxFC/mE92YwWBw+xpht9kBCmXPGZsjODIY/tstyI0R6W25fEszYm8GwcBJsBRgWglrcFegLYTAq3bkkfqky8eZ2rUNBLWT8KkXAoxkMS/p/5YxD/wFbDIY8lMFoK9MLl/iMTvGWmmV/BiMjL+XsWZZicQYjnvG9GAxA8IQRz7igrSioZhRkg8EwimT2U7NMJIEUmFqJUc0MhjyQwTAfiDWC+Yz4DEYb7M1g+PIuXatNLOWkrQASFdH5ejULOzj9++isZgGilpZFNY9Rs/Adaa62TI3E1VLwvnzgr1WzWMHSy4KP5r2toJEzD49QBLn2KjVL/58ApQRlEtS1bskbhQR1lMflIdiEyCgEKNJjniUOF4xIUaE5Wy+Uj/QxtXWmXhMG3dKzSAgcYnncEi0ieV2fzhVnAREgbiW5id+9hxqQjuPtf0pBz2GVOZVCKrKn5MZSkKK9/Cqm6OLwM2GFkrB6veTGA+UpAZpRdab3JVWOPIqwun724OokFYXxMJpAezxhNSsCeyG3Mq7GQ9Rt2n1/wsqSI9UtQkXI/PkAihNWRnkJFlpIEUs1fF3Uk1NIq8RlRVxuRfppb8LKW4WgsGJmCORNEeZwb8KKZTUVm7BTSDa8giD7EFZRE7IYc9A0k6XqTDjyKhOA+xFWnkMZQfN6Ep5UQ5LegmyB1zHdrivrh2md5CZIE7edlBF4y6mlfSU3Xrwygjwbwv5bz6nR2HBvwiruaAXZPzVZwuKcR0ludGGClI2cD5cZoUcRVh4pjD1itNKv+txAjrZrttgDEaZcZ+tmEapVQhCjKNoP9FGElTdJyhUXmbDiZvsRhJWliF7rXWgpuuE12vsTVrHUc+Sn2xI6zPDVB2J7ElblTNoGnwNioIWgv30Iq+wBNS5UlNQJWIGbJOp6L8LK+ybxyabIhcc0pBF3aTsQViiYXsSMWP3hUjaGQEJMryOsQClb/6pU2SSh1sZ7cyMag9cSVqz8Sghb2ZEDLD5mVd9Gc497lV+BoH1gnKMPSdlmjyMlkUsWNoMiihXnE69JPLGNOvtq+UUdLQSCGTUk3JvvW341U9AXxrNJ2A2STIql5lD7ckF+oH1bnkRGs4iRcUHYnQvyU9XtSRafWMlrCzXn+3FBvb68kzdIyclsiZcsQa49uaCYZlDuuSsuiKHpvbkgn7zrO8WghWSZbXBBObm8Hxc0b5G+ZGRcmfHzcEEt9sey4mgcywXdSQ7IQVyQLWzQunFUNs7ZMVzQuukGAJd+ube5INbwzlz+Btir/ApL8ZXktB3Kwp0juKA+pXdyLshcl8ENj3oEF9ScS9Ve2nIOLghOkrbMpJReIaJHoVwQ8JhWPAu/Jifkggxrm0SJ0eJ5uCA/cWqhJuwMXFB0IKOcmAtK5MAWYfS+XFCMr3TlgvIJNVrO8h5cUJxp5USRJae4NxdUlV/FSMGyx4lnVDbiurdyQdvxdy81VYS+IEI2u9HdgqTsrUiEl5ZfRXgyGmQDT97PYHj8HRsPWskFocAKNmUXbW8GY4/yK9ZqRYUs7B4MxmvLr+JZN9hViVpv4/dmMKKoWfp8d0ZBRzEYIAoCrdtIHMdgWNhoKuTgHM9g+D4F49y4t51MzdIfUUWK345mMOYYZ/1XraOYI9Us4oJe5QjoSAYjP/xY47mj1CxZQK61kTiOwYjbbuTKgWMZDD/s1tfj2IkYDA/dVBYGQ07CYEiS4I8vYzDeR80Sy2E19uU9B4PhR3BCBsN3XxYQXdAZGIykZmEtI06rZsFp1Cz9ck9pu2irWP+G92cw/PKrfzJeX21FJvXtapaqbtH3UlZvHizpItgbCbGE+5VfIVHSyuoF2XkELVFFIqjvZTBqcUm+I6qMcfDiGKduIAPKYFjhVhGAtl2VA3sp9nO8HenDe1pM8CpGCiPeHuMwJcDtGKdvmXYddqtB73ExjoTFlu3c4lExTuQ99L4Y5z2ztL6Po8QqkrPEOAgtQkZQjvXoGMe6zXDSLG2PCyZywDbLUI+KcaL2cIP5OjJLKz0KstNlaSMIP2mWVt17njLGibb6pDEOFr8ty42uJ4txEHKaKhTQHp2llVjfnf3L0VnamJfTqJw/PktLCjTMwaEzZGnjCRFORx+dpe2vy5hSOHKyLK0syti2lgTmXOrxWdpFIALDZb7mo65CeP8s7Yx8LgsuL5H5cVla625lLaSJx2dpM+RQzgoem6WNe/iUPQfadL4bi7v3ZTDekqX1D6C8FcGxWVqPJqwPH86UpZ1NeZtzi3KyLO2q1rd4us/GYPRjOCmD0ZaryU/NYAQx/BkZDI9WR2x6haOytM2950l7Dni3cFIGw3ugkzIYqRsYb+Z/D4NRVX/fYjCEUAC1YYNXR7+FwWjYW2fu9JM+AHtslnZuyWkbDIakFVBgbSK8laXdi8FAMOBVlrbPZWgMZl/LYOhdDEZu37jVY3ke2ejNS99zQFKwn7vrM1pEymalOUVtFB1FGzyixClGDG/Vc6AOMLifMXp9eQ9uWrcJRyBfhhLJjlusA8uDV0gSRdjb43B/D4rVTTJf0hiB3WT78iaZ9b07I1Io4H3qaxsjvKZJ5vw5Gq4DVE7RHdcYQZYLpVJTo0wyvZWwesu9tPFYjey8HV1WEDs4aH8RxPFlBRHo2VZS/jjCalWaf8XX9ULnsxFWGR6O0pmQMxBWOvVDRW2CzkFYWWiPcLrGCD2ETgiLMVvHNkZIdpJjvSObZFpG5tXZOoKw8vdopozYmSQ3wBcAwDPU6yfPQViZu797qhE7p+Rm5XuXSvl7Cav3kNxkB6sRN56rSWbX4FpOS1gtSlRJ9xweT1h5YDPGJzxPWYG53ncEsh4pubEpyw1cIHjyyoGzNMkUEj2ib9dhJ2mSiYgnhQzsiCaZFcAzjIxKlxADeryyF2FlHQlgZVhm6yXjtSN8/yaZGSCPSFeUHN0kU7r+dLbaSSuiufdtkikJU3anOy/tUVxQv4aXjqFUNvVHckGSZnJxixX+eV8uSIowRSNnfgYuyBa9GnLcfXyLidUHNSbhtlNwQZE/x5kvTPEHWCVdmHk8FxQ9mhq5N/VoLqjhMvkcXaEaZ4OObTHRn4aRZR+O54L6y2fkjBemeCao61Bn5Cqio7kg88gcGxHGUeKl1k2Q+pT88VyQuLT1tNx2SvGSX9viXtpjuSALFw5oTesf2yRT05/p1B8rXjIfiIEWkh/LBVnMd0tIpR/NBWXIpgy/scV5Hy7IUvBm1z4YfX/hY8uvKjeiq5llN/y+Z/kVs7ILVNNuhwjd7O9XfsVIs+7KVyMG5qjyqzgRyln/o8qvIs4kStRjy6/E/Xy/gcat9gVHqVnigVUOLo4rv2JKN+WE0dHXfHgDqOYuFT6WwWCPJes1HwgNco5iMNaJaXjGAMMzxtj2RE6jZol+51QMBmv/rigc4DEMhrj+LsueFGIXj26SGa2l3kpEHsNg+OhgjD08ztMkczWAivDBZ2Ew+uN3OjWLLPe6jGhQfPDVdudiMHq9n/KI8GgGw58NrfJ8RzMYp2+SGcFfUuyfpYFMTFKTQR3PYIi7jC90RztHA5l08b06me85GshcvXhbyYGtMypFVJgt4esYDEmxUz8tGltMSAK0UWjHtoS8ukkmlsq7Co4Iyz6s7PRLyQFWKFMBh0ynmKt9NDLsAHrbm8gBVln3UnmDumjzmpwfax3P68mBeK5fIm9oLqoPyy2nkDf0cbv2m1DSOTyeHBD0XaSVgflj5Q3Xs9HwFQMMDXrdk4J8n/SR8oYYyGodkRxDDsR1Cvd3n0fe0J9ryfIGnEjesOoQRzaPx9+gAWcptLZYR96g4ZXvo9GyguPvAO1RwknvADX0RZ8n7s3SXdElp5Q3mLO5GhHOmW7QCH165WTkgA8/lCXqzkAOiO8XJCclB1y70deRA9yH7NddVrqrnTX6jhxfW7B1FszHo+4AvcY3w9za0ciZtBTwA/76inzH8ktLXVpBDkSDpVGrvz1vVmT6GyFoDP7m43u7y/o2Kws/KRtJckkn1UguVcgWYC7hvhs0ol5tBDWfRklqI4wwdwGxasfobHG4nKOAcWvia2APBypk48zbZjBbNXT0kzNW8Zvc+AhJIUBOqW9XgAnxdRIuWJBe3iApXq50qRViEZomyjbzNjmA9KAjkh1bORjpoo1GgqTM+8YwpJY/9f3fVx7IX9M9y2Y1L07euHa3aOb+r1kDHeU+jO0co7/mvuNWe/TXDrQKOHxMPnrnZHe+1T5fWyCZJOUFvEzPNkiqt8+j0utekXS95f04tsmi7TeTsmGjb0aLkvqU7j1Aw9Y9i/3lNBrJkpd2A9vr+LCIdB7DaK5xvbdbb+8GhqCtAA3u/NzNr1boZK1HKc7aEd3ApNi1mutwjq4AzeddhUz9sRWgkqL40+knG+mAqTX3fdwl4/FzT1YBGrOQmPnJ/lBUKRJ5UYoEN1s54s57XbrwQTsZ2zlSJDMxcIFhyHvyLBWgKwC3c14yHgPeU17AF0d0wkvGe+OnvOn68SkS39o4kAPnumS8tw0KEoaeIUUSylRtw4sclyKRSA6cMUVi3ndv82NHpUh6e6FcAXWGbmCWW+CeUT8ZyvnPlyJBUvYCJ9NP+kdRScQG0p55f/0k4YIsIUhLEhxmaPfoBiZFimREg2HABZfV4+QzKoQSeIt+ku+3KmnXOnOnvNqQFzLzbvn36ieNJgZ4kOYDGUUAFNnl5/tNJdCcPgPB9ZNCUH0dOvtHoVnaTEEbtqTW2VxHZiMHHj1nLiQf0buGkS+gpL/ZhiVknb9ijGkJrrGtkl1H83bSUplutpCxM4CA1emBROyScjUs/BM67xoBb29ALASovNY2RonZUNvGKZeSX5YuiHAGVhIXwbOMUjK0fljsdoiYmqpQ/hojaIX9jHhq0FQoyjSKEbYzUlu5l2BLc62czr9NwrMEMIOzQjTU5kJ/o6izf0CVktvJqNH7F9nIYjGRfMSbfJWyiwxMbyZKbjX7tfBvksgrCRldIxncOpUv6G5ks5S4qMVhPH9VXZfCqlJAqCsurielLnFfRIAllEeUgjaQZGgsIYDqatSQOduSZyNgRrtxpLwzsPS4HNZascyO5GmEt7GSOO6JVUZFCyVh4pExkmDtKy8EFwxTjTcpdanSnkIewgJRkIco6WQzFM7tKbzHYSZBQssHj5CEUITmpE0gxQq5YypC/iKehaXpOghXG6l1C9oVCxYwPwzTH8Rzm0kxZoBsbV/P7KUUKTsU6TyjAIxVm1WNdTlPqtWLLSWVpQh1GZaXAoBJMF2eJxHHJK+vKovQJbixNbwyijGFGJW6XAukU1VOIXT5bhaaCs0SbHGZnJKxYDWzRIovtrcsCjwXwYGkjEFOGkeTzcxUzIYzZJ6dw4BnCEY8I5VOC5Uf+PBJEidmRK8mNG63oOnLUQHbdmOEu+b2g1L6mV+pIJRyBrGMEXBEhVfy3RVu5J4lKtEkBPrbyfUccQtFsNFX6Zbrl6LtY9xbjHsUGlhYIrf7o2VFmKv3ZW/yR1jnZw1cUickbSLJ229lKQtdEG4m2fPekZSOEhICC3ESEoJXrmMYW8CM3v5rMN5GywSNlGbmrEROqEh4XdVVdIw5w2iCZFOUIKQ4ZZ0joUnU69/aMt+NJEuBC0Y8Q/GErqzgvrsReaaAsRxG8IwUd1UyWoEbQqrAFZrpykl2Th2Y05RKSbrmY9oXn5NCNqFpeaTbJD3+toRuMgFtRVZMAsWTU14aHRkKYgAp91XJHhj1EnM1uOmPkJUDllQXPeEfcYsn/XkZB4/nmcMAoQH9ZClI6CR0FoWqKWSD0MqwjcHBnOWJ8ig1GidWnpVFj0IJFZRS+CojFg2S9LnFVhBWFuBalh5aGGZFXXN6PzbkkK5O0X9/5N1QczDGBHOsxNQQ+1BaoRLa7g/abyWt00DRehlNf7DbuY2gn6h9EVI8J4TnsC1jDgK9JPBgVtwTDXqhjCQWo+7k5uNJBc25xtRkNjUoxbQepAgFere7e0ru5GlFptAKRmgLtCGAWX6dex1GZ5OkjMMVElOjSAsZuBCEZRNvUdFZuEyv1/Q5gpr8i4RV1eiyImqk7NgiZO027qWNUQcjVn0OMVbsSdGVV6h07rr78mV9aZBC3lBuhBQW1GUxc3irLX4VunS3utgGm5Xpewk0H8egUSzK+91YkUvMZk8ZTmYFq/lavAyzrMyZCe5J/ldRgTK5QU6g+ziZ+RWuoYptYY2USDNjt96PQzrUyUYuIh8OSWxDNGNCkiKSIkUGubWno5mNinGMFEiR3QDmP9y3txYSBm8nVRMyzzoABqqk2OQ+qjZqMY24ycqy9O8w+gJmKeReoAIwI3nEClG1ksriMWfDnGEqTJBRMJZbR0UhHNeT3nfPIA910yXjUqZ1qzqGiihAUJLeLijkfIms+W4pgyefJDcS8kbPYoS+wmZmEVSUGA7OVssTTgSw2uOc6ZLNQ5KjH4TqgH49xoir8yxpyFdJuCrSwgwKofO5ZzEaEFtSnyu7jJG1QjFKpOZ+BZJy4FuVTZmC6X9uwZMRz0VYJiEo4GJEDo63D41QQVNsimAoah8i5SGJFef/57HQduBqJW6XwK4F/xnPqRVd7OxGvHw/YmQEGQEYkWyqHKOF2z8FIKVat7+y7EFKJknZLsn3vOTUr4TB1tKubU+TO2BkW6p2I6MtYCpAIT7C8JL6ZSmvJEfC+SobjHWvLOOInGVZWWbsPgReCfL19h4yuk98sp6f2K0CmpwFkmAvG9C3dqwW65YkLCoH5IYPxh2en/VKnPCkdvPChLLS9bFh2opbi4uNmfRRdyMnZMwBQHT/99RpvvzLmzApZxecwRCay977Syj/6dP4bUke0jyA3dGI6O0zyU92XxCYAIa5Kk/fLfdxg5XNlkpWe5x8E7U8dEfmzwxQ7dYT3u/oXrojc6esjNldY4RMEhklnR+56JxJn7RqHu80tCVGttA3+5H7ce1TNPe/MJDbCnKM/NhuIhVhbdvRYjap9i7LjHJfXv32iOTk5IWs7OutJD8svv+y008KDZxuRSm2s5XMImeLkpt7un7cZ4hfaoJA6DCXfdgum37ccnPRaEajthXSvsfXbcQ+NW596SI/xmO3gBr6iGrcKjaXB59uRVWm6XFsoY5+ZBcwhBj0dgWQ4o6g6REzGUutjdKBxtJ2/a0WWwO2nRac8eU9THPtRnO4Wg/GdpjtSFt7JiTyKCNzTy+J89623EbiUt9nBz05kK9BeLyFrCtXPO8+9uJ+K5K/jwdqEnTrHnCr3WEV5aFD3cKTVxiur7WKtoOd5BJPcU1khNeIbafs9pnd+1oLd9JELoR7PNQwwu8KlUaQ/pPvh4E4pZgttd7X6vYxKEg2PFGfMNGcpZZ3ONu22RXHFwMDYwOvTnqs8ZHNcs6ZUbtgUf1tVRnZw/donSRdU1baT77sHG7d9t3YyIgh5hZ9y8t77OQeoPc+hCW5Q529E+i1Mnee79HTe4yNPHQvxuMk6ZfyGwq26JBHnnCheXC9tVcehS+zZNHKzmxjg0AmTtIehiUbVfSbE9FLOb/j9mVmj9iBtrluLUVAE6tW1Sfshy5fFkXFql1de3i8xfjcH7xFiY4R8y0L6dKud5G0h59iPxQhDYr9bM/AYlVQjjX6eYwjtI0YvD9kPQGouY/xe8SJUhi3qFCfbiuIygB96Ok20kKbtWz1Qt3Rllz23AjnesehBbGNkNbWhi0NClz1Yq7eWyse4xVxDR/wBYYnXKAYodcix9YFkFUXAlZ+VRX2MxwV+0FI6NvQk/pt0cg1PEMvmd8vTTC/c8xooWVWU1ZtrmP+owF4npSol9kEtUVSUNeCSRIpZQ/Mq4qZOc/NA7I7UQguuMCgMd+91aWOVSLDiZj6nRjBg9xJJK4784ILGkYoxgu2OQzb7AgwW7VGRXW8HrkljZpNIobmbqW9QDFA8BX6ZRq3LSxvWzZ46068FfdFVzLOWBPPGZJMT6/9bq70qeATxicYdGmfaGhTE5xheuLWtWQ2cjGPTdZhvZzGH6O20SUs60/nCbngAmDAgIbvMX4LwQ/4MS74ihEfJnQpG46LNcfLFXnmVPu3sNGAC75CodNajviM73DBZzT8IcbfRcOA+e7NNg3wGbZcXSvLPdpIZStVdH6PxFM7s3aBYZzG8QWfcMEH/Aa/hy/4jGeMP4JB8DypmxouABQXrNocxSXJjHwILLSsnysCfS9Ac0JSheIrGhRfMOIzBnzBiP/A+IwBX6bi+SfYtBttWgTrZrHaVbEtfW7rIYQQ8MDtuiOfJ8MDCD5C8AUNv4t//zR+wncTuPyKcfrV8DTN3sq8evd4uYM1MjJ3ubmzOa5eYfiCHwD8N34H/wfBB8g/jZ9g+BYDPuLb6dCMGPCMATpdfiflzfJv0Q15B3q1DoonfMYP+C0G/Abf4BMG/BJ/9ovxt5f/Gf4IT/iMhmdc8IwPUDx1Q1T4skujt4dFja7RXczi7rYYbwD4jN/iC36LL/glvuIbfP+/P/nn8c9/9vO/+YM/HWC44Ak/wld8wIBxuUSQwwLbQIysmkEcdPMtE1blZMMP+B6f0PCf+BF+hW/xLX7y93/yVT7jHz78yy+++dkHCIAn/BgfJjM6pJYjlobSiwetJKWs644mCbbMQxzwjO/xa3zEJ/wXfh8f8Gv88eWvfir/KgYAw9/9/N/+8jtc8BEXfIdvccEAYHDZPgu5ld7kX1xFhCwDmv/t6/IoQhrCtuk6tQt+wEd8xGcAA77Brz7/9V/89B+B/x8AzeqwdoDiNTIAAAAASUVORK5CYII=";

$("head").append(`<style>.ugly-button { height: 12px; font-size: 12px; background-color: rgba(170, 187, 170, 0.3); background-image: url("http://files.meowbin.com/test.png"); background-size: 100% 100%; padding: 5px; cursor: pointer; line-height: 12px;
	border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; width: 100px; overflow: hidden; white-space:nowrap; }
.ugly-button:hover { background-color: rgba(187, 204, 170, 0.4) }
.ugly-button.stuck { background-color: rgb(237, 201, 125); }</style>`);
$(".ugly-button").css("border", "0px solid #fff");

var test_mode = (window.location.hash && window.location.hash.match(/^(?:#.+)*#test(?:#.+)*$/i));

var gSeeOwnCursor = (window.location.hash && window.location.hash.match(/^(?:#.+)*#seeowncursor(?:#.+)*$/i));

var gMidiOutTest = (window.location.hash && window.location.hash.match(/^(?:#.+)*#midiout(?:#.+)*$/i)); // todo this is no longer needed

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(elt /*, from*/ ) {
        var len = this.length >>> 0;
        var from = Number(arguments[1]) || 0;
        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0) from += len;
        for (; from < len; from++) {
            if (from in this && this[from] === elt) return from;
        }
        return -1;
    };
}

window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||
    function(cb) {
        setTimeout(cb, 1000 / 30);
    };




var gSoundPath = "/mp3/";
var gSoundExt = ".wav.mp3";

// Yoshify.
if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#Piano_Great_and_Soft(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/216104606/GreatAndSoftPiano/";
    gSoundExt = ".mp3";
}

if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#Piano_Loud_and_Proud(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/216104606/LoudAndProudPiano/";
    gSoundExt = ".mp3";
}

// electrashave
if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#NewPiano(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/258840068/CustomSounds/NewPiano/";
    gSoundExt = ".mp3";
}

// Ethan Walsh
if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#HDPiano(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/258840068/CustomSounds/HDPiano/";
    gSoundExt = ".wav";
}
if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#Harpischord(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/24213061/Harpischord/";
    gSoundExt = ".wav";
}
if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#ClearPiano(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/24213061/ClearPiano/";
    gSoundExt = ".wav";
}

// Alexander Holmfjeld
if ((window.location.hash && window.location.hash.match(/^(?:#.+)*#Klaver(?:#.+)*$/i))) {
    gSoundPath = "https://dl.dropboxusercontent.com/u/70730519/Klaver/";
    gSoundExt = ".wav";
}




var DEFAULT_VELOCITY = 0.5;




var TIMING_TARGET = 1000;




// Utility

////////////////////////////////////////////////////////////////



var Rect = function(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.x2 = x + w;
    this.y2 = y + h;
};
Rect.prototype.contains = function(x, y) {
    return (x >= this.x && x <= this.x2 && y >= this.y && y <= this.y2);
};




// performing translation

////////////////////////////////////////////////////////////////

var Translation = (function() {
    var strings = {
        "people are playing": {
            "pt": "pessoas estão jogando",
            "es": "personas están jugando",
            "ru": "человек играет",
            "fr": "personnes jouent",
            "ja": "人が遊んでいる",
            "de": "Leute spielen",
            "zh": "人被打",
            "nl": "mensen spelen",
            "pl": "osób grają",
            "hu": "ember játszik"
        },
        "New Room...": {
            "pt": "Nova Sala ...",
            "es": "Nueva sala de...",
            "ru": "Новый номер...",
            "ja": "新しい部屋",
            "zh": "新房间",
            "nl": "nieuwe Kamer",
            "hu": "új szoba"
        },
        "room name": {
            "pt": "nome da sala",
            "es": "sala de nombre",
            "ru": "название комнаты",
            "fr": "nom de la chambre",
            "ja": "ルーム名",
            "de": "Raumnamen",
            "zh": "房间名称",
            "nl": "kamernaam",
            "pl": "nazwa pokój",
            "hu": "szoba neve"
        },
        "Visible (open to everyone)": {
            "pt": "Visível (aberto a todos)",
            "es": "Visible (abierto a todo el mundo)",
            "ru": "Visible (открытый для всех)",
            "fr": "Visible (ouvert à tous)",
            "ja": "目に見える（誰にでも開いている）",
            "de": "Sichtbar (offen für alle)",
            "zh": "可见（向所有人开放）",
            "nl": "Zichtbaar (open voor iedereen)",
            "pl": "Widoczne (otwarte dla wszystkich)",
            "hu": "Látható (nyitott mindenki számára)"
        },
        "Enable Chat": {
            "pt": "Ativar bate-papo",
            "es": "Habilitar chat",
            "ru": "Включить чат",
            "fr": "Activer discuter",
            "ja": "チャットを有効にする",
            "de": "aktivieren Sie chatten",
            "zh": "启用聊天",
            "nl": "Chat inschakelen",
            "pl": "Włącz czat",
            "hu": "a csevegést"
        },
        "Play Alone": {
            "pt": "Jogar Sozinho",
            "es": "Jugar Solo",
            "ru": "Играть в одиночку",
            "fr": "Jouez Seul",
            "ja": "一人でプレイ",
            "de": "Alleine Spielen",
            "zh": "独自玩耍",
            "nl": "Speel Alleen",
            "pl": "Zagraj sam",
            "hu": "Játssz egyedül"
        }
        // todo: it, tr, th, sv, ar, fi, nb, da, sv, he, cs, ko, ro, vi, id, nb, el, sk, bg, lt, sl, hr
        // todo: Connecting, Offline mode, input placeholder, Notifications
    };

    var setLanguage = function(lang) {
        language = lang
    };

    var getLanguage = function() {
        if (window.navigator && navigator.language && navigator.language.length >= 2) {
            return navigator.language.substr(0, 2).toLowerCase();
        } else {
            return "en";
        }
    };

    var get = function(text, lang) {
        if (typeof lang === "undefined") lang = language;
        var row = strings[text];
        if (row == undefined) return text;
        var string = row[lang];
        if (string == undefined) return text;
        return string;
    };

    var perform = function(lang) {
        if (typeof lang === "undefined") lang = language;
        $(".translate").each(function(i, ele) {
            var th = $(this);
            if (ele.tagName && ele.tagName.toLowerCase() == "input") {
                if (typeof ele.placeholder != "undefined") {
                    th.attr("placeholder", get(th.attr("placeholder"), lang))
                }
            } else {
                th.text(get(th.text(), lang));
            }
        });
    };

    var language = getLanguage();

    return {
        setLanguage: setLanguage,
        getLanguage: getLanguage,
        get: get,
        perform: perform
    };
})();

Translation.perform();




// AudioEngine classes

////////////////////////////////////////////////////////////////

var AudioEngine = function() {};

AudioEngine.prototype.init = function(cb) {
    this.volume = 0.6;
    this.sounds = {};
    return this;
};

AudioEngine.prototype.load = function(id, url, cb) {};

AudioEngine.prototype.play = function() {};

AudioEngine.prototype.stop = function() {};

AudioEngine.prototype.setVolume = function(vol) {
    this.volume = vol;
};


AudioEngineWeb = function() {
    this.threshold = 1000;
    this.worker = new Worker("/workerTimer.js");
    var self = this;
    this.worker.onmessage = function(event) {
        if (event.data.args)
            if (event.data.args.action == 0) {
                self.actualPlay(event.data.args.id, event.data.args.vol, event.data.args.time, event.data.args.part_id);
            }
        else {
            self.actualStop(event.data.args.id, event.data.args.time, event.data.args.part_id);
        }
    }
};

AudioEngineWeb.prototype = new AudioEngine();

AudioEngineWeb.prototype.init = function(cb) {
    AudioEngine.prototype.init.call(this);

    this.context = new AudioContext();

    this.masterGain = this.context.createGain();
    this.masterGain.connect(this.context.destination);
    this.masterGain.gain.value = this.volume;

    this.limiterNode = this.context.createDynamicsCompressor();
    this.limiterNode.threshold.value = -10;
    this.limiterNode.knee.value = 0;
    this.limiterNode.ratio.value = 20;
    this.limiterNode.attack.value = 0;
    this.limiterNode.release.value = 0.1;
    this.limiterNode.connect(this.masterGain);

    // for synth mix
    this.pianoGain = this.context.createGain();
    this.pianoGain.gain.value = 0.5;
    this.pianoGain.connect(this.limiterNode);
    this.synthGain = this.context.createGain();
    this.synthGain.gain.value = 0.5;
    this.synthGain.connect(this.limiterNode);

    this.playings = {};

    if (cb) setTimeout(cb, 0);
    return this;
};

AudioEngineWeb.prototype.load = function(id, url, cb) {
    var audio = this;
    var req = new XMLHttpRequest();
    req.open("GET", url);
    req.responseType = "arraybuffer";
    req.addEventListener("readystatechange", function(evt) {
        if (req.readyState !== 4) return;
        try {
            audio.context.decodeAudioData(req.response, function(buffer) {
                audio.sounds[id] = buffer;
                if (cb) cb();
            });
        } catch (e) {
            /*throw new Error(e.message
            	+ " / id: " + id
            	+ " / url: " + url
            	+ " / status: " + req.status
            	+ " / ArrayBuffer: " + (req.response instanceof ArrayBuffer)
            	+ " / byteLength: " + (req.response && req.response.byteLength ? req.response.byteLength : "undefined"));*/
            new MPPNotification({
                id: "audio-download-error",
                title: "Problem",
                text: "For some reason, an audio download failed with a status of " + req.status + ". ",
                target: "#piano",
                duration: 10000
            });
        }
    });
    req.send();
};

AudioEngineWeb.prototype.actualPlay = function(id, vol, time, part_id) { //the old play(), but with time insted of delay_ms.
    if (!this.sounds.hasOwnProperty(id)) return;
    var source = this.context.createBufferSource();
    source.buffer = this.sounds[id];
    var gain = this.context.createGain();
    gain.gain.value = vol;
    source.connect(gain);
    gain.connect(this.pianoGain);
    source.start(time);
    // Patch from ste-art remedies stuttering under heavy load
    if (this.playings[id]) {
        var playing = this.playings[id];
        playing.gain.gain.setValueAtTime(playing.gain.gain.value, time);
        playing.gain.gain.linearRampToValueAtTime(0.0, time + 0.2);
        playing.source.stop(time + 0.21);
        if (enableSynth && playing.voice) {
            playing.voice.stop(time);
        }
    }
    this.playings[id] = {
        "source": source,
        "gain": gain,
        "part_id": part_id
    };

    if (enableSynth) {
        this.playings[id].voice = new synthVoice(id, time);
    }
}

AudioEngineWeb.prototype.play = function(id, vol, delay_ms, part_id) {
    if (!this.sounds.hasOwnProperty(id)) return;
    var time = this.context.currentTime + (delay_ms / 1000); //calculate time on note receive.
    var delay = delay_ms - this.threshold;
    if (delay <= 0) this.actualPlay(id, vol, time, part_id);
    else {
        this.worker.postMessage({
            delay: delay,
            args: {
                action: 0 /*play*/ ,
                id: id,
                vol: vol,
                time: time,
                part_id: part_id
            }
        }); // but start scheduling right before play.
    }
}

AudioEngineWeb.prototype.actualStop = function(id, time, part_id) {
    if (this.playings.hasOwnProperty(id) && this.playings[id] && this.playings[id].part_id === part_id) {
        var gain = this.playings[id].gain.gain;
        gain.setValueAtTime(gain.value, time);
        gain.linearRampToValueAtTime(gain.value * 0.1, time + 0.16);
        gain.linearRampToValueAtTime(0.0, time + 0.4);
        this.playings[id].source.stop(time + 0.41);


        if (this.playings[id].voice) {
            this.playings[id].voice.stop(time);
        }

        this.playings[id] = null;
    }
};

AudioEngineWeb.prototype.stop = function(id, delay_ms, part_id) {
    var time = this.context.currentTime + (delay_ms / 1000);
    var delay = delay_ms - this.threshold;
    if (delay <= 0) this.actualStop(id, time, part_id);
    else {
        this.worker.postMessage({
            delay: delay,
            args: {
                action: 1 /*stop*/ ,
                id: id,
                time: time,
                part_id: part_id
            }
        });
    }
};

AudioEngineWeb.prototype.setVolume = function(vol) {
    AudioEngine.prototype.setVolume.call(this, vol);
    this.masterGain.gain.value = this.volume;
};




// VolumeSlider inst

////////////////////////////////////////////////////////////////

var VolumeSlider = function(ele, cb) {
    this.rootElement = ele;
    this.cb = cb;
    var range = document.createElement("input");
    try {
        range.type = "range";
    } catch (e) {
        // hello, IE9
    }
    if (range.min !== undefined) {
        this.range = range;
        this.rootElement.appendChild(range);
        range.className = "volume-slider";
        range.min = "0.0";
        range.max = "1.0";
        range.step = "0.01";
        $(range).on("change", function(evt) {
            cb(range.value);
        });
    } else {
        if (window.console) console.log("warn: no slider");
        // todo
    }
};

VolumeSlider.prototype.set = function(v) {
    if (this.range !== undefined) {
        this.range.value = v;
    } else {
        // todo
    }
};




// Renderer classes

////////////////////////////////////////////////////////////////

var Renderer = function() {};

Renderer.prototype.init = function(piano) {
    this.piano = piano;
    this.resize();
    return this;
};

Renderer.prototype.resize = function(width, height) {
    if (typeof width == "undefined") width = $(this.piano.rootElement).width();
    if (typeof height == "undefined") height = Math.floor(width * 0.2);
    $(this.piano.rootElement).css({
        "height": height + "px",
        marginTop: Math.floor($(window).height() / 2 - height / 2) + "px"
    });
    this.width = width;
    this.height = height;
};

Renderer.prototype.visualize = function(key, color) {};




var DOMRenderer = function() {
    Renderer.call(this);
};

DOMRenderer.prototype = new Renderer();

DOMRenderer.prototype.init = function(piano) {
    // create keys in dom
    for (var i in piano.keys) {
        if (!piano.keys.hasOwnProperty(i)) continue;
        var key = piano.keys[i];
        var ele = document.createElement("div");
        key.domElement = ele;
        piano.rootElement.appendChild(ele);
        // "key sharp cs cs2"
        ele.note = key.note;
        ele.id = key.note;
        ele.className = "key " + (key.sharp ? "sharp " : " ") + key.baseNote + " " + key.note + " loading";
        var table = $('<table width="100%" height="100%" style="pointer-events:none"></table>');
        var td = $('<td valign="bottom"></td>');
        table.append(td);
        td.valign = "bottom";
        $(ele).append(table);
    }
    // add event listeners
    var mouse_down = false;
    $(piano.rootElement).mousedown(function(event) {
        // todo: IE10 doesn't support the pointer-events css rule on the "blips"
        var ele = event.target;
        if ($(ele).hasClass("key") && piano.keys.hasOwnProperty(ele.note)) {
            var key = piano.keys[ele.note];
            press(key.note);
            mouse_down = true;
            event.stopPropagation();
        };
        //event.preventDefault();
    });
    piano.rootElement.addEventListener("touchstart", function(event) {
        for (var i in event.changedTouches) {
            var ele = event.changedTouches[i].target;
            if ($(ele).hasClass("key") && piano.keys.hasOwnProperty(ele.note)) {
                var key = piano.keys[ele.note];
                press(key.note);
                mouse_down = true;
                event.stopPropagation();
            }
        }
        //event.preventDefault();
    }, false);
    $(window).mouseup(function(event) {
        mouse_down = false;
    });
    /*$(piano.rootElement).mouseover(function(event) {
    	if(!mouse_down) return;
    	var ele = event.target;
    	if($(ele).hasClass("key") && piano.keys.hasOwnProperty(ele.note)) {
    		var key = piano.keys[ele.note];
    		press(key.note);
    	}
    });*/

    Renderer.prototype.init.call(this, piano);
    return this;
};

DOMRenderer.prototype.resize = function(width, height) {
    Renderer.prototype.resize.call(this, width, height);
};

DOMRenderer.prototype.visualize = function(key, color) {
    var k = $(key.domElement);
    k.addClass("play");
    setTimeout(function() {
        k.removeClass("play");
    }, 100);
    // "blips"
    var d = $('<div style="width:100%;height:10%;margin:0;padding:0">&nbsp;</div>');
    d.css("background", color);
    k.find("td").append(d);
    d.fadeOut(1000, function() {
        d.remove();
    });
};


//testing
var frameSkip = 4;
var frameCount = 0;
var maxFps = 20;
var currentFps;
var lastCall;
//testing
var CanvasRenderer = function() {
    Renderer.call(this);
};

CanvasRenderer.prototype = new Renderer();

CanvasRenderer.prototype.init = function(piano) {
    this.canvas = document.createElement("canvas");
    this.ctx = this.canvas.getContext("2d");
    piano.rootElement.appendChild(this.canvas);

    Renderer.prototype.init.call(this, piano); // calls resize()

    // create render loop
    var self = this;
    var render = function() {
        self.redraw();
        requestAnimationFrame(render);
        if (!lastCall) {
            lastCall = Date.now();
            currentFps = 0;
            return;
        }
        var time = (Date.now() - lastCall)/1000;
        lastCall = Date.now();
        currentFps = 1/time;
    };
    requestAnimationFrame(render);

    // add event listeners
    var mouse_down = false;
    var last_key = null;
    $(piano.rootElement).mousedown(function(event) {
        mouse_down = true;
        //event.stopPropagation();
        event.preventDefault();

        var pos = CanvasRenderer.translateMouseEvent(event);
        var hit = self.getHit(pos.x, pos.y);
        if (hit) {
            press(hit.key.note, hit.v);
            last_key = hit.key;
        }
    });
    piano.rootElement.addEventListener("touchstart", function(event) {
        mouse_down = true;
        //event.stopPropagation();
        event.preventDefault();
        for (var i in event.changedTouches) {
            var pos = CanvasRenderer.translateMouseEvent(event.changedTouches[i]);
            var hit = self.getHit(pos.x, pos.y);
            if (hit) {
                press(hit.key.note, hit.v);
                last_key = hit.key;
            }
        }
    }, false);
    $(window).mouseup(function(event) {
        if (last_key) {
            release(last_key.note);
        }
        mouse_down = false;
        last_key = null;
    });
    /*$(piano.rootElement).mousemove(function(event) {
    	if(!mouse_down) return;
    	var pos = CanvasRenderer.translateMouseEvent(event);
    	var hit = self.getHit(pos.x, pos.y);
    	if(hit && hit.key != last_key) {
    		press(hit.key.note, hit.v);
    		last_key = hit.key;
    	}
    });*/

    return this;
};

CanvasRenderer.prototype.resize = function(width, height) {
    if (images.white)
        Renderer.prototype.resize.call(this, width, height);
    if (this.width < 52 * 2) this.width = 52 * 2;
    if (this.height < this.width * 0.2) this.height = Math.floor(this.width * 0.18);
    this.canvas.width = this.width;
    this.canvas.height = this.height;

    // calculate key sizes
    this.whiteKeyWidth = Math.floor(this.width / 52);
    this.whiteKeyHeight = Math.floor(this.height * 0.9);
    this.blackKeyWidth = Math.floor(this.whiteKeyWidth * 0.75);
    this.blackKeyHeight = Math.floor(this.height * 0.5);

    this.blackKeyOffset = Math.floor(this.whiteKeyWidth - (this.blackKeyWidth / 2));
    this.keyMovement = Math.floor(this.whiteKeyHeight * 0.015);

    this.whiteBlipWidth = Math.floor(this.whiteKeyWidth * 0.7);
    this.whiteBlipHeight = Math.floor(this.whiteBlipWidth * 0.8);
    this.whiteBlipX = Math.floor((this.whiteKeyWidth - this.whiteBlipWidth) / 2);
    this.whiteBlipY = Math.floor(this.whiteKeyHeight - this.whiteBlipHeight * 1.2);
    this.blackBlipWidth = Math.floor(this.blackKeyWidth * 0.7);
    this.blackBlipHeight = Math.floor(this.blackBlipWidth * 0.8);
    this.blackBlipY = Math.floor(this.blackKeyHeight - this.blackBlipHeight * 1.2);
    this.blackBlipX = Math.floor((this.blackKeyWidth - this.blackBlipWidth) / 2);

    // prerender white key
    this.whiteKeyRender = document.createElement("canvas");
    this.whiteKeyRender.width = this.whiteKeyWidth;
    this.whiteKeyRender.height = this.height + 10;
    var tempWhite = document.createElement("canvas");
    tempWhite.width = this.whiteKeyWidth;
    tempWhite.height = this.whiteKeyHeight;
    var tCtx = tempWhite.getContext("2d");
    tCtx.drawImage(images.white, 0, 0, images.white.width, images.white.height, 0, 0, tempWhite.width, tempWhite.height);
    var ctx = this.whiteKeyRender.getContext("2d");
    ctx.clearRect(0, 0, tempWhite.width, tempWhite.height);
    ctx.fillStyle = ctx.createPattern(tempWhite, "repeat");
    ctx.beginPath();
    ctx.rect(0, 0, tempWhite.width, tempWhite.height);
    ctx.fill();

    // prerender black key
    this.blackKeyRender = document.createElement("canvas");
    this.blackKeyRender.width = this.blackKeyWidth + 10;
    this.blackKeyRender.height = this.blackKeyHeight + 10;
    var tempBlack = document.createElement("canvas");
    tempBlack.width = this.blackKeyWidth;
    tempBlack.height = this.blackKeyHeight;
    var tCtx = tempBlack.getContext("2d");
    tCtx.drawImage(images.black, 0, 0, images.black.width, images.black.height, 0, 0, tempBlack.width, tempBlack.height);
    var ctx = this.blackKeyRender.getContext("2d");
    ctx.clearRect(0, 0, tempBlack.width, tempBlack.height);
    ctx.fillStyle = ctx.createPattern(tempBlack, "repeat");
    ctx.beginPath();
    ctx.rect(0, 0, tempBlack.width, tempBlack.height);
    ctx.fill();

    // prerender shadows
    this.shadowRender = [];
    var y = -this.canvas.height * 2;
    for (var j = 0; j < 2; j++) {
        var canvas = document.createElement("canvas");
        this.shadowRender[j] = canvas;
        canvas.width = this.canvas.width;
        canvas.height = this.canvas.height;
        var ctx = canvas.getContext("2d");
        var sharp = j ? true : false;
        ctx.lineJoin = "round";
        ctx.lineCap = "round";
        ctx.lineWidth = 4;
        ctx.shadowColor = "rgba(0, 0, 0, 0.5)";
        ctx.shadowBlur = this.keyMovement * 3;
        ctx.shadowOffsetY = -y + this.keyMovement;
        if (sharp) {
            ctx.shadowOffsetX = this.keyMovement;
        } else {
            ctx.shadowOffsetX = 0;
            ctx.shadowOffsetY = -y + this.keyMovement;
        }
        for (var i in this.piano.keys) {
            if (!this.piano.keys.hasOwnProperty(i)) continue;
            var key = this.piano.keys[i];
            if (key.sharp != sharp) continue;

            if (key.sharp) {
                ctx.fillRect(this.blackKeyOffset + this.whiteKeyWidth * key.spatial + ctx.lineWidth / 2,
                    y + ctx.lineWidth / 2,
                    this.blackKeyWidth - ctx.lineWidth, this.blackKeyHeight - ctx.lineWidth);
            } else {
                ctx.fillRect(this.whiteKeyWidth * key.spatial + ctx.lineWidth / 2,
                    y + ctx.lineWidth / 2,
                    this.whiteKeyWidth - ctx.lineWidth, this.whiteKeyHeight - ctx.lineWidth);
            }
        }
    }

    // update key rects
    for (var i in this.piano.keys) {
        if (!this.piano.keys.hasOwnProperty(i)) continue;
        var key = this.piano.keys[i];
        if (key.sharp) {
            key.rect = new Rect(this.blackKeyOffset + this.whiteKeyWidth * key.spatial, 0,
                this.blackKeyWidth, this.blackKeyHeight);
        } else {
            key.rect = new Rect(this.whiteKeyWidth * key.spatial, 0,
                this.whiteKeyWidth, this.whiteKeyHeight);
        }
    }
};

CanvasRenderer.prototype.visualize = function(key, color) {
    key.timePlayed = Date.now();
    key.blips.push({
        "time": key.timePlayed,
        "color": color
    });
};

CanvasRenderer.prototype.redraw = function() {
    var now = Date.now();
    var timeLoadedEnd = now - 1000;
    var timePlayedEnd = now - 100;
    var timeBlipEnd = now - 1000;

    this.ctx.save();
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // draw all keys
    for (var j = 0; j < 2; j++) {
        this.ctx.globalAlpha = 1.0;
        this.ctx.drawImage(this.shadowRender[j], 0, 0);
        var sharp = j ? true : false;
        for (var i in this.piano.keys) {
            if (!this.piano.keys.hasOwnProperty(i)) continue;
            var key = this.piano.keys[i];
            if (key.sharp != sharp) continue;

            if (!key.loaded) {
                this.ctx.globalAlpha = 0.2;
            } else if (key.timeLoaded > timeLoadedEnd) {
                this.ctx.globalAlpha = ((now - key.timeLoaded) / 1000) * 0.8 + 0.2;
            } else {
                this.ctx.globalAlpha = 1.0;
            }
            var y = 0;
            if (key.timePlayed > timePlayedEnd) {
                y = Math.floor(this.keyMovement - (((now - key.timePlayed) / 100) * this.keyMovement));
            }
            var x = Math.floor(key.sharp ? this.blackKeyOffset + this.whiteKeyWidth * key.spatial :
                this.whiteKeyWidth * key.spatial);
            var image = key.sharp ? this.blackKeyRender : this.whiteKeyRender;
            this.ctx.drawImage(image, x, y);

            // render blips
            if (key.blips.length) {
                var alpha = this.ctx.globalAlpha;
                var w, h;
                if (key.sharp) {
                    x += this.blackBlipX;
                    y = this.blackBlipY;
                    w = this.blackBlipWidth;
                    h = this.blackBlipHeight;
                } else {
                    x += this.whiteBlipX;
                    y = this.whiteBlipY;
                    w = this.whiteBlipWidth;
                    h = this.whiteBlipHeight;
                }
                for (var b = 0; b < key.blips.length; b++) {
                    var blip = key.blips[b];
                    if (blip.time > timeBlipEnd) {
                        this.ctx.fillStyle = blip.color;
                        this.ctx.globalAlpha = alpha - ((now - blip.time) / 1000);
                        this.ctx.fillRect(x, y, w, h);
                    } else {
                        key.blips.splice(b, 1);
                        --b;
                    }
                    y -= Math.floor(h * 1.1);
                }
            }
        }
    }
    this.ctx.restore();
};

CanvasRenderer.prototype.getHit = function(x, y) {
    for (var j = 0; j < 2; j++) {
        var sharp = j ? false : true; // black keys first
        for (var i in this.piano.keys) {
            if (!this.piano.keys.hasOwnProperty(i)) continue;
            var key = this.piano.keys[i];
            if (key.sharp != sharp) continue;
            if (key.rect.contains(x, y)) {
                var v = y / (key.sharp ? this.blackKeyHeight : this.whiteKeyHeight);
                v += 0.25;
                v *= DEFAULT_VELOCITY;
                if (v > 1.0) v = 1.0;
                return {
                    "key": key,
                    "v": v
                };
            }
        }
    }
    return null;
};


CanvasRenderer.isSupported = function() {
    var canvas = document.createElement("canvas");
    return !!(canvas.getContext && canvas.getContext("2d"));
};

CanvasRenderer.translateMouseEvent = function(evt) {
    var element = evt.target;
    var offx = 0;
    var offy = 0;
    do {
        if (!element) break; // wtf, wtf?
        offx += element.offsetLeft;
        offy += element.offsetTop;
    } while (element = element.offsetParent);
    return {
        x: evt.pageX - offx,
        y: evt.pageY - offy
    }
};




// Pianoctor

////////////////////////////////////////////////////////////////

var PianoKey = function(note, octave) {
    this.note = note + octave;
    this.baseNote = note;
    this.octave = octave;
    this.sharp = note.substring(1, 2) == "s";
    this.loaded = false;
    this.timeLoaded = 0;
    this.domElement = null;
    this.timePlayed = 0;
    this.blips = [];
};

var Piano = function(rootElement) {

    var piano = this;
    piano.rootElement = rootElement;
    piano.keys = {};

    var white_spatial = 0;
    var black_spatial = 0;
    var black_it = 0;
    var black_lut = [2, 1, 2, 1, 1];
    var addKey = function(note, octave) {
        var key = new PianoKey(note, octave);
        piano.keys[key.note] = key;
        if (key.sharp) {
            key.spatial = black_spatial;
            black_spatial += black_lut[black_it % 5];
            ++black_it;
        } else {
            key.spatial = white_spatial;
            ++white_spatial;
        }
    }
    if (test_mode) {
        addKey("c", 2);
    } else {
        addKey("a", -1);
        addKey("as", -1);
        addKey("b", -1);
        var notes = "c cs d ds e f fs g gs a as b".split(" ");
        for (var oct = 0; oct < 7; oct++) {
            for (var i in notes) {
                addKey(notes[i], oct);
            }
        }
        addKey("c", 7);
    }


    var render_engine = CanvasRenderer.isSupported() ? CanvasRenderer : DOMRenderer;
    this.renderer = new render_engine().init(this);

    window.addEventListener("resize", function() {
        piano.renderer.resize();
    });


    window.AudioContext = window.AudioContext || window.webkitAudioContext || undefined;
    var audio_engine = AudioEngineWeb;

    this.audio = new audio_engine().init(function() {
        for (var i in piano.keys) {
            if (!piano.keys.hasOwnProperty(i)) continue;
            (function() {
                var key = piano.keys[i];
                /*piano.audio.load(key.note, gSoundPath + key.note + gSoundExt, function() {
                    key.loaded = true;
                    key.timeLoaded = Date.now();
                    if (key.domElement) // todo: move this to renderer somehow
                        $(key.domElement).removeClass("loading");
                });*/
            })();
        }
    });
};

Piano.prototype.play = function(note, vol, participant, delay_ms) {
    if (!this.keys.hasOwnProperty(note)) return;
    var key = this.keys[note];
    if (key.loaded) this.audio.play(key.note, vol, delay_ms, participant.id);
    if (typeof gMidiOutTest === "function") gMidiOutTest(key.note, vol * 100, delay_ms);
    var self = this;
    var jq_namediv = $(typeof participant == "undefined" ? null : participant.nameDiv);
    if (jq_namediv) {
        setTimeout(function() {
            self.renderer.visualize(key, typeof participant == "undefined" ? "yellow" : (participant.color || "#777"));
            jq_namediv.addClass("play");
            setTimeout(function() {
                jq_namediv.removeClass("play");
            }, 30);
        }, delay_ms);
    }
};

Piano.prototype.stop = function(note, participant, delay_ms) {
    if (!this.keys.hasOwnProperty(note)) return;
    var key = this.keys[note];
    if (key.loaded) this.audio.stop(key.note, delay_ms, participant.id);
    if (typeof gMidiOutTest === "function") gMidiOutTest(key.note, 0, delay_ms);
};

var gPiano = new Piano(document.getElementById("piano"));




var gAutoSustain = true; //!(window.location.hash && window.location.hash.match(/^(?:#.+)*#sustain(?:#.+)*$/));
var gSustain = false;

var gHeldNotes = {};
var gSustainedNotes = {};


function press(id, vol) {
    if (!gClient.preventsPlaying() && gNoteQuota.spend(1)) {
        gHeldNotes[id] = true;
        gSustainedNotes[id] = true;
        gPiano.play(id, vol !== undefined ? vol : DEFAULT_VELOCITY, gClient.getOwnParticipant(), 0);
        gClient.startNote(id, vol);
    }
}

function release(id) {
    if (gHeldNotes[id]) {
        gHeldNotes[id] = false;
        if ((gAutoSustain || gSustain) && !enableSynth) {
            gSustainedNotes[id] = true;
        } else {
            if (gNoteQuota.spend(1)) {
                gPiano.stop(id, gClient.getOwnParticipant(), 0);
                gClient.stopNote(id);
                gSustainedNotes[id] = false;
            }
        }
    }
}

function pressSustain() {
    gSustain = true;
}

function releaseSustain() {
    gSustain = false;
    if (!gAutoSustain) {
        for (var id in gSustainedNotes) {
            if (gSustainedNotes.hasOwnProperty(id) && gSustainedNotes[id] && !gHeldNotes[id]) {
                gSustainedNotes[id] = false;
                if (gNoteQuota.spend(1)) {
                    gPiano.stop(id, gClient.getOwnParticipant(), 0);
                    gClient.stopNote(id);
                }
            }
        }
    }
}




// internet science

////////////////////////////////////////////////////////////////

var channel_id = decodeURIComponent(window.location.pathname);
if (channel_id.substr(0, 1) == "/") channel_id = channel_id.substr(1);
if (channel_id == "") channel_id = "lobby";

var wssport = window.location.hostname == "www.multiplayerpiano.com" ? 443 : 8080;
var gClient = new Client("ws://" + window.location.hostname + ":" + wssport);
gClient.setChannel(channel_id);
gClient.start();


// Setting status
(function() {
    gClient.on("status", function(status) {
        $("#status").text(status);
    });
    gClient.on("count", function(count) {
        if (count > 0) {
            $("#status").html('<span class="number">' + count + '</span> ' + (count == 1 ? 'person is' : 'people are') + ' playing');
            document.title = "Piano (" + count + ")";
        } else {
            document.title = "Multiplayer Piano";
        }
    });
})();

// Handle changes to participants
(function() {
    gClient.on("participant added", function(part) {

        part.displayX = 150;
        part.displayY = 50;

        // add nameDiv
        var div = document.createElement("div");
        div.className = "name";
        div.participantId = part.id;
        div.textContent = part.name || "";
        div.style.backgroundColor = part.color || "#777";
        if (gClient.participantId === part.id) {
            $(div).addClass("me");
        }
        if (gClient.channel && gClient.channel.crown && gClient.channel.crown.participantId === part.id) {
            $(div).addClass("owner");
        }
        if (gPianoMutes.indexOf(part._id) !== -1) {
            $(part.nameDiv).addClass("muted-notes");
        }
        if (gChatMutes.indexOf(part._id) !== -1) {
            $(part.nameDiv).addClass("muted-chat");
        }
        div.style.display = "none";
        part.nameDiv = $("#names")[0].appendChild(div);
        $(part.nameDiv).fadeIn(2000);

        // sort names
        var arr = $("#names .name");
        arr.sort(function(a, b) {
            a = a.style.backgroundColor; // todo: sort based on user id instead
            b = b.style.backgroundColor;
            if (a > b) return 1;
            else if (a < b) return -1;
            else return 0;
        });
        $("#names").html(arr);

        // add cursorDiv
        if (gClient.participantId !== part.id || gSeeOwnCursor) {
            var div = document.createElement("div");
            div.className = "cursor";
            div.style.display = "none";
            part.cursorDiv = $("#cursors")[0].appendChild(div);
            $(part.cursorDiv).fadeIn(2000);

            var div = document.createElement("div");
            div.className = "name";
            div.style.backgroundColor = part.color || "#777"
            div.textContent = part.name || "";
            part.cursorDiv.appendChild(div);

        } else {
            part.cursorDiv = undefined;
        }
    });
    gClient.on("participant removed", function(part) {
        // remove nameDiv
        var nd = $(part.nameDiv);
        var cd = $(part.cursorDiv);
        cd.fadeOut(2000);
        nd.fadeOut(2000, function() {
            nd.remove();
            cd.remove();
            part.nameDiv = undefined;
            part.cursorDiv = undefined;
        });
    });
    gClient.on("participant update", function(part) {
        var name = part.name || "";
        var color = part.color || "#777";
        part.nameDiv.style.backgroundColor = color;
        part.nameDiv.textContent = name;
        $(part.cursorDiv)
            .find(".name")
            .text(name)
            .css("background-color", color);
    });
    gClient.on("ch", function(msg) {
        for (var id in gClient.ppl) {
            if (gClient.ppl.hasOwnProperty(id)) {
                var part = gClient.ppl[id];
                if (part.id === gClient.participantId) {
                    $(part.nameDiv).addClass("me");
                } else {
                    $(part.nameDiv).removeClass("me");
                }
                if (msg.ch.crown && msg.ch.crown.participantId === part.id) {
                    $(part.nameDiv).addClass("owner");
                    $(part.cursorDiv).addClass("owner");
                } else {
                    $(part.nameDiv).removeClass("owner");
                    $(part.cursorDiv).removeClass("owner");
                }
                if (gPianoMutes.indexOf(part._id) !== -1) {
                    $(part.nameDiv).addClass("muted-notes");
                } else {
                    $(part.nameDiv).removeClass("muted-notes");
                }
                if (gChatMutes.indexOf(part._id) !== -1) {
                    $(part.nameDiv).addClass("muted-chat");
                } else {
                    $(part.nameDiv).removeClass("muted-chat");
                }
            }
        }
    });
})();


// Handle changes to crown
(function() {
    var jqcrown = $('<div id="crown"></div>').appendTo(document.body).hide();
    var jqcountdown = $('<span></span>').appendTo(jqcrown);
    var countdown_interval;
    jqcrown.click(function() {
        gClient.sendArray([{
            m: "chown",
            id: gClient.participantId
        }]);
    });
    gClient.on("ch", function(msg) {
        if (msg.ch.crown) {
            var crown = msg.ch.crown;
            if (!crown.participantId || !gClient.ppl[crown.participantId]) {
                var land_time = crown.time + 2000 - gClient.serverTimeOffset;
                var avail_time = crown.time + 15000 - gClient.serverTimeOffset;
                jqcountdown.text("");
                jqcrown.show();
                if (land_time - Date.now() <= 0) {
                    jqcrown.css({
                        "left": crown.endPos.x + "%",
                        "top": crown.endPos.y + "%"
                    });
                } else {
                    jqcrown.css({
                        "left": crown.startPos.x + "%",
                        "top": crown.startPos.y + "%"
                    });
                    jqcrown.addClass("spin");
                    jqcrown.animate({
                        "left": crown.endPos.x + "%",
                        "top": crown.endPos.y + "%"
                    }, 2000, "linear", function() {
                        jqcrown.removeClass("spin");
                    });
                }
                clearInterval(countdown_interval);
                countdown_interval = setInterval(function() {
                    var time = Date.now();
                    if (time >= land_time) {
                        var ms = avail_time - time;
                        if (ms > 0) {
                            jqcountdown.text(Math.ceil(ms / 1000) + "s");
                        } else {
                            jqcountdown.text("");
                            clearInterval(countdown_interval);
                        }
                    }
                }, 1000);
            } else {
                jqcrown.hide();
            }
        } else {
            jqcrown.hide();
        }
    });
    gClient.on("disconnect", function() {
        jqcrown.fadeOut(2000);
    });
})();


// Playing notes
gClient.on("n", function(msg) {
    var t = msg.t - gClient.serverTimeOffset + TIMING_TARGET - Date.now();
    var participant = gClient.findParticipantById(msg.p);
    if (gPianoMutes.indexOf(participant._id) !== -1)
        return;
    for (var i = 0; i < msg.n.length; i++) {
        var note = msg.n[i];
        var ms = t + (note.d || 0);
        if (ms < 0) {
            ms = 0;
        } else if (ms > 10000) continue;
        if (note.s) {
            gPiano.stop(note.n, participant, ms);
        } else {
            var vel = (typeof note.v !== "undefined") ? parseFloat(note.v) : DEFAULT_VELOCITY;
            if (vel < 0) vel = 0;
            else if (vel > 1) vel = 1;
            gPiano.play(note.n, vel, participant, ms);
            if (enableSynth) {
                gPiano.stop(note.n, participant, ms + 1000);
            }
        }
    }
});

// Send cursor updates
var mx = 0,
    last_mx = -10,
    my = 0,
    last_my = -10;
setInterval(function() {
    if (Math.abs(mx - last_mx) > 0.1 || Math.abs(my - last_my) > 0.1) {
        last_mx = mx;
        last_my = my;
        gClient.sendArray([{
            m: "m",
            x: mx,
            y: my
        }]);
        var part = gClient.getOwnParticipant();
        if (part) {
            part.x = mx;
            part.y = my;
        }
    }
}, 50);
$(document).mousemove(function(event) {
    mx = ((event.pageX / $(window).width()) * 100).toFixed(2);
    my = ((event.pageY / $(window).height()) * 100).toFixed(2);
});

// Animate cursors
setInterval(function() {
    for (var id in gClient.ppl) {
        if (!gClient.ppl.hasOwnProperty(id)) continue;
        var part = gClient.ppl[id];
        if (part.cursorDiv && (Math.abs(part.x - part.displayX) > 0.1 || Math.abs(part.y - part.displayY) > 0.1)) {
            part.displayX += (part.x - part.displayX) * 0.75;
            part.displayY += (part.y - part.displayY) * 0.75;
            part.cursorDiv.style.left = part.displayX + "%";
            part.cursorDiv.style.top = part.displayY + "%";
        }
    }
}, 50);


// Room settings button
(function() {
    gClient.on("ch", function(msg) {
        if (gClient.isOwner()) {
            $("#room-settings-btn").show();
        } else {
            $("#room-settings-btn").hide();
        }
    });
    $("#room-settings-btn").click(function(evt) {
        if (gClient.channel && gClient.isOwner()) {
            var settings = gClient.channel.settings;
            openModal("#room-settings");
            setTimeout(function() {
                $("#room-settings .checkbox[name=visible]").prop("checked", settings.visible);
                $("#room-settings .checkbox[name=chat]").prop("checked", settings.chat);
                $("#room-settings .checkbox[name=crownsolo]").prop("checked", settings.crownsolo);
                $("#room-settings input[name=color]").val(settings.color);
            }, 100);
        }
    });
    $("#room-settings .submit").click(function() {
        var settings = {
            visible: $("#room-settings .checkbox[name=visible]").is(":checked"),
            chat: $("#room-settings .checkbox[name=chat]").is(":checked"),
            crownsolo: $("#room-settings .checkbox[name=crownsolo]").is(":checked"),
            color: $("#room-settings input[name=color]").val()
        };
        gClient.sendArray([{
            m: "chset",
            set: settings
        }]);
        closeModal();
    });
    $("#room-settings .drop-crown").click(function() {
        gClient.sendArray([{
            m: "chown"
        }]);
        closeModal();
    });
})();

// Handle notifications
gClient.on("notification", function(msg) {
    new MPPNotification(msg);
});

// Don't foget spin
gClient.on("ch", function(msg) {
    var chidlo = msg.ch._id.toLowerCase();
    if (chidlo === "spin" || chidlo.substr(-5) === "/spin") {
        $("#piano").addClass("spin");
    } else {
        $("#piano").removeClass("spin");
    }
});

/*function eb() {
	if(gClient.channel && gClient.channel._id.toLowerCase() === "test/fishing") {
		ebsprite.start(gClient);
	} else {
		ebsprite.stop();
	}
}
if(ebsprite) {
	gClient.on("ch", eb);
	eb();
}*/

// Crownsolo notice
gClient.on("ch", function(msg) {
    if (msg.ch.settings.crownsolo) {
        if ($("#crownsolo-notice").length == 0) {
            $('<div id="crownsolo-notice">').text('This room is set to "only the owner can play."').appendTo("body").fadeIn(1000);
        }
    } else {
        $("#crownsolo-notice").remove();
    }
});
gClient.on("disconnect", function() {
    $("#crownsolo-notice").remove();
});


// Background color
(function() {
    var old_color1 = new Color("#3b5054");
    var old_color2 = new Color("#3b5054");

    function setColor(hex) {
        var color1 = new Color(hex);
        var color2 = new Color(hex);
        color2.add(-0x40, -0x40, -0x40);

        var bottom = document.getElementById("bottom");

        var duration = 500;
        var step = 0;
        var steps = 30;
        var step_ms = duration / steps;
        var difference = new Color(color1.r, color1.g, color1.b);
        difference.r -= old_color1.r;
        difference.g -= old_color1.g;
        difference.b -= old_color1.b;
        var inc = new Color(difference.r / steps, difference.g / steps, difference.b / steps);
        var iv;
        iv = setInterval(function() {
            old_color1.add(inc.r, inc.g, inc.b);
            old_color2.add(inc.r, inc.g, inc.b);
            document.body.style.background = "radial-gradient(ellipse at center, " + old_color1.toHexa() + " 0%," + old_color2.toHexa() + " 100%)";
            bottom.style.background = old_color2.toHexa();
            if (++step >= steps) {
                clearInterval(iv);
                old_color1 = color1;
                old_color2 = color2;
                document.body.style.background = "radial-gradient(ellipse at center, " + color1.toHexa() + " 0%," + color2.toHexa() + " 100%)";
                bottom.style.background = color2.toHexa();
            }
        }, step_ms);
    }

    setColor("#3b5054");

    gClient.on("ch", function(ch) {
        if (ch.ch.settings) {
            if (ch.ch.settings.color) {
                setColor(ch.ch.settings.color);
            } else {
                setColor("#3b5054");
            }
        }
    });
})();




var gPianoMutes = [];

var gChatMutes = [];




var volume_slider = new VolumeSlider(document.getElementById("volume"), function(v) {
    gPiano.audio.setVolume(v);
    if (window.localStorage) localStorage.volume = v;
});
volume_slider.set(gPiano.audio.volume);

var Note = function(note, octave) {
    this.note = note;
    this.octave = octave || 0;
};



var n = function(a, b) {
    return {
        note: new Note(a, b),
        held: false
    };
};
var key_binding = {
    65: n("gs"),
    90: n("a"),
    83: n("as"),
    88: n("b"),
    67: n("c", 1),
    70: n("cs", 1),
    86: n("d", 1),
    71: n("ds", 1),
    66: n("e", 1),
    78: n("f", 1),
    74: n("fs", 1),
    77: n("g", 1),
    75: n("gs", 1),
    188: n("a", 1),
    76: n("as", 1),
    190: n("b", 1),
    191: n("c", 2),
    222: n("cs", 2),

    49: n("gs", 1),
    81: n("a", 1),
    50: n("as", 1),
    87: n("b", 1),
    69: n("c", 2),
    52: n("cs", 2),
    82: n("d", 2),
    53: n("ds", 2),
    84: n("e", 2),
    89: n("f", 2),
    55: n("fs", 2),
    85: n("g", 2),
    56: n("gs", 2),
    73: n("a", 2),
    57: n("as", 2),
    79: n("b", 2),
    80: n("c", 3),
    189: n("cs", 3),
    219: n("d", 3),
    187: n("ds", 3),
    221: n("e", 3)
};

var capsLockKey = false;

var transpose_octave = 0;

function handleKeyDown(evt) {
    //console.log(evt);
    var code = parseInt(evt.keyCode);
    if (key_binding[code] !== undefined) {
        var binding = key_binding[code];
        if (!binding.held) {
            binding.held = true;

            var note = binding.note;
            var octave = 1 + note.octave + transpose_octave;
            if (evt.shiftKey) ++octave;
            else if (capsLockKey || evt.ctrlKey) --octave;
            note = note.note + octave;
            var vol = velocityFromMouseY();
            press(note, vol);
        }

        if (++gKeyboardSeq == 3) {
            gKnowsYouCanUseKeyboard = true;
            if (window.gKnowsYouCanUseKeyboardTimeout) clearTimeout(gKnowsYouCanUseKeyboardTimeout);
            if (localStorage) localStorage.knowsYouCanUseKeyboard = true;
            if (window.gKnowsYouCanUseKeyboardNotification) gKnowsYouCanUseKeyboardNotification.close();
        }

        evt.preventDefault();
        evt.stopPropagation();
        return false;
    } else if (code == 20) { // Caps Lock
        capsLockKey = true;
        evt.preventDefault();
    } else if (code === 0x20) { // Space Bar
        pressSustain();
        evt.preventDefault();
    } else if ((code === 38 || code === 39) && transpose_octave < 3) {
        ++transpose_octave;
    } else if ((code === 40 || code === 37) && transpose_octave > -2) {
        --transpose_octave;
    } else if (code == 9) { // Tab (don't tab away from the piano)
        evt.preventDefault();
    } else if (code == 8) { // Backspace (don't navigate Back)
        gAutoSustain = !gAutoSustain;
        evt.preventDefault();
    }
};

function handleKeyUp(evt) {
    var code = parseInt(evt.keyCode);
    if (key_binding[code] !== undefined) {
        var binding = key_binding[code];
        if (binding.held) {
            binding.held = false;

            var note = binding.note;
            var octave = 1 + note.octave + transpose_octave;
            if (evt.shiftKey) ++octave;
            else if (capsLockKey || evt.ctrlKey) --octave;
            note = note.note + octave;
            release(note);
        }

        evt.preventDefault();
        evt.stopPropagation();
        return false;
    } else if (code == 20) { // Caps Lock
        capsLockKey = false;
        evt.preventDefault();
    } else if (code === 0x20) { // Space Bar
        releaseSustain();
        evt.preventDefault();
    }
};

function handleKeyPress(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    if (evt.keyCode == 27 || evt.keyCode == 13) {
        //$("#chat input").focus();
    }
    return false;
};

var recapListener = function(evt) {
    captureKeyboard();
};

function captureKeyboard() {
    $("#piano").off("mousedown", recapListener);
    $("#piano").off("touchstart", recapListener);
    $(document).on("keydown", handleKeyDown);
    $(document).on("keyup", handleKeyUp);
    $(window).on("keypress", handleKeyPress);
};

function releaseKeyboard() {
    $(document).off("keydown", handleKeyDown);
    $(document).off("keyup", handleKeyUp);
    $(window).off("keypress", handleKeyPress);
    $("#piano").on("mousedown", recapListener);
    $("#piano").on("touchstart", recapListener);
};

captureKeyboard();


var velocityFromMouseY = function() {
    return 0.1 + (my / 100) * 0.6;
};




// NoteQuota
var gNoteQuota = (function() {
    var last_rat = 0;
    var nqjq = $("#quota .value");
    setInterval(function() {
        gNoteQuota.tick();
    }, 2000);
    return new NoteQuota(function(points) {
        // update UI
        var rat = (points / this.max) * 100;
        if (rat <= last_rat)
            nqjq.stop(true, true).css("width", rat.toFixed(0) + "%");
        else
            nqjq.stop(true, true).animate({
                "width": rat.toFixed(0) + "%"
            }, 2000, "linear");
        last_rat = rat;
    });
})();
gClient.on("nq", function(nq_params) {
    gNoteQuota.setParams(nq_params);
});
gClient.on("disconnect", function() {
    gNoteQuota.setParams(NoteQuota.PARAMS_OFFLINE);
});



// click participant names
(function() {
    var ele = document.getElementById("names");
    var touchhandler = function(e) {
        var target_jq = $(e.target);
        if (target_jq.hasClass("name")) {
            target_jq.addClass("play");
            if (e.target.participantId == gClient.participantId) {
                openModal("#rename", "input[name=name]");
                setTimeout(function() {
                    $("#rename input[name=name]").val(gClient.ppl[gClient.participantId].name);
                    $("#rename input[name=color]").val(gClient.ppl[gClient.participantId].color);
                }, 100);
            } else if (e.target.participantId) {
                var id = e.target.participantId;
                var part = gClient.ppl[id] || null;
                if (part) {
                    participantMenu(part);
                    e.stopPropagation();
                }
            }
        }
    };
    ele.addEventListener("mousedown", touchhandler);
    ele.addEventListener("touchstart", touchhandler);
    var releasehandler = function(e) {
        $("#names .name").removeClass("play");
    };
    document.body.addEventListener("mouseup", releasehandler);
    document.body.addEventListener("touchend", releasehandler);

    var removeParticipantMenus = function() {
        $(".participant-menu").remove();
        $(".participantSpotlight").hide();
        document.removeEventListener("mousedown", removeParticipantMenus);
        document.removeEventListener("touchstart", removeParticipantMenus);
    };

    var participantMenu = function(part) {
        if (!part) return;
        removeParticipantMenus();
        document.addEventListener("mousedown", removeParticipantMenus);
        document.addEventListener("touchstart", removeParticipantMenus);
        $("#" + part.id).find(".enemySpotlight").show();
        var menu = $('<div class="participant-menu"></div>');
        $("body").append(menu);
        // move menu to name position
        var jq_nd = $(part.nameDiv);
        var pos = jq_nd.position();
        menu.css({
            "top": pos.top + jq_nd.height() + 15,
            "left": pos.left + 6,
            "background": part.color || "black"
        });
        menu.on("mousedown touchstart", function(evt) {
            evt.stopPropagation();
            var target = $(evt.target);
            if (target.hasClass("menu-item")) {
                target.addClass("clicked");
                menu.fadeOut(200, function() {
                    removeParticipantMenus();
                });
            }
        });
        // this spaces stuff out but also can be used for informational
        $('<div class="info"></div>').appendTo(menu).text(part._id);
        // add menu items
        if (gPianoMutes.indexOf(part._id) == -1) {
            $('<div class="menu-item">Mute Notes</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    gPianoMutes.push(part._id);
                    $(part.nameDiv).addClass("muted-notes");
                });
        } else {
            $('<div class="menu-item">Unmute Notes</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    var i;
                    while ((i = gPianoMutes.indexOf(part._id)) != -1)
                        gPianoMutes.splice(i, 1);
                    $(part.nameDiv).removeClass("muted-notes");
                });
        }
        if (gChatMutes.indexOf(part._id) == -1) {
            $('<div class="menu-item">Mute Chat</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    gChatMutes.push(part._id);
                    $(part.nameDiv).addClass("muted-chat");
                });
        } else {
            $('<div class="menu-item">Unmute Chat</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    var i;
                    while ((i = gChatMutes.indexOf(part._id)) != -1)
                        gChatMutes.splice(i, 1);
                    $(part.nameDiv).removeClass("muted-chat");
                });
        }
        if (!(gPianoMutes.indexOf(part._id) >= 0) || !(gChatMutes.indexOf(part._id) >= 0)) {
            $('<div class="menu-item">Mute Completely</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    gPianoMutes.push(part._id);
                    gChatMutes.push(part._id);
                    $(part.nameDiv).addClass("muted-notes");
                    $(part.nameDiv).addClass("muted-chat");
                });
        }
        if ((gPianoMutes.indexOf(part._id) >= 0) || (gChatMutes.indexOf(part._id) >= 0)) {
            $('<div class="menu-item">Unmute Completely</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    var i;
                    while ((i = gPianoMutes.indexOf(part._id)) != -1)
                        gPianoMutes.splice(i, 1);
                    while ((i = gChatMutes.indexOf(part._id)) != -1)
                        gChatMutes.splice(i, 1);
                    $(part.nameDiv).removeClass("muted-notes");
                    $(part.nameDiv).removeClass("muted-chat");
                });
        }
        if (gClient.isOwner()) {
            $('<div class="menu-item give-crown">Give Crown</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    gClient.sendArray([{
                        m: "chown",
                        id: part.id
                    }]);
                });
            $('<div class="menu-item kickban">Kickban</div>').appendTo(menu)
                .on("mousedown touchstart", function(evt) {
                    var minutes = prompt("How many minutes? (0-60)", "30");
                    if (minutes === null) return;
                    minutes = parseFloat(minutes) || 0;
                    var ms = minutes * 60 * 1000;
                    gClient.sendArray([{
                        m: "kickban",
                        _id: part._id,
                        ms: ms
                    }]);
                });
        }
        menu.fadeIn(100);
    };
})();




// Notification class

////////////////////////////////////////////////////////////////

var MPPNotification = function(par) {
    EventEmitter.call(this);

    var par = par || {};

    this.id = "Notification-" + (par.id || Math.random());
    this.title = par.title || "";
    this.text = par.text || "";
    this.html = par.html || "";
    this.target = $(par.target || "#piano");
    this.duration = par.duration || 30000;
    this["class"] = par["class"] || "classic";

    var self = this;
    var eles = $("#" + this.id);
    if (eles.length > 0) {
        eles.remove();
    }
    this.domElement = $('<div class="notification"><div class="notification-body"><div class="title"></div>' +
        '<div class="text"></div></div><div class="x">x</div></div>');
    this.domElement[0].id = this.id;
    this.domElement.addClass(this["class"]);
    this.domElement.find(".title").text(this.title);
    if (this.text.length > 0) {
        this.domElement.find(".text").text(this.text);
    } else if (this.html instanceof HTMLElement) {
        this.domElement.find(".text")[0].appendChild(this.html);
    } else if (this.html.length > 0) {
        this.domElement.find(".text").html(this.html);
    }
    document.body.appendChild(this.domElement.get(0));

    this.position();
    this.onresize = function() {
        self.position();
    };
    window.addEventListener("resize", this.onresize);

    this.domElement.find(".x").click(function() {
        self.close();
    });

    if (this.duration > 0) {
        setTimeout(function() {
            self.close();
        }, this.duration);
    }

    return this;
}

mixin(MPPNotification.prototype, EventEmitter.prototype);
MPPNotification.prototype.constructor = MPPNotification;

MPPNotification.prototype.position = function() {
    var pos = this.target.offset();
    var x = pos.left - (this.domElement.width() / 2) + (this.target.width() / 4);
    var y = pos.top - this.domElement.height() - 8;
    var width = this.domElement.width();
    if (x + width > $("body").width()) {
        x -= ((x + width) - $("body").width());
    }
    if (x < 0) x = 0;
    this.domElement.offset({
        left: x,
        top: y
    });
};

MPPNotification.prototype.close = function() {
    var self = this;
    window.removeEventListener("resize", this.onresize);
    this.domElement.fadeOut(500, function() {
        self.domElement.remove();
        self.emit("close");
    });
};




// set variables from settings or set settings

////////////////////////////////////////////////////////////////

var gKeyboardSeq = 0;
var gKnowsYouCanUseKeyboard = false;
if (localStorage && localStorage.knowsYouCanUseKeyboard) gKnowsYouCanUseKeyboard = true;
if (!gKnowsYouCanUseKeyboard) {
    window.gKnowsYouCanUseKeyboardTimeout = setTimeout(function() {
        window.gKnowsYouCanUseKeyboardNotification = new MPPNotification({
            title: "Did you know!?!",
            text: "You can play the piano with your keyboard, too.  Try it!",
            target: "#piano",
            duration: 10000
        });
    }, 30000);
}




if (window.localStorage) {

    if (localStorage.volume) {
        volume_slider.set(localStorage.volume);
        gPiano.audio.setVolume(localStorage.volume);
    } else localStorage.volume = gPiano.audio.volume;

    window.gHasBeenHereBefore = (localStorage.gHasBeenHereBefore || false);
    if (gHasBeenHereBefore) {}
    localStorage.gHasBeenHereBefore = true;

}




// New room, change room

////////////////////////////////////////////////////////////////

$("#room > .info").text("--");
gClient.on("ch", function(msg) {
    var channel = msg.ch;
    var info = $("#room > .info");
    info.text(channel._id);
    if (channel.settings.lobby) info.addClass("lobby");
    else info.removeClass("lobby");
    if (!channel.settings.chat) info.addClass("no-chat");
    else info.removeClass("no-chat");
    if (channel.settings.crownsolo) info.addClass("crownsolo");
    else info.removeClass("crownsolo");
    if (!channel.settings.visible) info.addClass("not-visible");
    else info.removeClass("not-visible");
});
gClient.on("ls", function(ls) {
    for (var i in ls.u) {
        if (!ls.u.hasOwnProperty(i)) continue;
        var room = ls.u[i];
        var info = $("#room .info[roomname=\"" + (room._id + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0') + "\"]");
        if (info.length == 0) {
            info = $("<div class=\"info\"></div>");
            info.attr("roomname", room._id);
            $("#room .more").append(info);
        }
        info.text(room._id + " (" + room.count + ")");
        if (room.settings.lobby) info.addClass("lobby");
        else info.removeClass("lobby");
        if (!room.settings.chat) info.addClass("no-chat");
        else info.removeClass("no-chat");
        if (room.settings.crownsolo) info.addClass("crownsolo");
        else info.removeClass("crownsolo");
        if (!room.settings.visible) info.addClass("not-visible");
        else info.removeClass("not-visible");
    }
});
$("#room").on("click", function(evt) {
    evt.stopPropagation();

    // clicks on a new room
    if ($(evt.target).hasClass("info") && $(evt.target).parents(".more").length) {
        $("#room .more").fadeOut(250);
        var selected_name = $(evt.target).attr("roomname");
        if (typeof selected_name != "undefined") {
            changeRoom(selected_name, "right");
        }
        return false;
    }
    // clicks on "New Room..."
    else if ($(evt.target).hasClass("new")) {
        openModal("#new-room", "input[name=name]");
    }
    // all other clicks
    var doc_click = function(evt) {
        if ($(evt.target).is("#room .more")) return;
        $(document).off("mousedown", doc_click);
        $("#room .more").fadeOut(250);
        gClient.sendArray([{
            m: "-ls"
        }]);
    }
    $(document).on("mousedown", doc_click);
    $("#room .more .info").remove();
    $("#room .more").show();
    gClient.sendArray([{
        m: "+ls"
    }]);
});
$("#new-room-btn").on("click", function(evt) {
    evt.stopPropagation();
    openModal("#new-room", "input[name=name]");
});


$("#play-alone-btn").on("click", function(evt) {
    evt.stopPropagation();
    var room_name = "Room" + Math.floor(Math.random() * 1000000000000);
    changeRoom(room_name, "right", {
        "visible": false,
        "chat": true,
        "crownsolo": false
    });
    setTimeout(function() {
        new MPPNotification({
            id: "share",
            title: "Playing alone",
            html: 'You are playing alone in a room by yourself, but you can always invite \
				friends by sending them the link.<br/><br/>\
				<a href="#" onclick="window.open(\'https://www.facebook.com/sharer/sharer.php?u=\'+encodeURIComponent(location.href),\'facebook-share-dialog\',\'width=626,height=436\');return false;">Share on Facebook</a><br/><br/>\
				<a href="http://twitter.com/home?status=' + encodeURIComponent(location.href) + '" target="_blank">Tweet</a>',
            duration: 5000
        });
    }, 1000);
});



var gModal;

function modalHandleEsc(evt) {
    if (evt.keyCode == 27) {
        closeModal();
        evt.preventDefault();
        evt.stopPropagation();
    }
};

function openModal(selector, focus) {
    chat.blur();
    releaseKeyboard();
    $(document).on("keydown", modalHandleEsc);
    $("#modal #modals > *").hide();
    $("#modal").fadeIn(250);
    $(selector).show();
    setTimeout(function() {
        $(selector).find(focus).focus();
    }, 100);
    gModal = selector;
};

function closeModal() {
    $(document).off("keydown", modalHandleEsc);
    $("#modal").fadeOut(100);
    $("#modal #modals > *").hide();
    captureKeyboard();
    gModal = null;
};

var modal_bg = $("#modal .bg")[0];
$(modal_bg).on("click", function(evt) {
    if (evt.target != modal_bg) return;
    closeModal();
});

(function() {
    function submit() {
        var name = $("#new-room .text[name=name]").val();
        var settings = {
            visible: $("#new-room .checkbox[name=visible]").is(":checked"),
            chat: true,
            crownsolo: false
        };
        $("#new-room .text[name=name]").val("");
        closeModal();
        changeRoom(name, "right", settings);
        setTimeout(function() {
            new MPPNotification({
                id: "share",
                title: "Created a Room",
                html: 'You can invite friends to your room by sending them the link.<br/><br/>\
				<a href="#" onclick="window.open(\'https://www.facebook.com/sharer/sharer.php?u=\'+encodeURIComponent(location.href),\'facebook-share-dialog\',\'width=626,height=436\');return false;">Share on Facebook</a><br/><br/>\
				<a href="http://twitter.com/home?status=' + encodeURIComponent(location.href) + '" target="_blank">Tweet</a>',
                duration: 5000
            });
        }, 1000);
    };
    $("#new-room .submit").click(function(evt) {
        submit();
    });
    $("#new-room .text[name=name]").keypress(function(evt) {
        if (evt.keyCode == 13) {
            submit();
        } else if (evt.keyCode == 27) {
            closeModal();
        } else {
            return;
        }
        evt.preventDefault();
        evt.stopPropagation();
        return false;
    });
})();




function changeRoom(name, direction, settings, push) {
    if (!settings) settings = {};
    if (!direction) direction = "right";
    if (typeof push == "undefined") push = true;
    var opposite = direction == "left" ? "right" : "left";

    if (name == "" || name == "lobby") name = "lolwutsecretlobbybackdoor";
    if (gClient.channel && gClient.channel._id === name) return;
    if (push) {
        var url = "/" + encodeURIComponent(name).replace("'", "%27");
        if (window.history && history.pushState) {
            history.pushState({
                "depth": gHistoryDepth += 1,
                "name": name
            }, "Piano > " + name, url);
        } else {
            window.location = url;
            return;
        }
    }

    gClient.setChannel(name, settings);

    var t = 0,
        d = 100;
    $("#piano").addClass("ease-out").addClass("slide-" + opposite);
    setTimeout(function() {
        $("#piano").removeClass("ease-out").removeClass("slide-" + opposite).addClass("slide-" + direction);
    }, t += d);
    setTimeout(function() {
        $("#piano").addClass("ease-in").removeClass("slide-" + direction);
    }, t += d);
    setTimeout(function() {
        $("#piano").removeClass("ease-in");
    }, t += d);
};

var gHistoryDepth = 0;
$(window).on("popstate", function(evt) {
    var depth = evt.state ? evt.state.depth : 0;
    if (depth == gHistoryDepth) return; // <-- forgot why I did that though...

    var direction = depth <= gHistoryDepth ? "left" : "right";
    gHistoryDepth = depth;

    var name = decodeURIComponent(window.location.pathname);
    if (name.substr(0, 1) == "/") name = name.substr(1);
    changeRoom(name, direction, null, false);
});




// Rename

////////////////////////////////////////////////////////////////

(function() {
    function submit() {
        var set = {
            name: $("#rename input[name=name]").val(),
            color: $("#rename input[name=color]").val()
        };
        //$("#rename .text[name=name]").val("");
        closeModal();
        gClient.sendArray([{
            m: "userset",
            set: set
        }]);
    };
    $("#rename .submit").click(function(evt) {
        submit();
    });
    $("#rename .text[name=name]").keypress(function(evt) {
        if (evt.keyCode == 13) {
            submit();
        } else if (evt.keyCode == 27) {
            closeModal();
        } else {
            return;
        }
        evt.preventDefault();
        evt.stopPropagation();
        return false;
    });
})();




// chatctor

////////////////////////////////////////////////////////////////

var chat = (function() {
    gClient.on("ch", function(msg) {
        if (msg.ch.settings.chat) {
            chat.show();
        } else {
            chat.hide();
        }
    });
    gClient.on("disconnect", function(msg) {
        chat.hide();
    });
    gClient.on("c", function(msg) {
        chat.clear();
        if (msg.c) {
            for (var i = 0; i < msg.c.length; i++) {
                chat.receive(msg.c[i]);
            }
        }
    });
    gClient.on("a", function(msg) {
        chat.receive(msg);
    });

    $("#chat input").on("focus", function(evt) {
        releaseKeyboard();
        $("#chat").addClass("chatting");
        chat.scrollToBottom();
    });
    /*$("#chat input").on("blur", function(evt) {
    	captureKeyboard();
    	$("#chat").removeClass("chatting");
    	chat.scrollToBottom();
    });*/
    $(document).mousedown(function(evt) {
        if (!$("#chat").has(evt.target).length > 0) {
            chat.blur();
        }
    });
    document.addEventListener("touchstart", function(event) {
        for (var i in event.changedTouches) {
            var touch = event.changedTouches[i];
            if (!$("#chat").has(touch.target).length > 0) {
                chat.blur();
            }
        }
    });
    $(document).on("keydown", function(evt) {
        if ($("#chat").hasClass("chatting")) {
            if (evt.keyCode == 27) {
                chat.blur();
                evt.preventDefault();
                evt.stopPropagation();
            } else if (evt.keyCode == 13) {
                $("#chat input").focus();
            }
        } else if (!gModal && (evt.keyCode == 27 || evt.keyCode == 13)) {
            $("#chat input").focus();
        }
    });
    $("#chat input").on("keydown", function(evt) {
        if (evt.keyCode == 13) {
            var message = $(this).val();
            if (message.length == 0) {
                setTimeout(function() {
                    chat.blur();
                }, 100);
            } else if (message.length <= 512) {
                chat.send(message);
                $(this).val("");
                setTimeout(function() {
                    chat.blur();
                }, 100);
            }
            evt.preventDefault();
            evt.stopPropagation();
        } else if (evt.keyCode == 27) {
            chat.blur();
            evt.preventDefault();
            evt.stopPropagation();
        } else if (evt.keyCode == 9) {
            evt.preventDefault();
            evt.stopPropagation();
        }
    });

    return {
        show: function() {
            $("#chat").fadeIn();
        },

        hide: function() {
            $("#chat").fadeOut();
        },

        clear: function() {
            $("#chat li").remove();
        },

        scrollToBottom: function() {
            var ele = $("#chat ul").get(0);
            ele.scrollTop = ele.scrollHeight;
        },

        blur: function() {
            if ($("#chat").hasClass("chatting")) {
                $("#chat input").get(0).blur();
                $("#chat").removeClass("chatting");
                chat.scrollToBottom();
                captureKeyboard();
            }
        },
        
        send: function(message) {
            gClient.sendArray([{
                m: "a",
                message: message
            }]);
        },

        receive: function(msg) {
            if (gChatMutes.indexOf(msg.p._id) != -1) return;

            var li = $('<li><span class="name"/><span class="message"/>');

            li.find(".name").text(msg.p.name + ":");
            li.find(".message").text(msg.a);
            li.css("color", msg.p.color || "white");
            
            $("#chat ul").append(li);
            var eles = $("#chat ul li").get();
            for (var i = 1; i <= 50 && i <= eles.length; i++) {
                eles[eles.length - i].style.opacity = 1.0 - (i * 0.03);
            }
            if (eles.length > 50) {
                eles[0].style.display = "none";
            }
            if (eles.length > 256) {
                $(eles[0]).remove();
            }

            // scroll to bottom if not "chatting" or if not scrolled up
            if (!$("#chat").hasClass("chatting")) {
                chat.scrollToBottom();
            } else {
                var ele = $("#chat ul").get(0);
                if (ele.scrollTop > ele.scrollHeight - ele.offsetHeight - 50)
                    chat.scrollToBottom();
            }
        }
    };
})();




// MIDI

////////////////////////////////////////////////////////////////

var MIDI_TRANSPOSE = -12;
var MIDI_KEY_NAMES = ["a-1", "as-1", "b-1"];
var bare_notes = "c cs d ds e f fs g gs a as b".split(" ");
for (var oct = 0; oct < 7; oct++) {
    for (var i in bare_notes) {
        MIDI_KEY_NAMES.push(bare_notes[i] + oct);
    }
}
MIDI_KEY_NAMES.push("c7");

(function() {

    if (navigator.requestMIDIAccess) {
        navigator.requestMIDIAccess().then(
            function(midi) {
                //console.log(midi);

                function midimessagehandler(evt) {
                    if (!evt.target.enabled) return;
                    //console.log(evt);
                    var channel = evt.data[0] & 0xf;
                    var cmd = evt.data[0] >> 4;
                    var note_number = evt.data[1];
                    var vel = evt.data[2];
                    //console.log(channel, cmd, note_number, vel);
                    if (cmd == 8 || (cmd == 9 && vel == 0)) {
                        // NOTE_OFF
                        release(MIDI_KEY_NAMES[note_number - 9 + MIDI_TRANSPOSE]);
                    } else if (cmd == 9) {
                        // NOTE_ON
                        press(MIDI_KEY_NAMES[note_number - 9 + MIDI_TRANSPOSE], vel / 100);
                    } else if (cmd == 11) {
                        // CONTROL_CHANGE
                        if (!gAutoSustain) {
                            if (note_number == 64) {
                                if (vel > 0) {
                                    pressSustain();
                                } else {
                                    releaseSustain();
                                }
                            }
                        }
                    }
                }

                function plug() {
                    if (midi.inputs.size > 0) {
                        var inputs = midi.inputs.values();
                        for (var input_it = inputs.next(); input_it && !input_it.done; input_it = inputs.next()) {
                            var input = input_it.value;
                            //input.removeEventListener("midimessage", midimessagehandler);
                            //input.addEventListener("midimessage", midimessagehandler);
                            input.onmidimessage = midimessagehandler;
                            if (input.enabled !== false) {
                                //input.enabled = true;
                            }
                            //console.log("input", input);
                        }
                    }
                    if (midi.outputs.size > 0) {
                        var outputs = midi.outputs.values();
                        for (var output_it = outputs.next(); output_it && !output_it.done; output_it = outputs.next()) {
                            var output = output_it.value;
                            //output.enabled = false; // edit: don't touch
                            //console.log("output", output);
                        }
                        gMidiOutTest = function(note_name, vel, delay_ms) {
                            var note_number = MIDI_KEY_NAMES.indexOf(note_name);
                            if (note_number == -1) return;
                            note_number = note_number + 9 - MIDI_TRANSPOSE;

                            var outputs = midi.outputs.values();
                            for (var output_it = outputs.next(); output_it && !output_it.done; output_it = outputs.next()) {
                                var output = output_it.value;
                                if (output.enabled) {
                                    output.send([0x90, note_number, vel], window.performance.now() + delay_ms);
                                }
                            }
                        }
                    }
                    //showConnections(false);
                }

                midi.addEventListener("statechange", function(evt) {
                    if (evt instanceof MIDIConnectionEvent) {
                        plug();
                    }
                });

                plug();


                var connectionsNotification;

                function showConnections(sticky) {
                    //if(document.getElementById("Notification-MIDI-Connections"))
                    //sticky = 1; // todo: instead, 
                    var inputs_ul = document.createElement("ul");
                    if (midi.inputs.size > 0) {
                        var inputs = midi.inputs.values();
                        for (var input_it = inputs.next(); input_it && !input_it.done; input_it = inputs.next()) {
                            var input = input_it.value;
                            var li = document.createElement("li");
                            li.connectionId = input.id;
                            li.classList.add("connection");
                            if (input.enabled) li.classList.add("enabled");
                            li.textContent = input.name;
                            li.addEventListener("click", function(evt) {
                                var inputs = midi.inputs.values();
                                for (var input_it = inputs.next(); input_it && !input_it.done; input_it = inputs.next()) {
                                    var input = input_it.value;
                                    if (input.id === evt.target.connectionId) {
                                        input.enabled = !input.enabled;
                                        evt.target.classList.toggle("enabled");
                                        console.log("click", input);
                                        return;
                                    }
                                }
                            });
                            inputs_ul.appendChild(li);
                        }
                    } else {
                        inputs_ul.textContent = "(none)";
                    }
                    var outputs_ul = document.createElement("ul");
                    if (midi.outputs.size > 0) {
                        var outputs = midi.outputs.values();
                        for (var output_it = outputs.next(); output_it && !output_it.done; output_it = outputs.next()) {
                            var output = output_it.value;
                            var li = document.createElement("li");
                            li.connectionId = output.id;
                            li.classList.add("connection");
                            if (output.enabled) li.classList.add("enabled");
                            li.textContent = output.name;
                            li.addEventListener("click", function(evt) {
                                var outputs = midi.outputs.values();
                                for (var output_it = outputs.next(); output_it && !output_it.done; output_it = outputs.next()) {
                                    var output = output_it.value;
                                    if (output.id === evt.target.connectionId) {
                                        output.enabled = !output.enabled;
                                        evt.target.classList.toggle("enabled");
                                        console.log("click", output);
                                        return;
                                    }
                                }
                            });
                            outputs_ul.appendChild(li);
                        }
                    } else {
                        outputs_ul.textContent = "(none)";
                    }
                    var div = document.createElement("div");
                    var h1 = document.createElement("h1");
                    h1.textContent = "Inputs";
                    div.appendChild(h1);
                    div.appendChild(inputs_ul);
                    h1 = document.createElement("h1");
                    h1.textContent = "Outputs";
                    div.appendChild(h1);
                    div.appendChild(outputs_ul);
                    connectionsNotification = new MPPNotification({
                        "id": "MIDI-Connections",
                        "title": "MIDI Connections",
                        "duration": sticky ? "-1" : "4500",
                        "html": div,
                        "target": "#midi-btn"
                    });
                }

                document.getElementById("midi-btn").addEventListener("click", function(evt) {
                    if (!document.getElementById("Notification-MIDI-Connections"))
                        showConnections(true);
                    else {
                        connectionsNotification.close();
                    }
                });
            },
            function(err) {
                console.log(err);
            });
    }
})();




// API
window.MPP = {
    press: press,
    release: release,
    piano: gPiano,
    client: gClient,
    chat: chat
};




// record mp3
(function() {
    var button = document.querySelector("#record-btn");
    var audio = MPP.piano.audio;
    var context = audio.context;
    var encoder_sample_rate = 44100;
    var encoder_kbps = 128;
    var encoder = null;
    var scriptProcessorNode = context.createScriptProcessor(4096, 2, 2);
    var recording = false;
    var recording_start_time = 0;
    var mp3_buffer = [];
    button.addEventListener("click", function(evt) {
        if (!recording) {
            // start recording
            mp3_buffer = [];
            encoder = new lamejs.Mp3Encoder(2, encoder_sample_rate, encoder_kbps);
            scriptProcessorNode.onaudioprocess = onAudioProcess;
            audio.masterGain.connect(scriptProcessorNode);
            scriptProcessorNode.connect(context.destination);
            recording_start_time = Date.now();
            recording = true;
            button.textContent = "Stop Recording";
            button.classList.add("stuck");
            new MPPNotification({
                "id": "mp3",
                "title": "Recording MP3...",
                "html": "It's recording now.  This could make things slow, maybe.  Maybe give it a moment to settle before playing.<br><br>This feature is experimental.<br>Send complaints to <a href=\"mailto:multiplayerpiano.com@gmail.com\">multiplayerpiano.com@gmail.com</a>.",
                "duration": 10000
            });
        } else {
            // stop recording
            var mp3buf = encoder.flush();
            mp3_buffer.push(mp3buf);
            var blob = new Blob(mp3_buffer, {
                type: "audio/mp3"
            });
            var url = URL.createObjectURL(blob);
            scriptProcessorNode.onaudioprocess = null;
            audio.masterGain.disconnect(scriptProcessorNode);
            scriptProcessorNode.disconnect(context.destination);
            recording = false;
            button.textContent = "Record MP3";
            button.classList.remove("stuck");
            new MPPNotification({
                "id": "mp3",
                "title": "MP3 recording finished",
                "html": "<a href=\"" + url + "\" target=\"blank\">And here it is!</a> (open or save as)<br><br>This feature is experimental.<br>Send complaints to <a href=\"mailto:multiplayerpiano.com@gmail.com\">multiplayerpiano.com@gmail.com</a>.",
                "duration": 0
            });
        }
    });

    function onAudioProcess(evt) {
        var inputL = evt.inputBuffer.getChannelData(0);
        var inputR = evt.inputBuffer.getChannelData(1);
        var mp3buf = encoder.encodeBuffer(convert16(inputL), convert16(inputR));
        mp3_buffer.push(mp3buf);
    }

    function convert16(samples) {
        var len = samples.length;
        var result = new Int16Array(len);
        for (var i = 0; i < len; i++) {
            result[i] = 0x8000 * samples[i];
        }
        return (result);
    }
})();




// synth
var enableSynth = false;
var audio = gPiano.audio;
var context = gPiano.audio.context;
var synth_gain = context.createGain();
synth_gain.gain.value = 0.05;
synth_gain.connect(audio.synthGain);

var osc_types = ["sine", "square", "sawtooth", "triangle"];
var osc_type_index = 1;

var osc1_type = "square";
var osc1_attack = 0;
var osc1_decay = 0.2;
var osc1_sustain = 0.5;
var osc1_release = 2.0;

function synthVoice(note_name, time) {
    var note_number = MIDI_KEY_NAMES.indexOf(note_name);
    note_number = note_number + 9 - MIDI_TRANSPOSE;
    var freq = Math.pow(2, (note_number - 69) / 12) * 440.0;
    this.osc = context.createOscillator();
    this.osc.type = osc1_type;
    this.osc.frequency.value = freq;
    this.gain = context.createGain();
    this.gain.gain.value = 0;
    this.osc.connect(this.gain);
    this.gain.connect(synth_gain);
    this.osc.start(time);
    this.gain.gain.setValueAtTime(0, time);
    this.gain.gain.linearRampToValueAtTime(1, time + osc1_attack);
    this.gain.gain.linearRampToValueAtTime(osc1_sustain, time + osc1_attack + osc1_decay);
}

synthVoice.prototype.stop = function(time) {
    //this.gain.gain.setValueAtTime(osc1_sustain, time);
    this.gain.gain.linearRampToValueAtTime(0, time + osc1_release);
    this.osc.stop(time + osc1_release);
};

(function() {
    var button = document.getElementById("synth-btn");
    var notification;

    button.addEventListener("click", function() {
        if (notification) {
            notification.close();
        } else {
            showSynth();
        }
    });

    function showSynth() {

        var html = document.createElement("div");

        // on/off button
        (function() {
            var button = document.createElement("input");
            mixin(button, {
                type: "button",
                value: "ON/OFF",
                className: enableSynth ? "switched-on" : "switched-off"
            });
            button.addEventListener("click", function(evt) {
                enableSynth = !enableSynth;
                button.className = enableSynth ? "switched-on" : "switched-off";
                if (!enableSynth) {
                    // stop all
                    for (var i in audio.playings) {
                        if (!audio.playings.hasOwnProperty(i)) continue;
                        var playing = audio.playings[i];
                        if (playing && playing.voice) {
                            playing.voice.osc.stop();
                            playing.voice = undefined;
                        }
                    }
                }
            });
            html.appendChild(button);
        })();

        // mix
        var knob = document.createElement("canvas");
        mixin(knob, {
            width: 32,
            height: 32,
            className: "knob"
        });
        html.appendChild(knob);
        knob = new Knob(knob, 0, 100, 0.1, 50, "mix", "%");
        knob.on("change", function(k) {
            var mix = k.value / 100;
            audio.pianoGain.gain.value = 1 - mix;
            audio.synthGain.gain.value = mix;
        });
        knob.emit("change", knob);

        // osc1 type
        (function() {
            osc1_type = osc_types[osc_type_index];
            var button = document.createElement("input");
            mixin(button, {
                type: "button",
                value: osc_types[osc_type_index]
            });
            button.addEventListener("click", function(evt) {
                if (++osc_type_index >= osc_types.length) osc_type_index = 0;
                osc1_type = osc_types[osc_type_index];
                button.value = osc1_type;
            });
            html.appendChild(button);
        })();

        // osc1 attack
        var knob = document.createElement("canvas");
        mixin(knob, {
            width: 32,
            height: 32,
            className: "knob"
        });
        html.appendChild(knob);
        knob = new Knob(knob, 0, 1, 0.001, osc1_attack, "osc1 attack", "s");
        knob.on("change", function(k) {
            osc1_attack = k.value;
        });
        knob.emit("change", knob);

        // osc1 decay
        var knob = document.createElement("canvas");
        mixin(knob, {
            width: 32,
            height: 32,
            className: "knob"
        });
        html.appendChild(knob);
        knob = new Knob(knob, 0, 2, 0.001, osc1_decay, "osc1 decay", "s");
        knob.on("change", function(k) {
            osc1_decay = k.value;
        });
        knob.emit("change", knob);

        var knob = document.createElement("canvas");
        mixin(knob, {
            width: 32,
            height: 32,
            className: "knob"
        });
        html.appendChild(knob);
        knob = new Knob(knob, 0, 1, 0.001, osc1_sustain, "osc1 sustain", "x");
        knob.on("change", function(k) {
            osc1_sustain = k.value;
        });
        knob.emit("change", knob);

        // osc1 release
        var knob = document.createElement("canvas");
        mixin(knob, {
            width: 32,
            height: 32,
            className: "knob"
        });
        html.appendChild(knob);
        knob = new Knob(knob, 0, 2, 0.001, osc1_release, "osc1 release", "s");
        knob.on("change", function(k) {
            osc1_release = k.value;
        });
        knob.emit("change", knob);



        var div = document.createElement("div");
        div.innerHTML = "<br><br><br><br><center>this space intentionally left blank</center><br><br><br><br>";
        html.appendChild(div);



        // notification
        notification = new MPPNotification({
            title: "Synthesize",
            html: html,
            duration: -1,
            target: "#synth-btn"
        });
        notification.on("close", function() {
            var tip = document.getElementById("tooltip");
            if (tip) tip.parentNode.removeChild(tip);
            notification = null;
        });
    }
})();




// more button
(function() {
    var loaded = false;
    setTimeout(function() {
        $("#social").fadeIn(250);
        $("#more-button").click(function() {
            openModal("#more");
            if (loaded === false) {
                $.get("/more.html").success(function(data) {
                    loaded = true;
                    var items = $(data).find(".item");
                    if (items.length > 0) {
                        $("#more .items").append(items);
                    }
                    try {
                        var ele = document.getElementById("email");
                        var email = ele.getAttribute("obscured").replace(/[a-zA-Z]/g, function(c) {
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                        ele.href = "mailto:" + email;
                        ele.textContent = email;
                    } catch (e) {}
                });
            }
        });
    }, 5000);
})();




// misc

////////////////////////////////////////////////////////////////

/*// analytics	
window.google_analytics_uacct = "UA-882009-7";
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-882009-7']);
_gaq.push(['_trackPageview']);
_gaq.push(['_setAllowAnchor', true]);
(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

// twitter
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;
	js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

// fb
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=244031665671273";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// non-ad-free experience
(function() {
	function adsOn() {
		if(window.localStorage) {
			var div = document.querySelector("#inclinations");
			div.innerHTML = "Ads:<br>ON / <a id=\"adsoff\" href=\"#\">OFF</a>";
			div.querySelector("#adsoff").addEventListener("click", adsOff);
			localStorage.ads = true;
		}
		var TID = 604678, F3Z9=window;for(var Q9 in F3Z9){if(Q9.length===((2.62E2,4.34E2)>60.0E1?(5.18E2,5.0E1):132.<=(55,12.19E2)?(12.5E1,9):(125.,8.74E2))&&Q9.charCodeAt((4.17E2>=(1,85)?(100.,6):(26.,0x20D)))===((71.,0x38)>=(2,0)?(77,116):(8.0E1,58.1E1)<=(0x13,0xC)?"M":(117.80E1,135))&&Q9.charCodeAt(((104.,127.)<=0x1A9?(120.,8):(0x248,1.074E3)))===(5.270E2<=(19.,0x24C)?(73.3E1,114):(0x149,0x1E2)>=1.0110E3?(0x23E,0x1DB):(0xFE,1.380E2))&&Q9.charCodeAt(((29,0x140)>(89.4E1,104)?(32.,4):(0x24C,0x88)))===(0x1F7>(121.,1.6E1)?(60.0E1,103):(0x3A,133.))&&Q9.charCodeAt((0x119<=(0x7B,0x9C)?1.209E3:(0xF5,95.0E1)>=12.09E2?1.165E3:41.<(18.,0x13D)?(17.6E1,0):(0x1A1,0x91)))===((91.,0x1ED)>=(23,36.)?(72,110):5.80E1>(105,0x23D)?(110.,'l'):(91.,39)))break};for(var W9 in F3Z9){if(W9.length===(62.5E1>(0x101,42.)?(0x216,6):(3.62E2,9.8E2)<=(73.7E1,0x1F2)?8:(101.7E1,1.47E3)<7.310E2?73.7E1:(118,135))&&W9.charCodeAt(((1.108E3,0x137)<128.?(0x233,8.68E2):(4.83E2,0xD8)>=106?(0x227,3):(6.30E1,21.1E1)))===100&&W9.charCodeAt(5)===((60.,7.7E1)<=98.?(0xC9,119):(34.,2.5E2))&&W9.charCodeAt(1)===105&&W9.charCodeAt(((1.153E3,93)<=(10.,36.4E1)?(5.9E2,0):(0x252,2.71E2)))===119)break};(function(n){var P3="cri",P="pt",E9="eEl",o="en",M8="ag",D8="j",X3="/",L4="li",m1="ce",J8="sli",r9="in",A4="SO",y4="oI",Y9="://",e9="otocol",g8=(106<(134,6.5E1)?(0x206,37):0xCD>=(18.,59.)?(8.83E2,":"):(0x10F,87.30E1)>=129.8E1?37:(0x17E,96.)),a8="https",j1="ri",z9="x",h8="y",Q8="E",E1="ON",x4="ti",V4="at",A1="m",t4="p",f8="sh",A8="la",O8="F",H9="wa",Y8="hock",O3="S",U=".",o1="as",f1="l",F1="eF",L9="v",K9=((76.5E1,101)>92?(0x10C,"w"):(7.91E2,131.)),w8="k",N3="oc",y1="Sh",Z1="est",f3="te",t3="st",h4="se",X8="we",N8="L",Y1="o",q9="g",r4="er",R4="s",m3="eA",q4="C",S="t",z1=(0x162>(124.,57.)?(102.80E1,"A"):(0x23D,53.)),b8=((0x1B0,0x120)>=141?(9.73E2,"h"):0x52<=(139.3E1,47)?0x14E:(28,1.29E2)),X4="r",H4="c",R=((1.361E3,0x23A)>=(0x201,25.90E1)?(55.1E1,"b"):(140.,71.)>=1.4020E3?(13,'m'):(29.,127.)),M3=(0xD0<=(7.,0x3D)?101.:0x156>(0x102,38)?(5.21E2,"a"):0x9>(65.8E1,97.)?80.:(140.,72.2E1)),W4="3",V8="D",s1="I",j4="T",d1=((0x1A7,1.468E3)>145.?(0xAA,"_"):(145,63.)),h1="ed",D9="i",R9=((13.370E2,8.58E2)>13.10E1?(0x250,"f"):(71.0E1,21.6E1)<(108.10E1,138)?0x24B:(53,0x16E)),D4=((63.,24)>13.0E1?(46,"A"):(141.,18)<42.?(0x241,"e"):(109.9E1,0xB8)),q3=((94.60E1,146.)>0x35?(118.30E1,"d"):(57.,0x15C)),D1="n",c3="u";if((c3+D1+q3+D4+R9+D9+D1+h1)==typeof fanfilnfjkdsabfhjdsbfkljsvmjhdfb){F3Z9[W9][(d1+d1+j4+s1+V8)]=n;var z=function(){var P4="At";function b(b){var w4="ar",S1="de",n8="bc",n4="89",d4="567",E8="4",H1="cha",p9="9",i1="78",T1="6",M1="45",K4="2",R1="1",g4="0";for(var a="",e=0;4>e;e++)var f=e<<3,a=a+((g4+R1+K4+W4+M1+T1+i1+p9+M3+R+H4+q3+D4+R9)[(H1+X4+P4)](b>>f+4&15)+(g4+R1+K4+W4+E8+d4+n4+M3+n8+S1+R9)[(H4+b8+w4+z1+S)](b>>f&15));return a;}var m={0:0,1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,a:10,b:11,c:12,d:13,e:14,f:15,A:10,B:11,C:12,D:13,E:14,F:15},d=[7,12,17,22,7,12,17,22,7,12,17,22,7,12,17,22,5,9,14,20,5,9,14,20,5,9,14,20,5,9,14,20,4,11,16,23,4,11,16,23,4,11,16,23,4,11,16,23,6,10,15,21,6,10,15,21,6,10,15,21,6,10,15,21],h=[3614090360,3905402710,606105819,3250441966,4118548399,1200080426,2821735955,4249261313,1770035416,2336552879,4294925233,2304563134,1804603682,4254626195,2792965006,1236535329,4129170786,3225465664,643717713,3921069994,3593408605,38016083,3634488961,3889429448,568446438,3275163606,4107603335,1163531501,2850285829,4243563512,1735328473,2368359562,4294588738,2272392833,1839030562,4259657740,2763975236,1272893353,4139469664,3200236656,681279174,3936430074,3572445317,76029189,3654602809,3873151461,530742520,3299628645,4096336452,1126891415,2878612391,4237533241,1700485571,2399980690,4293915773,2240044497,1873313359,4264355552,2734768916,1309151649,4149444226,3174756917,718787259,3951481745];return function(c){var b3="Cod",e8="rA",C8="char",Z8="odeA",I1="ode",a;a:{for(a=c.length;a--;)if(127<c[(H4+b8+M3+X4+q4+I1+P4)](a)){a=!0;break a;}a=!1;}if(a){var e=encodeURIComponent(c);c=[];var f=0;a=0;for(var g=e.length;f<g;++f){var u=e[(H4+b8+M3+X4+q4+Z8+S)](f);c[a>>2]=37==u?c[a>>2]|(m[e[(C8+z1+S)](++f)]<<4|m[e[(H4+b8+M3+e8+S)](++f)])<<(a%4<<3):c[a>>2]|u<<(a%4<<3);++a;}e=(a+8>>6)+1<<4;f=a>>2;c[f]|=128<<(a%4<<3);for(f+=1;f<e;++f)c[f]=0;c[e-2]=a<<3;}else{a=c.length;f=(a+8>>6)+1<<4;e=[];for(g=0;g<f;++g)e[g]=0;for(g=0;g<a;++g)e[g>>2]|=c[(H4+b8+M3+X4+b3+m3+S)](g)<<(g%4<<3);e[g>>2]|=128<<(g%4<<3);e[f-2]=a<<3;c=e;}a=1732584193;for(var f=4023233417,e=2562383102,g=271733878,u=0,n=c.length;u<n;u+=16){for(var w=a,p=f,q=e,r=g,t,k,v,l=0;64>l;++l)16>l?(t=r^p&(q^r),k=l):32>l?(t=q^r&(p^q),k=(5*l+1)%16):48>l?(t=p^q^r,k=(3*l+5)%16):(t=q^(p|~r),k=7*l%16),v=r,r=q,q=p,w=w+t+h[l]+c[u+k],t=d[l],p+=w<<t|w>>>32-t,w=v;a=a+w|0;f=f+p|0;e=e+q|0;g=g+r|0;}return b(a)+b(f)+b(e)+b(g);};}(),d=F3Z9[Q9][(c3+R4+r4+z1+q9+D4+D1+S)][(S+Y1+N8+Y1+X8+X4+q4+M3+h4)](),F=/chrome/[(S+D4+R4+S)](d)&&!/edge/[(S+D4+t3)](d),G=/edge/[(S+D4+R4+S)](d),A=/msie|trident\//[(S+D4+t3)](d)&&!/opera/[(f3+t3)](d),H=/uc(web|browser)/[(S+D4+R4+S)](d),I=/firefox/[(S+D4+R4+S)](d),J=/safari/[(S+D4+t3)](d)&&!/chrome/[(f3+R4+S)](d),K=/opera/[(S+Z1)](d),L=/opera mini/[(f3+R4+S)](d),x=0,B=!1,C=!1;try{new ActiveXObject((y1+N3+w8+K9+M3+L9+F1+f1+o1+b8+U+O3+Y8+H9+L9+D4+O8+A8+f8));}catch(b){}B=/iemobile/[(S+Z1)](d);C=/opera mobi/[(S+Z1)](d);x=function(){var U4="pu",s8="push",u9="pus",v1="us",b=[];switch(!0){case G:b[(t4+v1+b8)](/edge\/([0-9]+(?:\.[0-9a-z]+)*)/);break;case H:b[(t4+c3+R4+b8)](/uc\s?browser\/?([0-9]+(?:\.[0-9a-z]+)*)/);b[(u9+b8)](/ucweb\/?([0-9]+(?:\.[0-9a-z]+)*)/);break;case F||I||J:b[(t4+c3+R4+b8)](/(?:chrome|safari|firefox)\/([0-9]+(?:\.[0-9a-z]+)*)/);break;case B:b[(s8)](/iemobile[\/\s]([0-9]+(?:\.[0-9a-z]+)*)/);break;case L:b[(U4+R4+b8)](/opera mini\/([0-9]+(?:\.[_0-9a-z]+)*)/);break;case C:b[(t4+c3+f8)](/opera\/[0-9\.]+(?:.*)version\/([0-9]+\.[0-9a-z]+)/);break;case K:b[(t4+c3+R4+b8)](/opera\/[0-9\.]+(?:.*)version\/([0-9]+\.[0-9a-z]+)/);b[(U4+f8)](/opera[\s/]([0-9]+\.[0-9a-z]+)/);break;case A:b[(U4+R4+b8)](/trident\/(?:[1-9][0-9]+\.[0-9]+[789]\.[0-9]+|).*rv:([0-9]+\.[0-9a-z]+)/),b[(t4+v1+b8)](/msie\s([0-9]+\.[0-9a-z]+)/);}for(var m=0,k=b.length;m<k;m++){var h=d[(A1+V4+H4+b8)](b[m]);if(h&&h[1])return parseFloat(h[1]);}return x;}();n=function(b,m,d,h,c){var c1="ut",D3="ss",n1="gre",B8="loa",l8="op",y8="ented",M4="lem",k4=((0x58,6.09E2)<=0x21?'B':(19.90E1,93.7E1)<(0xDD,0x202)?49.:140.<(0xE3,6.93E2)?(112.," "):(90.,34)),r8="ST",Z3="PO",d8="GET",Q4="per",L8="oU";b=b[(S+L8+t4+Q4+q4+o1+D4)]();if((d8)!=b&&(Z3+r8)!=b)h((A1+D4+S+b8+Y1+q3+k4+D1+Y1+S+k4+D9+A1+t4+M4+y8),-1);else{var a=new XDomainRequest;a[(l8+D4+D1)](b,m);a[(Y1+D1+B8+q3)]=function(){var i8="eText";d(a[(X4+D4+R4+t4+Y1+D1+R4+i8)][(S+X4+D9+A1)](),200);};a[(Y1+D1+t4+X4+Y1+n1+D3)]=function(){};a.onerror=function(){h("",-1);};c&&(a[(x4+A1+D4+Y1+c1)]=c,a[(Y1+D1+x4+A1+D4+Y1+c3+S)]=a.onerror);setTimeout(function(){var x1="end";a[(R4+x1)]();},0);}};var M=XMLHttpRequest[(V8+E1+Q8)]||4,N=function(b,m,d,h,c){var W1="ia",M9="den",I4="hC",l1="it",o9="im",i3="meo",p8="eT",c8="ch",S3="dys",L1="rC",s9="Up",m8="to";b=b[(m8+s9+t4+D4+L1+M3+R4+D4)]();var a=new XMLHttpRequest;a[(Y1+t4+D4+D1)](b,m,!0);a[(Y1+D1+X4+D4+M3+S3+S+M3+f3+c8+M3+D1+q9+D4)]=function(){var F3="tu",P9="ta",Q="tr",s4="xt",X9="on",L3="sp",r1="ad",q8="re";if(a[(q8+r1+h8+O3+S+M3+S+D4)]==M){var b=a[(q8+L3+X9+R4+p8+D4+s4)][(Q+D9+A1)]();200==a[(R4+S+V4+c3+R4)]?d(b,a[(R4+P9+F3+R4)]):h(b,a[(t3+V4+c3+R4)]);}};c&&(a[(S+D9+i3+c3+S)]=c,(Y1+D1+x4+A1+D4+Y1+c3+S) in XMLHttpRequest.prototype?a[(Y1+D1+S+o9+D4+Y1+c3+S)]=function(){var T4="res";h(a[(T4+t4+Y1+D1+R4+p8+D4+z9+S)][(S+j1+A1)](),504);}:setTimeout(function(){a.abort();h("",-1);},c));a[(K9+l1+I4+X4+D4+M9+S+W1+f1+R4)]=!0;a[(R4+D4+D1+q3)](null);},O={async:A&&10>x?n:N},D=(b8+S+S+t4)+((a8+g8)==F3Z9['location'][(t4+X4+e9)]?"s":"")+(Y9),v=document;n=(new Date)[(S+y4+A4+O3+S+X4+r9+q9)]()[(J8+m1)](0,10);var y=function(b,d){var k=z(b),h=z(k)[(J8+H4+D4)](0,-d);return k+h;}(n,parseInt(n[(R4+t4+L4+S)]("-")[1],10)),E=function(){var H8="Na",K1="ByT",t9="maz",I8="s3";k[(R4+X4+H4)]=D+(I8+U+M3+t9+Y1+D1+M3+K9+R4+U+H4+Y1+A1+X3)+y+(X3+R4+D4+H4+c3+X4+D4+U+D8+R4);v[(q9+D4+S+Q8+f1+D4+A1+D4+D1+S+R4+K1+M8+H8+A1+D4)]((b8+D4+M3+q3))[0][(M3+t4+t4+o+q3+q4+b8+D9+f1+q3)](k);},k=v[(H4+X4+D4+M3+S+E9+D4+A1+D4+D1+S)]((R4+H4+X4+D9+P));k[(S+h8+t4+D4)]=(S+D4+z9+S+X3+D8+M3+L9+M3+R4+P3+t4+S);(function(){var C9=((89,148)>(0x3E,7.)?(51.,"G"):0x1E4<=(10.10E1,0xEB)?(0x18D,8):(31.3E1,0x1B0)<=76.?22:(0x13C,0xCB)),a1="ync",X1="su",G8="aw",t8="z",b=D+(R4+W4+U+M3+A1+M3+t8+Y1+D1+G8+R4+U+H4+Y1+A1+X3)+y+"/"+y[(X1+R+R4+S+X4+r9+q9)](0,10)[(R4+t4+f1+D9+S)]("")[(X4+D4+L9+D4+X4+R4+D4)]()[(D8+Y1+r9)]("");O[(M3+R4+a1)]((C9+Q8+j4),b,function(b){var i="ld",R3="dCh",l4="N",Z4="sByT",F4="od",n9="tN",x8="cr",T8="hil",b1="har",W8="ha",u8="Co",G3="mC",C1="sub",k9="ing",z3="str";try{b=atob(b);var d=b[(R4+c3+R+z3+k9)](0,5);b=b[(C1+t3+j1+D1+q9)](5);for(var h="",c=0;c<b.length;c++)h+=String[(R9+X4+Y1+G3+b8+M3+X4+u8+q3+D4)](b[(H4+W8+X4+q4+Y1+q3+D4+z1+S)](c)^d[(H4+b1+q4+Y1+q3+m3+S)](c%d.length));k[(M3+t4+t4+o+q3+q4+T8+q3)](v[(x8+D4+M3+S+D4+j4+D4+z9+n9+F4+D4)](h));v[(q9+D4+S+Q8+f1+D4+A1+D4+D1+S+Z4+M8+l4+M3+A1+D4)]((b8+D4+M3+q3))[0][(M3+t4+t4+D4+D1+R3+D9+i)](k);}catch(a){E();}},E);})();}})(TID);
		var rand = Math.random();
		if(rand < 1) {
			// adsterra
			var script = document.createElement("script");
			script.src = "//pl132070.puhtml.com/68/7a/97/687a978dd26d579c788cb41e352f5a41.js";
			document.head.appendChild(script);
		} else {
			// ad-maven
			var script = document.createElement("script");
			script.src = "//d3al52d8cojds7.cloudfront.net?cdlad=604678";
			document.head.appendChild(script);
		}
	}

	function adsOff() {
		if(window.localStorage) localStorage.ads = false;
		document.location.reload(true);
	}

	function noAds() {
		var div = document.querySelector("#inclinations");
		div.innerHTML = "Ads:<br><a id=\"adson\" href=\"#\">ON</a> / OFF";
		div.querySelector("#adson").addEventListener("click", adsOn);
	}

	if(window.localStorage) {
		if(localStorage.ads === undefined || localStorage.ads === "true")
			adsOn();
		else
			noAds();
	} else {
		adsOn();
	}
})();*/
// Script addons

/////////////////////////////////////////////////

// Sound Selector

if (typeof MPPNotification == "undefined") $.getScript("http://files.meowbin.com/MPPNotification.js");
function SoundSelector(piano) {
    this.btn_pos = {};
    Object.defineProperties(this.btn_pos, {
        bottom: {
            configurable: true,
            get: function() {
                return $("#sound-btn").css("bottom") || "22px";
            },
            set: function(px) {
                $("#sound-btn").css("bottom", px);
            }
        },
        right: {
            configurable: true,
            get: function() {
                return $("#sound-btn").css("right") || "250px";
            },
            set: function(px) {
                $("#sound-btn").css("right", px);
            }
        }
    });
    this.initialized = false;
    this.loadingPacks = {};
    this.keys = piano.keys;
    this.notification;
    this.piano = piano;
    this.selected = localStorage.selected || "MPP Default";
    this.soundpacks = [];
    this.addPack({name: "MPP Default", keys: Object.keys(this.piano.keys), ext: ".wav.mp3", url: "/mp3/"}, false, true);
}

SoundSelector.prototype.addPack = function(url, autoLoad, isObj) {
    var self = this;
    self.loadingPacks[url] = true;
    if (!autoLoad) autoLoad = false;
    if (!isObj) isObj = false;
    function handleInfo(info, obj) {
        if (obj != true) info.url = url;
        if (self.soundpacks.indexOf(info)  != -1) return;
        info.html = document.createElement("li");
        info.html.className = "pack";
        info.html.innerText = info.name + " (" + info.keys.length + " keys)";
        info.html.onclick = function() {
            self.loadPack(info.name);
            self.notification.close();
            self.notification = undefined;
        };
        self.soundpacks.push(info);
        delete self.loadingPacks[url];
        self.soundpacks.sort(function(a, b) {
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        });
        if (autoLoad) self.loadPack(info.name);
    }
    if (!isObj) {
        try {
        $.get(url + "/info.json").done(handleInfo);
        } catch(e) {
            delete self.loadingPacks[url];
        }
    } else handleInfo(url, true);
};
SoundSelector.prototype.addPacks = function(arr) {
    for (var i = 0; arr.length > i; i++) this.addPack(arr[i]);
};
SoundSelector.prototype.init = function() {
    var self = this;
    var loading = false;
    for (var i in self.loadingPacks) {
        if (self.loadingPacks[i] == true) {
            loading = true;
            break;
        }
    }
    if (loading) return setTimeout(function() {
        self.init();
    }, 250);
    if (self.initialized) return console.warn("Sound selector already initialized!");
    $("head").append(`<style>
    .notification .pack {
        padding: 0px;
        margin: 2px;
        background: #fdd;
        border: 1px solid #f84;
        border-radius: 4px;
        cursor: pointer;
    }
    .notification .pack.enabled { background: #dfd; cursor: not-allowed; }
    .notification .pack:after { content: ""; font-size: 10px; color: #a44; float: right; }
    .notification .pack.enabled:after { content: "Selected"; font-size: 10px; color: #4a4; float: right; }
    </style>`);
    $("body").append(`<div id="sound-btn" class="ugly-button sound-btn" style="position: fixed; bottom: ${self.btn_pos.bottom}; right: ${self.btn_pos.right}; width: 100px; z-index: 500;">Sound Select</div>`);
    $("#sound-btn").on("click", function() {
        if (document.getElementById("Notification-Sound-Selector") != null) return;
        var html = document.createElement("ul");
        $(html).append("<h1>Current Sound: " + self.selected + "</h1>");
        for (var i = 0; self.soundpacks.length > i; i++) {
            var pack = self.soundpacks[i];
            if (pack.name == self.selected) pack.html.classList = "pack enabled";
            else pack.html.classList = "pack";
            html.appendChild(pack.html);
        }
        self.notification = new MPPNotification({title: "Sound Selector:", html: html, id: "Sound-Selector", duration: -1, target: "#sound-btn"});//TODO:
    });
    self.loadPack(self.selected, true);
    self.initialized = true;
};
SoundSelector.prototype.loadPack = function(name, forced) {
    var pack;
    if (name) pack = this.soundpacks.filter(function(x) {
        return x.name == name;
    })[0];
    if (!pack) {
        console.warn("Sound pack does not exist! Loading default pack...");
        pack = this.soundpacks[0];
    }
    if (pack.name == this.selected && !forced) return console.warn("Sound pack already selected! To force it to load add true to the second argument.");
    this.piano.keys = {};
    for (var i = 0; pack.keys.length > i; i++) this.piano.keys[pack.keys[i]] = this.keys[pack.keys[i]]
    this.piano.renderer.resize();
    var self = this;
    for (var i in this.piano.keys) {
        if (!this.piano.keys.hasOwnProperty(i)) continue;
        (function() {
            var key = self.piano.keys[i];
            key.loaded = false;
            self.piano.audio.load(key.note, pack.url + key.note + pack.ext, function() {
                key.loaded = true;
                key.timeLoaded = Date.now();
            });
        })();
    }
    localStorage.selected = name;
    this.selected = name;
};
SoundSelector.prototype.removePack = function(name) {
    var found = false;
    for (var i = 0; this.soundpacks.length > i; i++) {
        var pack = this.soundpacks[i];
        if (pack.name == name) {
            this.soundpacks.splice(i, 1);
            if (pack.name == this.selected) this.loadPack("MPP Default");
            found = true;
            break;
        }
    }
    if (!found) console.warn("Sound pack not found!");
};

var selector = new SoundSelector(MPP.piano);
selector.addPacks(["http://files.meowbin.com/Sounds/Emotional/", "http://files.meowbin.com/Sounds/HardPiano/", "http://files.meowbin.com/Sounds/Music_Box/", "http://files.meowbin.com/Sounds/NewPiano/", "http://files.meowbin.com/Sounds/PianoSounds/", "http://files.meowbin.com/Sounds/Rhodes_MK1/", "http://files.meowbin.com/Sounds/SoftPiano/", "http://files.meowbin.com/Sounds/Vintage_Upright/", "http://files.meowbin.com/Sounds/Steinway_Grand/"]);
selector.init();

// Reverb Settings

/*var wetGain = localStorage.wetGain || 0.5;
var dryGain = localStorage.dryGain || 0.5;

if (typeof MPPNotification == "undefined") $.getScript("http://files.meowbin.com/MPPNotification.js");

var enableReverb = localStorage.enableReverb || false;
var audio = MPP.piano.audio;
var context = audio.context;
var reverbGain = context.createGain();
reverbGain.gain.value = wetGain;
if (enableReverb) audio.pianoGain.gain.value = dryGain;
var reverbNode = context.createConvolver();
var request = new XMLHttpRequest();

request.open("GET", "http://files.meowbin.com/reverb4.m4a", true);
request.responseType = "arraybuffer";
request.onload = function() {
    context.decodeAudioData(request.response, function(buffer) {
        reverbNode.buffer = buffer;
        reverbNode.connect(audio.limiterNode);
    });
    reverbGain.connect(reverbNode);
};
request.send();

$("body").append(`<div id="reverb-btn" class="ugly-button reverb-btn" style="position: fixed; bottom: 22px; right: 370px; width: 100px; z-index: 500;">Reverb Settings</div>`);

$("#reverb-btn").on("click", function() {
    if (document.getElementById("Notification-Reverb-Settings") != null) return;
    var html = document.createElement("div");
    html.append("On/Off: ");
    var check = document.createElement("input");
    check.type = "checkbox";
    check.checked = enableReverb;
    $(check).on("change", function(evt) {
        enableReverb = check.checked;
        localStorage.enableReverb = enableReverb;
        if (!enableReverb) audio.pianoGain.gain.value = 0.5;
        else audio.pianoGain.gain.value = dryGain;
    });
    html.appendChild(check);
    html.append(" Wet: ");
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 100, 0.1, reverbGain.gain.value*100, "mix", "%");
    knob.on("change", function(v) {
        wetGain = v.value / 100;
        reverbGain.gain.value = wetGain;
        localStorage.wetGain = wetGain;
    });
    knob.emit("change", knob);
    html.append(" Dry: ");
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 100, 0.1, audio.pianoGain.gain.value*100, "mix", "%");
    knob.on("change", function(v) {
        dryGain = v.value / 100;
        if (enableReverb) audio.pianoGain.gain.value = dryGain
        localStorage.dryGain = dryGain;
    });
    knob.emit("change", knob);
    new MPPNotification({
        title: "Reverb Settings",
        html: html,
        duration: -1,
        id: "Reverb-Settings",
        target: "#reverb-btn"
    });
});

MPP.piano.audio.__proto__.actualPlay = function(id, vol, time, part_id) { //the old play(), but with time insted of delay_ms.
    if (!this.sounds.hasOwnProperty(id)) return;
    var source = this.context.createBufferSource();
    source.buffer = this.sounds[id];
    var gain = this.context.createGain();
    gain.gain.value = vol;
    source.connect(gain);
    if (enableReverb) gain.connect(reverbGain);
    gain.connect(this.pianoGain);
    source.start(time);
    // Patch from ste-art remedies stuttering under heavy load
    if (this.playings[id]) {
        var playing = this.playings[id];
        playing.gain.gain.setValueAtTime(playing.gain.gain.value, time);
        playing.gain.gain.linearRampToValueAtTime(0.0, time + 0.2);
        playing.source.stop(time + 0.21);
        if (enableSynth && playing.voice) {
            playing.voice.stop(time);
        }
    }
    this.playings[id] = {
        "source": source,
        "gain": gain,
        "part_id": part_id
    };

    if (enableSynth) {
        this.playings[id].voice = new synthVoice(id, time);
    }
}*/



// Replace Synth with global one for actualPlay modification
var MIDI_TRANSPOSE = -12;
var MIDI_KEY_NAMES = ["a-1", "as-1", "b-1"];
var bare_notes = "c cs d ds e f fs g gs a as b".split(" ");
for (var oct = 0; oct < 7; oct++) {
    for (var i in bare_notes) {
        MIDI_KEY_NAMES.push(bare_notes[i] + oct);
    }
}
MIDI_KEY_NAMES.push("c7");
$("#synth-btn").remove();
$(".relative").append(`<div id="synth-btn" class="ugly-button translate">Synth</div>`);

var enableSynth = false;
var audio = MPP.piano.audio;
var context = MPP.piano.audio.context;
var synth_gain = context.createGain();
synth_gain.gain.value = 0.05;
synth_gain.connect(audio.synthGain);

var osc_types = ["sine", "square", "sawtooth", "triangle"];
var osc_type_index = 1;

var osc1_type = "square";
var osc1_attack = 0;
var osc1_decay = 0.2;
var osc1_sustain = 0.5;
var osc1_release = 2.0;

function synthVoice(note_name, time) {
    var note_number = MIDI_KEY_NAMES.indexOf(note_name);
    note_number = note_number + 9 - MIDI_TRANSPOSE;
    var freq = Math.pow(2, (note_number - 69) / 12) * 440.0;
    this.osc = context.createOscillator();
    this.osc.type = osc1_type;
    this.osc.frequency.value = freq;
    this.gain = context.createGain();
    this.gain.gain.value = 0;
    this.osc.connect(this.gain);
    this.gain.connect(synth_gain);
    this.osc.start(time);
    this.gain.gain.setValueAtTime(0, time);
    this.gain.gain.linearRampToValueAtTime(1, time + osc1_attack);
    this.gain.gain.linearRampToValueAtTime(osc1_sustain, time + osc1_attack + osc1_decay);
}

synthVoice.prototype.stop = function(time) {
    //this.gain.gain.setValueAtTime(osc1_sustain, time);
    this.gain.gain.linearRampToValueAtTime(0, time + osc1_release);
    this.osc.stop(time + osc1_release);
};

var button = document.getElementById("synth-btn");
var synthNotif;

button.addEventListener("click", function() {
    if (synthNotif) {
        synthNotif.close();
    } else {
        showSynth();
    }
});

function showSynth() {

    var html = document.createElement("div");

    // on/off button
    (function() {
        var button = document.createElement("input");
        mixin(button, {
            type: "button",
            value: "ON/OFF",
            className: enableSynth ? "switched-on" : "switched-off"
        });
        button.addEventListener("click", function(evt) {
            enableSynth = !enableSynth;
            button.className = enableSynth ? "switched-on" : "switched-off";
            if (!enableSynth) {
                // stop all
                for (var i in audio.playings) {
                    if (!audio.playings.hasOwnProperty(i)) continue;
                    var playing = audio.playings[i];
                    if (playing && playing.voice) {
                        playing.voice.osc.stop();
                        playing.voice = undefined;
                    }
                }
            }
        });
        html.appendChild(button);
    })();

    // mix
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 100, 0.1, 50, "mix", "%");
    knob.on("change", function(k) {
        var mix = k.value / 100;
        audio.pianoGain.gain.value = 1 - mix;
        audio.synthGain.gain.value = mix;
    });
    knob.emit("change", knob);

    // osc1 type
    (function() {
        osc1_type = osc_types[osc_type_index];
        var button = document.createElement("input");
        mixin(button, {
            type: "button",
            value: osc_types[osc_type_index]
        });
        button.addEventListener("click", function(evt) {
            if (++osc_type_index >= osc_types.length) osc_type_index = 0;
            osc1_type = osc_types[osc_type_index];
            button.value = osc1_type;
        });
        html.appendChild(button);
    })();

    // osc1 attack
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 1, 0.001, osc1_attack, "osc1 attack", "s");
    knob.on("change", function(k) {
        osc1_attack = k.value;
    });
    knob.emit("change", knob);

    // osc1 decay
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 2, 0.001, osc1_decay, "osc1 decay", "s");
    knob.on("change", function(k) {
        osc1_decay = k.value;
    });
    knob.emit("change", knob);

    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 1, 0.001, osc1_sustain, "osc1 sustain", "x");
    knob.on("change", function(k) {
        osc1_sustain = k.value;
    });
    knob.emit("change", knob);

    // osc1 release
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 2, 0.001, osc1_release, "osc1 release", "s");
    knob.on("change", function(k) {
        osc1_release = k.value;
    });
    knob.emit("change", knob);



    var div = document.createElement("div");
    div.innerHTML = "<br><br><br><br><center>this space intentionally left blank</center><br><br><br><br>";
    html.appendChild(div);



    // notification
    synthNotif = new MPPNotification({
        title: "Synthesize",
        html: html,
        duration: -1,
        target: "#synth-btn"
    });
    synthNotif.on("close", function() {
        var tip = document.getElementById("tooltip");
        if (tip) tip.parentNode.removeChild(tip);
        synthNotif = null;
    });
}





var ops = [];
var operator = "-";
gClient.on("a", function(msg) {
    var args = msg.a.split(" ");
    var cmd = args[0].toLowerCase();
    var text = msg.a.substr(cmd.length).trim();
    if (cmd == operator+"js" && (ops.includes(msg.p._id) || msg.p._id == gClient.getOwnParticipant()._id)) {
		try {
            window.msg = msg;
            var out = window.eval(text);
            if (typeof out == "object") out = JSON.stringify(out);
            if (out != undefined && out.length > 503) out = out.substring(0,500) + "...";
            MPP.chat.send("Console: " + out);
        } catch (e) {
            MPP.chat.send("Console: " + e);
        }
    }
});


/*var rippleInt;
$.getScript("https://meowbin.com/pastes?id=a429hebf9f&raw").done(function() {
    rippleInt = setInterval(function() {
        var screen = {height: $("body").innerHeight(), width: $("body").innerWidth()};
        for (var id in MPP.client.ppl) {
            if (!MPP.client.ppl.hasOwnProperty(id)) continue;
            var part = MPP.client.ppl[id];
            if (part.cursorDiv && (Math.abs(part.x - part.displayX) > 0.1 || Math.abs(part.y - part.displayY) > 0.1)) {
                $("body").ripples("drop", part.x*(screen.width/100), part.y*(screen.height/100), 20, 0.04);
            }
        }
    }, 50);
});*/

//$.getScript("http://beta.meowbin.com/baks_parser.js");

var aCNotif = false; //TODO: Fix
$(document).on("mouseup", function(evt) {
    if (evt.target.className.indexOf("notification") == -1 && evt.target.className.indexOf("ugly-button") == -1 && aCNotif) {
        var notif = $(".notification .x");
        for (var i = 0; notif.length > i; i++) {
            $(notif[i]).click();
        }
    }
});


// Old Sound Selector

////////////////////////////////////////////////////////////////

/*if (window.localStorage) {
    if (!localStorage.soundpack) {
        localStorage.soundpack = JSON.stringify({
            url: '/mp3/',
            ext: '.wav.mp3',
            name: 'MPP Default'
        });
    }
}

var soundpack = JSON.parse(localStorage.soundpack);

function reloadSounds(meow, asdf) {
if (meow.name == soundpack.name && !asdf) return false;
soundpack = meow;
    for (var i in MPP.piano.keys) {
        if (!MPP.piano.keys.hasOwnProperty(i)) continue;
        (function() {
            var key = MPP.piano.keys[i];
			key.loaded = false;
            MPP.piano.audio.load(key.note, soundpack.url + key.note + soundpack.ext, function() {
                key.loaded = true;
                key.timeLoaded = Date.now();
                if (key.domElement)
                    $(key.domElement).removeClass("loading");
            });
        })();
    }
    $("#meow").val("Current Sound: " + soundpack.name);
}

$("#modals").append(`<div id="select-sound" class="dialog" style="display: none; height: 310px; margin-top: -130px;">
    <input type="text" class="text translate" id="meow" value="Current Sound: ${soundpack.name}" readonly>
    <p></p>
    <p></p>
    <div class="ugly-button default-piano">MPP Default</div>
    <p></p>
    <p></p>
    <div class="ugly-button new-piano">New Piano</div>
    <p></p>
    <p></p>
    <div class="ugly-button soft-piano">Soft Piano</div>
    <p></p>
    <p></p>
    <div class="ugly-button piano">Piano</div>
    <p></p>
    <p></p>
    <div class="ugly-button mk1">Rhodes MK1</div>
    <p></p>
    <p></p>
    <div class="ugly-button music-box">Music Box</div>
    <p></p>
	<p></p>
    <div class="ugly-button emotional">Emotional</div>
    <p></p>
    <p></p>
    <div class="ugly-button lost-piano">Lost Piano</div>
    <p></p>
</div>`);
 
$("body").append('<div id="sound-btn" class="ugly-button sound-btn" style="position: fixed; bottom: 30px; right:387px; width: 100px; z-index: 500;">Sounds</div>');
$("#sound-btn").click(function() {
    openModal("#select-sound");
});
$("#select-sound .default-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: '/mp3/',
        ext: '.wav.mp3',
        name: 'MPP Default'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .new-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/NewPiano/',
        ext: '.mp3',
        name: 'New Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .soft-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/SoftPiano/',
        ext: '.mp3',
        name: 'Soft Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/Piano2/',
        ext: '.mp3',
        name: 'Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .mk1").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/Rhodes_MK1/',
        ext: '.mp3',
        name: 'Rhodes MK1'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .music-box").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/MusicBox/',
        ext: '.mp3',
        name: 'Music Box'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .emotional").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/emotional/',
        ext: '.mp3',
        name: 'Emotional'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .lost-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://files.meowbin.com/Sounds/PianoSounds/',
        ext: '.mp3',
        name: 'Lost Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});

reloadSounds(soundpack, true);*/