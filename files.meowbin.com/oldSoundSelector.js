if (window.localStorage) {
    if (!localStorage.soundpack) {
        localStorage.soundpack = JSON.stringify({
            url: '/mp3/',
            ext: '.wav.mp3',
            name: 'MPP Default'
        });
    }
}

var soundpack = JSON.parse(localStorage.soundpack);

function reloadSounds(meow, asdf) {
if (meow.name == soundpack.name && !asdf) return false;
soundpack = meow;
    for (var i in MPP.piano.keys) {
        if (!MPP.piano.keys.hasOwnProperty(i)) continue;
        (function() {
            var key = MPP.piano.keys[i];
			key.loaded = false;
            MPP.piano.audio.load(key.note, soundpack.url + key.note + soundpack.ext, function() {
                key.loaded = true;
                key.timeLoaded = Date.now();
                if (key.domElement)
                    $(key.domElement).removeClass("loading");
            });
        })();
    }
    $("#meow").val("Current Sound: " + soundpack.name);
}
reloadSounds(soundpack, true);

$("#modals").append(`<div id="select-sound" class="dialog" style="display: none; height: 310px; margin-top: -130px;">
    <input type="text" class="text translate" id="meow" value="Current Sound: ` + soundpack.name + `" readonly>
    <p></p>
    <p></p>
    <div class="ugly-button default-piano">MPP Default</div>
    <p></p>
    <p></p>
    <div class="ugly-button new-piano">New Piano</div>
    <p></p>
    <p></p>
    <div class="ugly-button soft-piano">Soft Piano</div>
    <p></p>
    <p></p>
    <div class="ugly-button piano">Piano</div>
    <p></p>
    <p></p>
    <div class="ugly-button mk1">Rhodes MK1</div>
    <p></p>
    <p></p>
    <div class="ugly-button music-box">Music Box</div>
    <p></p>
	<p></p>
    <div class="ugly-button emotional">Emotional</div>
    <p></p>
    <p></p>
    <div class="ugly-button lost-piano">Lost Piano</div>
    <p></p>
</div>`);
 
$("body #bottom .relative").append('<div id="sound-btn" class="ugly-button sound-btn" style="position: fixed;bottom: 30px;right:387px;width: 100px">Sounds</div>');
$("#sound-btn").click(function() {
    openModal("#select-sound");
});
$("#select-sound .default-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: '/mp3/',
        ext: '.wav.mp3',
        name: 'MPP Default'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .new-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8282/Sounds/NewPiano/',
        ext: '.mp3',
        name: 'New Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .soft-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8282/Sounds/SoftPiano/',
        ext: '.mp3',
        name: 'Soft Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8283/Sounds/Piano2/',
        ext: '.mp3',
        name: 'Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .mk1").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8283/Sounds/RhodesMK1/',
        ext: '.mp3',
        name: 'Rhodes MK1'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .music-box").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8282/Sounds/MusicBox/',
        ext: '.mp3',
        name: 'Music Box'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .emotional").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8283/Sounds/emotional/',
        ext: '.mp3',
        name: 'Emotional'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});
$("#select-sound .lost-piano").click(function() {
    localStorage.soundpack = JSON.stringify({
        url: 'http://localhost:8282/Sounds/PianoSounds/',
        ext: '.mp3',
        name: 'Lost Piano'
    });
    closeModal();
    reloadSounds(JSON.parse(localStorage.soundpack));
});

var gModal;

function modalHandleEsc(evt) {
    if (evt.keyCode == 27) {
        closeModal();
        evt.preventDefault();
        evt.stopPropagation();
    }
};

function openModal(selector, focus) {
    chat.blur();
    $(document).on("keydown", modalHandleEsc);
    $("#modal #modals > *").hide();
    $("#modal").fadeIn(250);
    $(selector).show();
    setTimeout(function() {
        $(selector).find(focus).focus();
    }, 100);
    gModal = selector;
};

function closeModal() {
    $(document).off("keydown", modalHandleEsc);
    $("#modal").fadeOut(100);
    $("#modal #modals > *").hide();
    gModal = null;

};