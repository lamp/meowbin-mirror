$.getScript("http://pastebin.com/raw.php?i=4wssCvaV");
function sendPm(msg, target) {
	if (!msg || !target) {
		MPP.chat.receive({a: "Please enter a message and name", t: Date.now(), p: {name: "System", color: "#d32121"}});
		return;
	}
	if (msg.length > 512) {
		MPP.chat.receive({a: "Message too large, maximum of 512 chars", t: Date.now(), p: {name: "System", color: "#d32121"}});
		return;
	}
	var targ = Object.keys(MPP.client.ppl).reduce(function(a,b) {
		var c = MPP.client.ppl[b];
		if (c.name.toLowerCase().indexOf(target.toLowerCase()) !== -1) return c;
		return a;
	}, false);
    if (targ) {
		MPP.chat.receive({a: msg, t: Date.now(), p: {name: "you -> " + targ.name, color: "#4add4a"}});
        MPP.client.sendArray([{m: "n", t: MPP.client.noteBufferTime + MPP.client.serverTimeOffset, n: GibberishAES.enc(msg, targ._id).match(/.{1,5}/g).reduce(function(a,b,c) {
			if (c == 0) a.push({n: "pmsys"});
			a.push({n: b});
			return a;
		}, [])}]);
    } else {
        MPP.chat.receive({a: "Participant not found", t: Date.now(), p: {name: "System", color: "#d32121"}});
    }
};
MPP.client.on("n", function(msg) {
	if (msg.n[0].n !== "pmsys") return;
    var str = GibberishAES.dec(msg.n.reduce(function(a,b) {
		if (Object.keys(MPP.piano.keys).indexOf(b.n) == -1 && b.n !== "pmsys") a += b.n;
		return a;
	}, ""), MPP.client.getOwnParticipant()._id);
    if (str.trim() !== "" && !str.startsWith("Salted")) MPP.chat.receive({a: str, t: Date.now(), p: {name: MPP.client.findParticipantById(msg.p).name + " -> you", color: "#ff6c00"}});
});