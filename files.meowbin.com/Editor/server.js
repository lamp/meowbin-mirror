var fs = require('fs');
var express = require('express');
var compress = require('compression');
var app = express();
var http = require('http').Server(app);
var serve = require('serve-static');
app.use(compress());

app.use(serve(__dirname, {
    maxAge: 1000 * 60 * 60
}));
http.listen(3030);