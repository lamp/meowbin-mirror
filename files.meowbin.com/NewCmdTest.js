var accessLevels = {};
accessLevels[MPP.client.getOwnParticipant()._id] = 2;
var operator = "-";
function newCmd(cmd, alias, cb, al = 0) { //access levels: 0 = default, 1 = moderator, 2 = admin
	cmds[cmd] = {al, cb};
	if (alias) cmds[cmd].alias = alias.split(", ");
}
function delCmd(cmd) {
	delete cmds[cmd];
}
var cmds = {help: {
	alias: ["h"],
	al: 0,
	cb: (msg) => {
		var cmdKeys = Object.keys(cmds);
		var input = msg.input.toLowerCase();
		if (msg.input.trim() == "") {
			var out = "Commands: ";
			for (var i = 0; cmdKeys.length > i; i++) {
				var cmd = cmds[cmdKeys[i]];
				out += `${operator+cmdKeys[i]}` + (cmd.alias ? ` [${operator}${cmd.alias.join(" " + operator)}]` : "") + ", ";
			}
			MPP.chat.send(out.substr(0, out.length-2));
		} else if (cmdKeys.indexOf(input) != -1) { //if command is found
			var cmd = cmds[input];
			MPP.chat.send(`Description for ${operator+input}: ${cmd.desc}` + (cmd.alias ? ` Aliases: ${operator}${cmd.alias.join(" " + operator)}` : ""));
		} else {
			MPP.chat.send(`Command ${input} not found.`);
		}
	}, 
	desc: "Displays all commands"
}, js: {
	al: 2,
	cb: (msg) => {
		try {
			//window.msg = msg; //make msg global if you want to access it in eval
			MPP.chat.send(`Console: ${JSON.stringify(window.eval(msg.input))}`);
		} catch(e) {
			MPP.chat.send(`Console: ${e.toString()}`);
		}
	},
	desc: "Evaluate JavaScript code"
}, pet: {
	cb: (msg) => {
		if (!msg.part) { //not found
			MPP.chat.send(`Meow! ${msg.p.name} missed and pet the floor :33`);
		} else if (msg.part._id == msg.p._id) { //is command sender
			MPP.chat.send(`Meow! ${msg.p.name} pet themself :3`);
		} else { //found
			MPP.chat.send(`Purr x3 ${msg.p.name} pet ${msg.part.name} :33`);
		}
	},
	desc: "Pet all your friends :33"
}};
Client.prototype.search = function(query) {
	if (query.trim() == "") return undefined;
	for (var i in this.ppl) {
		var part = this.ppl[i];
		if (part.name.indexOf(query.toLowerCase()) !== -1 || part._id == query || part.id == query) {
			return part;
			break;
		}
	}
	return undefined;
};

MPP.client.on("a", function(msg) {
	if (msg.a[0] != operator) return; //no need to go further if it's not a command
	if (accessLevels[msg.p._id] == undefined) accessLevels[msg.p._id] = 0;
	msg.args = msg.a.split(" ");
	msg.p.al = accessLevels[msg.p._id];
	msg.cmd = msg.args[0].toLowerCase().substr(1);
	msg.input = msg.a.substr(msg.cmd.length+1).trim();
	msg.part = MPP.client.search(msg.input);
	msg.me = MPP.client.getOwnParticipant();
	var keys = Object.keys(cmds);
	for (var i = keys.length; i--;) {
		var cmd = cmds[keys[i]];
		if (msg.cmd == keys[i] || cmd.alias && cmd.alias.indexOf(msg.cmd) != -1) {
			if (cmd.al <= msg.p.al) cmd.cb(msg); //command found
			break;
		}
	}
});