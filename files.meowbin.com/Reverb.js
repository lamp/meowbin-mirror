// Reverb Settings
// Support older script.js versions
var audio = MPP.piano.audio;
var context = audio.context;
var olderVersion = typeof audio.gainNode != "undefined";
if (olderVersion) {
    audio.limiterNode = context.createDynamicsCompressor();
    audio.limiterNode.threshold.value = -10;
    audio.limiterNode.knee.value = 0;
    audio.limiterNode.ratio.value = 20;
    audio.limiterNode.attack.value = 0;
    audio.limiterNode.release.value = 0.1;
    audio.limiterNode.connect(audio.gainNode);
    
    // for synth mix
    audio.pianoGain = context.createGain();
    audio.pianoGain.gain.value = 0.5;
    audio.pianoGain.connect(audio.limiterNode);
    audio.synthGain = context.createGain();
    audio.synthGain.gain.value = 0.5;
    audio.synthGain.connect(audio.limiterNode);
}

MPP.piano.audio.__proto__.play = function(id, vol, delay_ms, part_id) {
    if (olderVersion) {
        this.actualPlay(id, vol, delay_ms, part_id);
        return;
    }
    if (!this.sounds.hasOwnProperty(id)) return;
    var time = this.context.currentTime + (delay_ms / 1000); //calculate time on note receive.
    var delay = delay_ms - this.threshold;
    if (delay <= 0) this.actualPlay(id, vol, time, part_id);
    else {
        this.worker.postMessage({
            delay: delay,
            args: {
                action: 0 /*play*/ ,
                id: id,
                vol: vol,
                time: time,
                part_id: part_id
            }
        }); // but start scheduling right before play.
    }
}

var wetGain = localStorage.wetGain || 0.5;
var dryGain = localStorage.dryGain || 0.5;

if (typeof MPPNotification == "undefined") $.getScript("http://files.meowbin.com/MPPNotification.js");

var enableReverb = localStorage.enableReverb || false;
//var enableSync = false; //default
var reverbGain = context.createGain();
reverbGain.gain.value = wetGain;
if (enableReverb) audio.pianoGain.gain.value = dryGain;
var reverbNode = context.createConvolver();
var request = new XMLHttpRequest();

request.open("GET", "http://files.meowbin.com/reverb4.m4a", true);
request.responseType = "arraybuffer";
request.onload = function() {
    context.decodeAudioData(request.response, function(buffer) {
        reverbNode.buffer = buffer;
        reverbNode.connect(audio.limiterNode);
    });
    reverbGain.connect(reverbNode);
};
request.send();

$("body").append(`<div id="reverb-btn" class="ugly-button reverb-btn" style="position: fixed; bottom: 22px; right: 370px; width: 100px; z-index: 500;">Reverb Settings</div>`);

$("#reverb-btn").on("click", function() {
    if (document.getElementById("Notification-Reverb-Settings") != null) return;
    var html = document.createElement("div");
    html.append("On/Off: ");
    var check = document.createElement("input");
    check.type = "checkbox";
    check.checked = enableReverb;
    $(check).on("change", function(evt) {
        enableReverb = check.checked;
        localStorage.enableReverb = enableReverb;
        if (!enableReverb) audio.pianoGain.gain.value = 0.5;
        else audio.pianoGain.gain.value = dryGain;
    });
    html.appendChild(check);
    html.append(" Wet: ");
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 100, 0.1, reverbGain.gain.value*100, "mix", "%");
    knob.on("change", function(v) {
        wetGain = v.value / 100;
        reverbGain.gain.value = wetGain;
        localStorage.wetGain = wetGain;
    });
    knob.emit("change", knob);
    html.append(" Dry: ");
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 100, 0.1, audio.pianoGain.gain.value*100, "mix", "%");
    knob.on("change", function(v) {
        dryGain = v.value / 100;
        if (enableReverb) audio.pianoGain.gain.value = dryGain
        localStorage.dryGain = dryGain;
    });
    knob.emit("change", knob);
    new MPPNotification({
        title: "Reverb Settings",
        html: html,
        duration: -1,
        id: "Reverb-Settings",
        target: "#reverb-btn"
    });
});

MPP.piano.audio.__proto__.actualPlay = function(id, vol, time, part_id) { //the old play(), but with time insted of delay_ms.
    if (!this.sounds.hasOwnProperty(id)) return;
    var source = this.context.createBufferSource();
    source.buffer = this.sounds[id];
    var gain = this.context.createGain();
    gain.gain.value = vol;
    source.connect(gain);
    if (enableReverb) gain.connect(reverbGain);
    gain.connect(this.pianoGain);
    source.start(time);
    // Patch from ste-art remedies stuttering under heavy load
    if (this.playings[id]) {
        var playing = this.playings[id];
        playing.gain.gain.setValueAtTime(playing.gain.gain.value, time);
        playing.gain.gain.linearRampToValueAtTime(0.0, time + 0.2);
        playing.source.stop(time + 0.21);
        if (enableSynth && playing.voice) {
            playing.voice.stop(time);
        }
    }
    this.playings[id] = {
        "source": source,
        "gain": gain,
        "part_id": part_id
    };

    if (enableSynth) {
        this.playings[id].voice = new synthVoice(id, time);
    }
}

// Replace Synth with global one for actualPlay modification
var MIDI_TRANSPOSE = -12;
var MIDI_KEY_NAMES = ["a-1", "as-1", "b-1"];
var bare_notes = "c cs d ds e f fs g gs a as b".split(" ");
for (var oct = 0; oct < 7; oct++) {
    for (var i in bare_notes) {
        MIDI_KEY_NAMES.push(bare_notes[i] + oct);
    }
}
MIDI_KEY_NAMES.push("c7");
$("#synth-btn").remove();
$(".relative").append(`<div id="synth-btn" class="ugly-button translate">Synth</div>`);

var enableSynth = false;
var audio = MPP.piano.audio;
var context = MPP.piano.audio.context;
var synth_gain = context.createGain();
synth_gain.gain.value = 0.05;
synth_gain.connect(audio.synthGain);

var osc_types = ["sine", "square", "sawtooth", "triangle"];
var osc_type_index = 1;

var osc1_type = "square";
var osc1_attack = 0;
var osc1_decay = 0.2;
var osc1_sustain = 0.5;
var osc1_release = 2.0;

function synthVoice(note_name, time) {
    var note_number = MIDI_KEY_NAMES.indexOf(note_name);
    note_number = note_number + 9 - MIDI_TRANSPOSE;
    var freq = Math.pow(2, (note_number - 69) / 12) * 440.0;
    this.osc = context.createOscillator();
    this.osc.type = osc1_type;
    this.osc.frequency.value = freq;
    this.gain = context.createGain();
    this.gain.gain.value = 0;
    this.osc.connect(this.gain);
    this.gain.connect(synth_gain);
    this.osc.start(time);
    this.gain.gain.setValueAtTime(0, time);
    this.gain.gain.linearRampToValueAtTime(1, time + osc1_attack);
    this.gain.gain.linearRampToValueAtTime(osc1_sustain, time + osc1_attack + osc1_decay);
}

synthVoice.prototype.stop = function(time) {
    //this.gain.gain.setValueAtTime(osc1_sustain, time);
    this.gain.gain.linearRampToValueAtTime(0, time + osc1_release);
    this.osc.stop(time + osc1_release);
};

var button = document.getElementById("synth-btn");
var synthNotif;

button.addEventListener("click", function() {
    if (synthNotif) {
        synthNotif.close();
    } else {
        showSynth();
    }
});

function showSynth() {

    var html = document.createElement("div");

    // on/off button
    (function() {
        var button = document.createElement("input");
        mixin(button, {
            type: "button",
            value: "ON/OFF",
            className: enableSynth ? "switched-on" : "switched-off"
        });
        button.addEventListener("click", function(evt) {
            enableSynth = !enableSynth;
            button.className = enableSynth ? "switched-on" : "switched-off";
            if (!enableSynth) {
                // stop all
                for (var i in audio.playings) {
                    if (!audio.playings.hasOwnProperty(i)) continue;
                    var playing = audio.playings[i];
                    if (playing && playing.voice) {
                        playing.voice.osc.stop();
                        playing.voice = undefined;
                    }
                }
            }
        });
        html.appendChild(button);
    })();

    // mix
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 100, 0.1, 50, "mix", "%");
    knob.on("change", function(k) {
        var mix = k.value / 100;
        audio.pianoGain.gain.value = 1 - mix;
        audio.synthGain.gain.value = mix;
    });
    knob.emit("change", knob);

    // osc1 type
    (function() {
        osc1_type = osc_types[osc_type_index];
        var button = document.createElement("input");
        mixin(button, {
            type: "button",
            value: osc_types[osc_type_index]
        });
        button.addEventListener("click", function(evt) {
            if (++osc_type_index >= osc_types.length) osc_type_index = 0;
            osc1_type = osc_types[osc_type_index];
            button.value = osc1_type;
        });
        html.appendChild(button);
    })();

    // osc1 attack
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 1, 0.001, osc1_attack, "osc1 attack", "s");
    knob.on("change", function(k) {
        osc1_attack = k.value;
    });
    knob.emit("change", knob);

    // osc1 decay
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 2, 0.001, osc1_decay, "osc1 decay", "s");
    knob.on("change", function(k) {
        osc1_decay = k.value;
    });
    knob.emit("change", knob);

    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 1, 0.001, osc1_sustain, "osc1 sustain", "x");
    knob.on("change", function(k) {
        osc1_sustain = k.value;
    });
    knob.emit("change", knob);

    // osc1 release
    var knob = document.createElement("canvas");
    mixin(knob, {
        width: 32,
        height: 32,
        className: "knob"
    });
    html.appendChild(knob);
    knob = new Knob(knob, 0, 2, 0.001, osc1_release, "osc1 release", "s");
    knob.on("change", function(k) {
        osc1_release = k.value;
    });
    knob.emit("change", knob);



    var div = document.createElement("div");
    div.innerHTML = "<br><br><br><br><center>this space intentionally left blank</center><br><br><br><br>";
    html.appendChild(div);



    // notification
    synthNotif = new MPPNotification({
        title: "Synthesize",
        html: html,
        duration: -1,
        target: "#synth-btn"
    });
    synthNotif.on("close", function() {
        var tip = document.getElementById("tooltip");
        if (tip) tip.parentNode.removeChild(tip);
        synthNotif = null;
    });
}