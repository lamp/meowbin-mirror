if (!localStorage.friends) localStorage.friends = "{}";
var friends = JSON.parse(localStorage.friends);

function sfn(part) {
    /*var div = $("<div></div>"); //will only work with modified script.js rip cant access the functions to disable keyboard
    var input = $("<input type=\"text\">");
    var nameButt = $("<button>Set Name</button>");
    nameButt.on("click", function() {
        friends[part._id] = input.text();
        localStorage.friends = JSON.stringify(friends);
    });
    input.appendTo(div);
    nameButt.appendTo(div);
    MPP.client.emit("notification", {title: "Set name for " + part.name + " click to close", html: div, duration: -1});*/
    var name = prompt("Set friend name", friends[part._id]);
    if (!name) return;
    friends[part._id] = name;
    localStorage.friends = JSON.stringify(friends);
}

$("head").append('<style>#names .name.friend:after { content: "Friend"; position: absolute; color: #18adf7; top: -4px; right: 45%; font-size: 10px; }</style>');
$("#names").on("mousedown touchstart", function(evt) {
    var part = false;
    var _id = $(".participant-menu .info").text();
    for (var id in MPP.client.ppl) {
        if (MPP.client.ppl[id]._id == _id) {
            part = MPP.client.ppl[id];
            break;
        }
    }
    if (!part) return; //shouldn't happen
    if (!friends[part._id]) {
        var addFriend = $('<div class="menu-item">Add Friend</div>');
        addFriend.on("mousedown touchstart", function() {
            sfn(part);
            $(part.nameDiv).toggleClass("friend");
        });
        addFriend.appendTo($(".participant-menu"));
    } else {
        var updateFriend = $('<div class="menu-item">Update Name</div>'); //TODO: Make a notification to set friend name manually
        updateFriend.on("mousedown touchstart", function() {
            sfn(part);
        });
        updateFriend.appendTo($(".participant-menu"));
        var removeFriend = $('<div class="menu-item">Remove Friend</div>'); //TODO: Make a notification to set friend name manually
        removeFriend.on("mousedown touchstart", function() {
            delete friends[part._id];
            localStorage.friends = JSON.stringify(friends);
            $(part.nameDiv).toggleClass("friend");
        });
        removeFriend.appendTo($(".participant-menu"));
        $('<div class="menu-item">' + friends[part._id] + '</div>').appendTo($(".participant-menu"));
    }
});
for (var id in MPP.client.ppl) {
    var part = MPP.client.ppl[id];
    if (friends[part._id]) $(part.nameDiv).toggleClass("friend");
}
MPP.client.on("participant added", function(part) {
    if (friends[part._id]) $(part.nameDiv).toggleClass("friend");
});