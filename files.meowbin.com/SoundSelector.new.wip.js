//todo: handle invalid packs, handle 404/timeout on .getJSON

function SoundSelector(piano) {
	this.btn_pos = {};
	Object.defineProperties(this.btn_pos, {
        bottom: {
            configurable: true,
            get: function() {
                return $("#sound-btn").css("bottom") || "22px";
            },
            set: function(px) {
                $("#sound-btn").css("bottom", px);
            }
        },
        right: {
            configurable: true,
            get: function() {
                return $("#sound-btn").css("right") || "250px";
            },
            set: function(px) {
                $("#sound-btn").css("right", px);
            }
        }
    });

    this.initiated = false;
    this.keys = piano.keys;
    this.loading = {};
    this.notif;
    this.packs = [];
    this.selected = localStorage.selected || "MPP Default";
    this.addPack({name: "MPP Default", keys: Object.keys(this.piano.keys), ext: ".wav.mp3", url: "/mp3/"});
}

SoundSelector.prototype.addPack = function(pack, load) {
	var self = this;
	self.loading[pack.url || pack] = true;
	function add(obj) {
		if (self.packs.indexOf(obj) != -1) return; //no adding packs twice D:<
		obj.html - document.createElement("li");
		obj.html.className = "pack";
		obj.html.innerText = obj.name + " (" + obj.keys.length + " keys)";
		obj.onclick = function() {
			self.loadPack(obj.name);
			self.notification.close();
		};
		self.packs.push(obj);
		self.packs.sort(function(a, b) {
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        });
        if (load) self.loadPack(obj.name);
        delete self.loading[obj.url];
	}

	if (!pack instanceof Object) $.getJSON(pack + "/info.json").done(function(json) {
		json.url = pack;
		add(json);
	}); else add(pack); //validate packs??
};

SoundSelector.prototype.addPacks = function(packs) {
	for (var i = 0; packs.length > i; i++) this.addPack(packs[i]);
};

SoundSelector.prototype.init = function() {
	var self = this;
	if (self.initialized) return console.warn("Sound selector already initialized!");

    if (!!Object.keys(self.loading).length) return setTimeout(function() {
        self.init();
    }, 250);

    $("#sound-btn").on("click", function() {
    	if (document.getElementById("Notification-Sound-Selector") != null) return;
    	var html = document.createElement("ul");
        $(html).append("<h1>Current Sound: " + self.selected + "</h1>");

        for (var i = 0; self.packs.length > i; i++) {
        	var pack = self.packs[i];
        	if (pack.name == self.selected) pack.html.classList = "pack enabled";
            else pack.html.classList = "pack";
        	html.appendChild(pack.html); 
        }
        self.notification = new Notification({title: "Sound Selector:", html: html, id: "Sound-Selector", duration: -1, target: "#sound-btn"});
    });
    self.initialized = true;
    self.loadPack(self.selected, true);
};

SoundSelector.prototype.loadPack = function(pack, f) {
	for (var i = 0; this.packs.length > i; i++) {
		var p = this.packs[i];
		if (p.name == pack) {
			pack = p;
			break;
		}
	}
	if (pack instanceof String) {
		console.warn("Sound pack does not exist! Loading default pack...");
        pack = this.packs[0];
	}

	if (pack.name == this.selected && !f) return;

	this.piano.keys = {};
	for (var i = 0; pack.keys.length > i; i++) this.piano.keys[pack.keys[i]] = this.keys[pack.keys[i]];
    this.piano.renderer.resize();

	var self = this;
	for (var i in this.piano.keys) {
        if (!this.piano.keys.hasOwnProperty(i)) continue;
        (function() {
            var key = self.piano.keys[i];
            key.loaded = false;
            self.piano.audio.load(key.note, pack.url + key.note + pack.ext, function() {
                key.loaded = true;
                key.timeLoaded = Date.now();
            });
        })();
    }
    localStorage.selected = name;
    this.selected = name;
};

SoundSelector.prototype.removePack = function(name) {
	var found = false;
	for (var i = 0; this.packs.length > i; i++) {
		var pack = this.packs[i];

		if (pack.name == name) {
			this.packs.splice(i, 1);
			if (pack.name == this.selected) this.loadPack(this.packs[0].name); //add mpp default if none?
			break;
		}
	}
	if (!found) console.warn("Sound pack not found!");
};