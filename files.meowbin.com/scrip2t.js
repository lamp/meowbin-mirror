var currentFps = 0;
var lastCall = Date.now();
requestAnimationFrame(function countFps() {
	var time = (Date.now() - lastCall)/1000;
	lastCall = Date.now();
    currentFps = 1/time;
	requestAnimationFrame(countFps);
});